export default function insertRangeBefore(element, firstNode, lastNode) {
  var parentNode = element.parentNode;

  var nextNode = void 0;

  while (firstNode) {
    nextNode = firstNode.nextSibling;
    parentNode.insertBefore(firstNode, element);

    if (firstNode === lastNode) {
      break;
    }

    firstNode = nextNode;
  }
}