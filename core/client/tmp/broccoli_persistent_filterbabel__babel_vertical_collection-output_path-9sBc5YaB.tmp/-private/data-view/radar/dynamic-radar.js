

import Radar from './radar';
import SkipList from '../skip-list';
import roundTo from '../utils/round-to';

export default class DynamicRadar extends Radar {
  constructor(parentToken, initialItems, initialRenderCount, startingIndex, shouldRecycle) {
    super(parentToken, initialItems, initialRenderCount, startingIndex, shouldRecycle);

    this._firstItemIndex = 0;
    this._lastItemIndex = 0;

    this._totalBefore = 0;
    this._totalAfter = 0;

    this.skipList = null;

    if (true) {
      Object.preventExtensions(this);
    }
  }

  destroy() {
    super.destroy();

    this.skipList = null;
  }

  _updateConstants() {
    super._updateConstants();

    // Create the SkipList only after the estimateHeight has been calculated the first time
    if (this.skipList === null) {
      this.skipList = new SkipList(this.totalItems, this._estimateHeight);
    } else {
      this.skipList.defaultValue = this._estimateHeight;
    }
  }

  _updateIndexes() {
    var skipList = this.skipList,
        visibleMiddle = this.visibleMiddle,
        totalItems = this.totalItems,
        totalComponents = this.totalComponents,
        _prevFirstItemIndex = this._prevFirstItemIndex,
        _didReset = this._didReset;


    if (totalItems === 0) {
      this._firstItemIndex = 0;
      this._lastItemIndex = -1;
      this._totalBefore = 0;
      this._totalAfter = 0;

      return;
    }

    // Don't measure if the radar has just been instantiated or reset, as we are rendering with a
    // completely new set of items and won't get an accurate measurement until after they render the
    // first time.
    if (_didReset === false) {
      this._measure();
    }

    var values = skipList.values;

    var _skipList$find = this.skipList.find(visibleMiddle),
        totalBefore = _skipList$find.totalBefore,
        totalAfter = _skipList$find.totalAfter,
        middleItemIndex = _skipList$find.index;

    var maxIndex = totalItems - 1;

    var firstItemIndex = middleItemIndex - Math.floor(totalComponents / 2);
    var lastItemIndex = middleItemIndex + Math.ceil(totalComponents / 2) - 1;

    if (firstItemIndex < 0) {
      firstItemIndex = 0;
      lastItemIndex = Math.min(totalComponents - 1, maxIndex);
    }

    if (lastItemIndex > maxIndex) {
      lastItemIndex = maxIndex;
      firstItemIndex = Math.max(maxIndex - (totalComponents - 1), 0);
    }

    // Add buffers
    for (var i = middleItemIndex - 1; i >= firstItemIndex; i--) {
      totalBefore -= values[i];
    }

    for (var _i = middleItemIndex + 1; _i <= lastItemIndex; _i++) {
      totalAfter -= values[_i];
    }

    var itemDelta = _prevFirstItemIndex !== null ? firstItemIndex - _prevFirstItemIndex : lastItemIndex - firstItemIndex;

    if (itemDelta < 0 || itemDelta >= totalComponents) {
      this.schedule('measure', () => {
        // schedule a measurement for items that could affect scrollTop
        var staticVisibleIndex = this.renderFromLast ? this.lastVisibleIndex + 1 : this.firstVisibleIndex;
        var numBeforeStatic = staticVisibleIndex - firstItemIndex;

        var measureLimit = Math.min(Math.abs(itemDelta), numBeforeStatic);

        this._prependOffset += Math.round(this._measure(measureLimit));
      });
    }

    this._firstItemIndex = firstItemIndex;
    this._lastItemIndex = lastItemIndex;
    this._totalBefore = totalBefore;
    this._totalAfter = totalAfter;
  }

  _measure(measureLimit = null) {
    var orderedComponents = this.orderedComponents,
        itemContainer = this.itemContainer,
        totalBefore = this.totalBefore,
        skipList = this.skipList;


    var numToMeasure = measureLimit !== null ? measureLimit : orderedComponents.length;

    var totalDelta = 0;

    for (var i = 0; i < numToMeasure; i++) {
      var currentItem = orderedComponents[i];
      var previousItem = orderedComponents[i - 1];
      var itemIndex = currentItem.index;

      var _currentItem$getBound = currentItem.getBoundingClientRect(),
          currentItemTop = _currentItem$getBound.top,
          currentItemHeight = _currentItem$getBound.height;

      var margin = void 0;

      if (previousItem !== undefined) {
        margin = currentItemTop - previousItem.getBoundingClientRect().bottom;
      } else {
        margin = currentItemTop - itemContainer.getBoundingClientRect().top - totalBefore;
      }

      var itemDelta = skipList.set(itemIndex, roundTo(currentItemHeight + margin));

      if (itemDelta !== 0) {
        totalDelta += itemDelta;
      }
    }

    return totalDelta;
  }

  get total() {
    return this.skipList.total;
  }

  get totalBefore() {
    return this._totalBefore;
  }

  get totalAfter() {
    return this._totalAfter;
  }

  get firstItemIndex() {
    return this._firstItemIndex;
  }

  get lastItemIndex() {
    return this._lastItemIndex;
  }

  get firstVisibleIndex() {
    var visibleTop = this.visibleTop;

    var _skipList$find2 = this.skipList.find(visibleTop),
        index = _skipList$find2.index;

    return index;
  }

  get lastVisibleIndex() {
    var visibleBottom = this.visibleBottom,
        totalItems = this.totalItems;

    var _skipList$find3 = this.skipList.find(visibleBottom),
        index = _skipList$find3.index;

    return Math.min(index, totalItems - 1);
  }

  prepend(numPrepended) {
    super.prepend(numPrepended);

    this.skipList.prepend(numPrepended);
  }

  append(numAppended) {
    super.append(numAppended);

    this.skipList.append(numAppended);
  }

  reset() {
    super.reset();

    if (this.skipList !== null) {
      this.skipList.reset(this.totalItems);
    }
  }

  /*
   * Public API to query the skiplist for the offset of an item
   */
  getOffsetForIndex(index) {
    this._measure();

    return this.skipList.getOffset(index);
  }
}