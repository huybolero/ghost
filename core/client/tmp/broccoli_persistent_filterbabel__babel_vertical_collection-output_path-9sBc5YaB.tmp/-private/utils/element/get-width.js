export default function getWidth(dims, withMargins) {
  var width = void 0;

  switch (dims.boxSizing) {
    case 'border-box':
      width = dims.width + dims.borderLeftWidth + dims.borderRightWidth + dims.paddingLeft + dims.paddingRight;
      break;
    case 'content-box':
      width = dims.width;
      break;
    default:
      width = dims.width;
      break;
  }

  // TODO add explicit test
  if (withMargins) {
    width += dims.marginLeft + dims.marginRight;
  }
  return width;
}