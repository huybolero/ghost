define('ember-infinity/lib/infinity-promise-array', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.ArrayProxy.extend(Ember.PromiseProxyMixin);
});