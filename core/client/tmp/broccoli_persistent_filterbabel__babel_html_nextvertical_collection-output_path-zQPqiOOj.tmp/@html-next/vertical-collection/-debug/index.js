define('@html-next/vertical-collection/-debug/index', ['@html-next/vertical-collection/-debug/edge-visualization/debug-mixin', '@html-next/vertical-collection/components/vertical-collection/component'], function (_debugMixin, _component) {
  'use strict';

  _component.default.reopen(_debugMixin.default);
});