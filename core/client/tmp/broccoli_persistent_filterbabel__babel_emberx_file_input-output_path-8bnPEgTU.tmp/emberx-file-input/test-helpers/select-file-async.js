define('emberx-file-input/test-helpers/select-file-async', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Test.registerAsyncHelper('selectFile', function (app, selector, file) {
    return triggerEvent(selector, 'change', { testingFiles: [file] });
  });
});