define('ember-one-way-select/index', ['exports', 'ember-one-way-select/components/one-way-select', 'ember-one-way-select/components/one-way-input'], function (exports, _oneWaySelect, _oneWayInput) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'OneWaySelect', {
    enumerable: true,
    get: function () {
      return _oneWaySelect.default;
    }
  });
  Object.defineProperty(exports, 'OneWayInput', {
    enumerable: true,
    get: function () {
      return _oneWayInput.default;
    }
  });
});