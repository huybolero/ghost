define('ember-one-way-select/components/one-way-select/option', ['exports', 'ember-one-way-select/templates/components/one-way-select/option'], function (exports, _option) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    layout: _option.default,
    tagName: ''
  });
});