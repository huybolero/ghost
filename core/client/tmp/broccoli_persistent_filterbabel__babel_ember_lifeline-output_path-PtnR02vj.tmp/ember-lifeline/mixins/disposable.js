define('ember-lifeline/mixins/disposable', ['exports', 'ember-lifeline/utils/get-or-allocate'], function (exports, _getOrAllocate) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Mixin.create({
    init() {
      this._super(...arguments);

      this._registeredDisposables = undefined;
    },

    /**
     Adds a new disposable to the Ember object. A disposable is a function that
     disposes of resources that are outside of Ember's lifecyle. This essentially
     means you can register a function that you want to run to automatically tear
     down any resources when the Ember object is destroyed.
      Example:
      ```js
     // app/components/foo-bar.js
     import Ember from 'ember';
     import DOMish from 'some-external-library';
      const { run } = Ember;
      export default Component.extend({
       init() {
         this.DOMish = new DOMish();
          this.bindEvents();
       },
        bindEvents() {
         let onFoo = run.bind(this.respondToDomEvent);
         this.DOMish.on('foo', onFoo);
          this.domFooDisposable = this.registerDisposable(() => this.DOMish.off('foo', onFoo));
       },
        respondToDOMEvent() {
         // do something
       }
     });
     ```
     @method registerDisposable
    @param { Function } dispose
    @returns A disposable object
    @public
    */
    registerDisposable(dispose) {
      (true && !(typeof dispose === 'function') && Ember.assert('Called \`registerDisposable\` where \`dispose\` is not a function', typeof dispose === 'function'));


      let disposables = (0, _getOrAllocate.default)(this, '_registeredDisposables', Array);
      let disposable = toDisposable(dispose);

      disposables.push(disposable);

      return disposable;
    },

    willDestroy() {
      runDisposables(this._registeredDisposables);

      this._super(...arguments);
    }
  });


  function runDisposables(disposables) {
    if (!disposables) {
      return;
    }

    for (let i = 0, l = disposables.length; i < l; i++) {
      let disposable = disposables.pop();

      disposable.dispose();
    }
  }

  function toDisposable(doDispose) {
    return {
      dispose() {
        if (!this.disposed) {
          this.disposed = true;
          doDispose();
        }
      },
      disposed: false
    };
  }
});