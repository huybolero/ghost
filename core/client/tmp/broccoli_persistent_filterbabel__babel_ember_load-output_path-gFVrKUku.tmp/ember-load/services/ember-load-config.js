define('ember-load/services/ember-load-config', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  const Service = Ember.Service;
  exports.default = Service.extend({});
});