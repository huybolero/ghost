define('ember-scrollable/util/css', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  const isEmpty = Ember.isEmpty,
        htmlSafe = Ember.String.htmlSafe;


  function styleify(obj) {
    if (isEmpty(obj)) {
      return htmlSafe('');
    }
    const styles = Object.keys(obj).reduce((styleString, key) => {
      const styleValue = obj[key];
      if (!isEmpty(styleValue)) {
        styleString += `${key}: ${styleValue}; `;
      }
      return styleString;
    }, '');
    return htmlSafe(styles);
  }

  exports.styleify = styleify;
});