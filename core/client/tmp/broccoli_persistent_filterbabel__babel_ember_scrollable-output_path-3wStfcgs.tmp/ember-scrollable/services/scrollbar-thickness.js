define('ember-scrollable/services/scrollbar-thickness', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  const computed = Ember.computed,
        Service = Ember.Service,
        $ = Ember.$;
  exports.default = Service.extend({
    thickness: computed(() => {
      const tempEl = $(`
      <div class="scrollbar-width-tester" style="width: 50px; position: absolute; left: -100px;">
        <div style="overflow: scroll;">
          <div class="scrollbar-width-tester__inner"></div>
        </div>
      </div>
    `);
      $('body').append(tempEl);
      const width = $(tempEl).width();
      const widthMinusScrollbars = $('.scrollbar-width-tester__inner', tempEl).width();
      tempEl.remove();

      return width - widthMinusScrollbars;
    })
  });
});