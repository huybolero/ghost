define("koenig-editor/templates/components/koenig-card-hr", ["exports"], function (exports) {
  "use strict";

  exports.__esModule = true;
  exports.default = Ember.HTMLBars.template({ "id": "g+ahpVoW", "block": "{\"symbols\":[],\"statements\":[[4,\"koenig-card\",null,[[\"class\",\"isSelected\",\"isEditing\",\"selectCard\",\"editCard\",\"hasEditMode\"],[\"kg-card-hover\",[22,[\"isSelected\"]],[22,[\"isEditing\"]],[26,\"action\",[[21,0,[]],[22,[\"selectCard\"]]],null],[26,\"action\",[[21,0,[]],[22,[\"editCard\"]]],null],false]],{\"statements\":[[0,\"    \"],[6,\"hr\"],[8],[9],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "koenig-editor/templates/components/koenig-card-hr.hbs" } });
});