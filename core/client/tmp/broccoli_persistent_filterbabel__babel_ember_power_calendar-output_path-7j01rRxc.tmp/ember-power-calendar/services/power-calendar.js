define('ember-power-calendar/services/power-calendar', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Service.extend({
    date: null,

    // Lifecycle hooks
    init() {
      this._super(...arguments);
      this._calendars = {};
    },

    // Methods
    getDate() {
      return this.get('date') || new Date();
    },

    registerCalendar(calendar) {
      this._calendars[Ember.guidFor(calendar)] = calendar;
    },

    unregisterCalendar(calendar) {
      delete this._calendars[Ember.guidFor(calendar)];
    }
  });
});