define('liquid-wormhole/services/liquid-fire-transitions', ['exports', 'liquid-fire/action', 'liquid-fire/running-transition', 'liquid-fire/transition-map'], function (exports, _action, _runningTransition, _transitionMap) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const wormholeActionMap = new WeakMap();

  exports.default = _transitionMap.default.extend({
    transitionFor(conditions) {
      if (conditions.matchContext && conditions.matchContext.helperName === 'liquid-wormhole' || conditions.helperName === 'liquid-wormhole') {

        const versions = conditions.versions;

        conditions.versions = versions.map(version => version.value || version);
        conditions.parentElement = conditions.parentElement.find('.liquid-wormhole-element');
        conditions.firstTime = 'no';

        const rule = this.constraintsFor(conditions).bestMatch(conditions);
        let action;

        if (rule) {
          if (wormholeActionMap.has(rule)) {
            action = wormholeActionMap.get(rule);
          } else {
            action = new _action.default('wormhole', [{ use: rule.use }]);
            action.validateHandler(this);

            wormholeActionMap.set(rule, action);
          }
        } else {
          action = this.defaultAction();
        }

        return new _runningTransition.default(this, versions, action);
      } else {
        return this._super(conditions);
      }
    }
  });
});