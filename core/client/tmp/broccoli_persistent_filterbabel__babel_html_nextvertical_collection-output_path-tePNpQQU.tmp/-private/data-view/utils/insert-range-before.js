export default function insertRangeBefore(parent, element, firstNode, lastNode) {
  var nextNode = void 0;

  while (firstNode) {
    nextNode = firstNode.nextSibling;
    parent.insertBefore(firstNode, element);

    if (firstNode === lastNode) {
      break;
    }

    firstNode = nextNode;
  }
}