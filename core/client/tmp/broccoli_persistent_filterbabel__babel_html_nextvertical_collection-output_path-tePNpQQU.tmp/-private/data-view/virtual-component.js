

var VC_IDENTITY = 0;

export default class VirtualComponent {
  constructor(content = null, index = null) {
    this.id = VC_IDENTITY++;

    this.content = content;
    this.index = index;

    this.upperBound = document.createTextNode('');
    this.lowerBound = document.createTextNode('');
    this.element = null;

    this.rendered = false;

    // In older versions of Ember/IE, binding anything on an object in the template
    // adds observers which creates __ember_meta__
    this.__ember_meta__ = null; // eslint-disable-line camelcase

    if (true && true) {
      Object.preventExtensions(this);
    }
  }

  get realUpperBound() {
    return true ? this.upperBound : this.upperBound.previousSibling;
  }

  get realLowerBound() {
    return true ? this.lowerBound : this.lowerBound.nextSibling;
  }

  getBoundingClientRect() {
    var upperBound = this.upperBound,
        lowerBound = this.lowerBound;


    var top = Infinity;
    var bottom = -Infinity;

    while (upperBound !== lowerBound) {
      upperBound = upperBound.nextSibling;

      if (upperBound instanceof Element) {
        top = Math.min(top, upperBound.getBoundingClientRect().top);
        bottom = Math.max(bottom, upperBound.getBoundingClientRect().bottom);
      }

      if (true) {
        if (upperBound instanceof Element) {
          continue;
        }

        var text = upperBound.textContent;

        (true && !(text === '' || text.match(/^\s+$/)) && Ember.assert(`All content inside of vertical-collection must be wrapped in an element. Detected a text node with content: ${text}`, text === '' || text.match(/^\s+$/)));
      }
    }

    (true && !(top !== Infinity && bottom !== -Infinity) && Ember.assert('Items in a vertical collection require atleast one element in them', top !== Infinity && bottom !== -Infinity));


    var height = bottom - top;

    return { top, bottom, height };
  }

  recycle(newContent, newIndex) {
    (true && !(newContent) && Ember.assert(`You cannot set an item's content to undefined`, newContent));


    if (this.index !== newIndex) {
      Ember.set(this, 'index', newIndex);
    }

    if (this.content !== newContent) {
      Ember.set(this, 'content', newContent);
    }
  }

  destroy() {
    Ember.set(this, 'element', null);
    Ember.set(this, 'upperBound', null);
    Ember.set(this, 'lowerBound', null);
    Ember.set(this, 'content', null);
    Ember.set(this, 'index', null);
  }
}