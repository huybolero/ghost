

export default function estimateElementHeight(element, fallbackHeight) {
  (true && !(fallbackHeight) && Ember.assert(`You called estimateElement height without a fallbackHeight`, fallbackHeight));
  (true && !(element) && Ember.assert(`You called estimateElementHeight without an element`, element));


  if (fallbackHeight.indexOf('%') !== -1) {
    return getPercentageHeight(element, fallbackHeight);
  }

  if (fallbackHeight.indexOf('em') !== -1) {
    return getEmHeight(element, fallbackHeight);
  }

  return parseInt(fallbackHeight, 10);
}

function getPercentageHeight(element, fallbackHeight) {
  // We use offsetHeight here to get the element's true height, rather than the
  // bounding rect which may be scaled with transforms
  var parentHeight = element.offsetHeight;
  var percent = parseFloat(fallbackHeight);

  return percent * parentHeight / 100.0;
}

function getEmHeight(element, fallbackHeight) {
  var fontSizeElement = fallbackHeight.indexOf('rem') !== -1 ? document.documentElement : element;
  var fontSize = window.getComputedStyle(fontSizeElement).getPropertyValue('font-size');

  return parseFloat(fallbackHeight) * parseFloat(fontSize);
}