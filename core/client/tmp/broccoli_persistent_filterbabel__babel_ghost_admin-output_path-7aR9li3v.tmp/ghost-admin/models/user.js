define('ghost-admin/models/user', ['exports', 'ember-data/model', 'ghost-admin/mixins/validation-engine', 'ember-data/attr', 'ember-data/relationships', 'ember-concurrency'], function (exports, _model, _validationEngine, _attr, _relationships, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _model.default.extend(_validationEngine.default, {
        validationType: 'user',

        name: (0, _attr.default)('string'),
        slug: (0, _attr.default)('string'),
        email: (0, _attr.default)('string'),
        profileImage: (0, _attr.default)('string'),
        coverImage: (0, _attr.default)('string'),
        bio: (0, _attr.default)('string'),
        website: (0, _attr.default)('string'),
        location: (0, _attr.default)('string'),
        accessibility: (0, _attr.default)('string'),
        status: (0, _attr.default)('string'),
        locale: (0, _attr.default)('string'),
        metaTitle: (0, _attr.default)('string'),
        metaDescription: (0, _attr.default)('string'),
        lastLoginUTC: (0, _attr.default)('moment-utc'),
        createdAtUTC: (0, _attr.default)('moment-utc'),
        createdBy: (0, _attr.default)('number'),
        updatedAtUTC: (0, _attr.default)('moment-utc'),
        updatedBy: (0, _attr.default)('number'),
        roles: (0, _relationships.hasMany)('role', {
            embedded: 'always',
            async: false
        }),
        count: (0, _attr.default)('raw'),
        facebook: (0, _attr.default)('facebook-url-user'),
        twitter: (0, _attr.default)('twitter-url-user'),
        tour: (0, _attr.default)('json-string'),

        ghostPaths: Ember.inject.service(),
        ajax: Ember.inject.service(),
        session: Ember.inject.service(),
        notifications: Ember.inject.service(),
        config: Ember.inject.service(),

        // TODO: Once client-side permissions are in place,
        // remove the hard role check.
        isContributor: Ember.computed.equal('role.name', 'Contributor'),
        isAuthor: Ember.computed.equal('role.name', 'Author'),
        isEditor: Ember.computed.equal('role.name', 'Editor'),
        isAdmin: Ember.computed.equal('role.name', 'Administrator'),
        isOwner: Ember.computed.equal('role.name', 'Owner'),

        // This is used in enough places that it's useful to throw it here
        isAuthorOrContributor: Ember.computed.or('isAuthor', 'isContributor'),

        isLoggedIn: Ember.computed('id', 'session.user.id', function () {
            return this.get('id') === this.get('session.user.id');
        }),

        isActive: Ember.computed('status', function () {
            // TODO: review "locked" as an "active" status
            return ['active', 'warn-1', 'warn-2', 'warn-3', 'warn-4', 'locked'].indexOf(this.get('status')) > -1;
        }),

        isSuspended: Ember.computed.equal('status', 'inactive'),
        isLocked: Ember.computed.equal('status', 'locked'),

        role: Ember.computed('roles', {
            get() {
                return this.get('roles.firstObject');
            },
            set(key, value) {
                // Only one role per user, so remove any old data.
                this.get('roles').clear();
                this.get('roles').pushObject(value);

                return value;
            }
        }),

        profileImageUrl: Ember.computed('ghostPaths.assetRoot', 'profileImage', function () {
            // keep path separate so asset rewriting correctly picks it up
            let defaultImage = '/img/user-image.png';
            let defaultPath = `${this.ghostPaths.assetRoot.replace(/\/$/, '')}${defaultImage}`;
            return this.profileImage || defaultPath;
        }),

        coverImageUrl: Ember.computed('ghostPaths.assetRoot', 'coverImage', function () {
            // keep path separate so asset rewriting correctly picks it up
            let defaultImage = '/img/user-cover.png';
            let defaultPath = `${this.ghostPaths.assetRoot.replace(/\/$/, '')}${defaultImage}`;
            return this.coverImage || defaultPath;
        }),

        saveNewPassword: (0, _emberConcurrency.task)(function* () {
            let validation = this.get('isLoggedIn') ? 'ownPasswordChange' : 'passwordChange';

            try {
                yield this.validate({ property: validation });
            } catch (e) {
                // validation error, don't do anything
                return;
            }

            try {
                let url = this.get('ghostPaths.url').api('users', 'password');

                yield this.get('ajax').put(url, {
                    data: {
                        password: [{
                            user_id: this.get('id'),
                            oldPassword: this.get('password'),
                            newPassword: this.get('newPassword'),
                            ne2Password: this.get('ne2Password')
                        }]
                    }
                });

                this.setProperties({
                    password: '',
                    newPassword: '',
                    ne2Password: ''
                });

                this.get('notifications').showNotification('Password updated.', { type: 'success', key: 'user.change-password.success' });

                // clear errors manually for ne2password because validation
                // engine only clears the "validated proeprty"
                // TODO: clean up once we have a better validations library
                this.get('errors').remove('ne2Password');

                return true;
            } catch (error) {
                this.get('notifications').showAPIError(error, { key: 'user.change-password' });
            }
        }).drop()
    });
});