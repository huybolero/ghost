define('ghost-admin/models/slack-integration', ['exports', 'ghost-admin/mixins/validation-engine'], function (exports, _validationEngine) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Object.extend(_validationEngine.default, {
        // values entered here will act as defaults
        url: '',

        validationType: 'slackIntegration',

        isActive: Ember.computed('url', function () {
            let url = this.get('url');
            return !Ember.isBlank(url);
        })
    });
});