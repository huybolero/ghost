define('ghost-admin/models/tag', ['exports', 'ember-data/model', 'ghost-admin/mixins/validation-engine', 'ember-data/attr'], function (exports, _model, _validationEngine, _attr) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _model.default.extend(_validationEngine.default, {
        validationType: 'tag',

        name: (0, _attr.default)('string'),
        slug: (0, _attr.default)('string'),
        description: (0, _attr.default)('string'),
        parent: (0, _attr.default)('string'), // unused
        metaTitle: (0, _attr.default)('string'),
        metaDescription: (0, _attr.default)('string'),
        featureImage: (0, _attr.default)('string'),
        visibility: (0, _attr.default)('string', { defaultValue: 'public' }),
        createdAtUTC: (0, _attr.default)('moment-utc'),
        updatedAtUTC: (0, _attr.default)('moment-utc'),
        createdBy: (0, _attr.default)('number'),
        updatedBy: (0, _attr.default)('number'),
        count: (0, _attr.default)('raw'),

        isInternal: Ember.computed.equal('visibility', 'internal'),
        isPublic: Ember.computed.equal('visibility', 'public'),

        feature: Ember.inject.service(),

        updateVisibility() {
            let internalRegex = /^#.?/;
            this.set('visibility', internalRegex.test(this.get('name')) ? 'internal' : 'public');
        },

        save() {
            if (this.get('changedAttributes.name') && !this.get('isDeleted')) {
                this.updateVisibility();
            }
            return this._super(...arguments);
        }
    });
});