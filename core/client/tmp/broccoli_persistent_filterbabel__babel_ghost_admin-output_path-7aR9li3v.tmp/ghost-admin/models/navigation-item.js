define('ghost-admin/models/navigation-item', ['exports', 'ghost-admin/mixins/validation-engine'], function (exports, _validationEngine) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Object.extend(_validationEngine.default, {
        label: '',
        url: '',
        isNew: false,

        validationType: 'navItem',

        isComplete: Ember.computed('label', 'url', function () {
            var _getProperties = this.getProperties('label', 'url');

            let label = _getProperties.label,
                url = _getProperties.url;


            return !Ember.isBlank(label) && !Ember.isBlank(url);
        }),

        isBlank: Ember.computed('label', 'url', function () {
            var _getProperties2 = this.getProperties('label', 'url');

            let label = _getProperties2.label,
                url = _getProperties2.url;


            return Ember.isBlank(label) && Ember.isBlank(url);
        })
    });
});