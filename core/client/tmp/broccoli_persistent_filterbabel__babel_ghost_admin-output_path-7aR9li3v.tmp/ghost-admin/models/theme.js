define('ghost-admin/models/theme', ['exports', 'ember-data/model', 'ember-data/attr'], function (exports, _model, _attr) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _model.default.extend({
        active: (0, _attr.default)('boolean'),
        errors: (0, _attr.default)('raw'),
        name: (0, _attr.default)('string'),
        package: (0, _attr.default)('raw'),
        templates: (0, _attr.default)('raw', { defaultValue: () => [] }),
        warnings: (0, _attr.default)('raw'),

        customTemplates: Ember.computed('templates.[]', function () {
            let templates = this.get('templates') || [];

            return templates.filter(function (template) {
                return Ember.isBlank(template.slug);
            });
        }),

        slugTemplates: Ember.computed('templates.[]', function () {
            let templates = this.get('templates') || [];

            return templates.filter(function (template) {
                return !Ember.isBlank(template.slug);
            });
        }),

        activate() {
            let adapter = this.store.adapterFor(this.constructor.modelName);

            return adapter.activate(this).then(() => {
                // the server only gives us the newly active theme back so we need
                // to manually mark other themes as inactive in the store
                let activeThemes = this.store.peekAll('theme').filterBy('active', true);

                activeThemes.forEach(theme => {
                    if (theme !== this) {
                        // store.push is necessary to avoid dirty records that cause
                        // problems when we get new data back in subsequent requests
                        this.store.push({ data: {
                                id: theme.id,
                                type: 'theme',
                                attributes: { active: false }
                            } });
                    }
                });

                return this;
            });
        }
    });
});