define('ghost-admin/models/post', ['exports', 'ember-data/model', 'ghost-admin/mixins/validation-engine', 'ember-data/attr', 'ghost-admin/utils/bound-one-way', 'moment', 'koenig-editor/components/koenig-editor', 'ember-data/relationships'], function (exports, _model, _validationEngine, _attr, _boundOneWay, _moment, _koenigEditor, _relationships) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.BLANK_MARKDOWN = undefined;
    const Comparable = Ember.Comparable;


    // for our markdown-only editor we need to build a blank mobiledoc
    const MOBILEDOC_VERSION = '0.3.1';
    const BLANK_MARKDOWN = exports.BLANK_MARKDOWN = {
        version: MOBILEDOC_VERSION,
        markups: [],
        atoms: [],
        cards: [['card-markdown', {
            cardName: 'card-markdown',
            markdown: ''
        }]],
        sections: [[10, 0]]
    };

    function statusCompare(postA, postB) {
        let status1 = postA.get('status');
        let status2 = postB.get('status');

        // if any of those is empty
        if (!status1 && !status2) {
            return 0;
        }

        if (!status1 && status2) {
            return -1;
        }

        if (!status2 && status1) {
            return 1;
        }

        // We have to make sure, that scheduled posts will be listed first
        // after that, draft and published will be sorted alphabetically and don't need
        // any manual comparison.

        if (status1 === 'scheduled' && (status2 === 'draft' || status2 === 'published')) {
            return -1;
        }

        if (status2 === 'scheduled' && (status1 === 'draft' || status1 === 'published')) {
            return 1;
        }

        return Ember.compare(status1.valueOf(), status2.valueOf());
    }

    function publishedAtCompare(postA, postB) {
        let published1 = postA.get('publishedAtUTC');
        let published2 = postB.get('publishedAtUTC');

        if (!published1 && !published2) {
            return 0;
        }

        if (!published1 && published2) {
            return -1;
        }

        if (!published2 && published1) {
            return 1;
        }

        return Ember.compare(published1.valueOf(), published2.valueOf());
    }

    exports.default = _model.default.extend(Comparable, _validationEngine.default, {
        config: Ember.inject.service(),
        feature: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),
        clock: Ember.inject.service(),
        settings: Ember.inject.service(),

        validationType: 'post',

        createdAtUTC: (0, _attr.default)('moment-utc'),
        customExcerpt: (0, _attr.default)('string'),
        featured: (0, _attr.default)('boolean', { defaultValue: false }),
        featureImage: (0, _attr.default)('string'),
        codeinjectionFoot: (0, _attr.default)('string', { defaultValue: '' }),
        codeinjectionHead: (0, _attr.default)('string', { defaultValue: '' }),
        customTemplate: (0, _attr.default)('string'),
        ogImage: (0, _attr.default)('string'),
        ogTitle: (0, _attr.default)('string'),
        ogDescription: (0, _attr.default)('string'),
        twitterImage: (0, _attr.default)('string'),
        twitterTitle: (0, _attr.default)('string'),
        twitterDescription: (0, _attr.default)('string'),
        html: (0, _attr.default)('string'),
        locale: (0, _attr.default)('string'),
        metaDescription: (0, _attr.default)('string'),
        metaTitle: (0, _attr.default)('string'),
        mobiledoc: (0, _attr.default)('json-string'),
        page: (0, _attr.default)('boolean', { defaultValue: false }),
        plaintext: (0, _attr.default)('string'),
        publishedAtUTC: (0, _attr.default)('moment-utc'),
        slug: (0, _attr.default)('string'),
        status: (0, _attr.default)('string', { defaultValue: 'draft' }),
        title: (0, _attr.default)('string', { defaultValue: '' }),
        updatedAtUTC: (0, _attr.default)('moment-utc'),
        updatedBy: (0, _attr.default)('number'),
        url: (0, _attr.default)('string'),
        uuid: (0, _attr.default)('string'),

        authors: (0, _relationships.hasMany)('user', {
            embedded: 'always',
            async: false
        }),
        createdBy: (0, _relationships.belongsTo)('user', { async: true }),
        publishedBy: (0, _relationships.belongsTo)('user', { async: true }),
        tags: (0, _relationships.hasMany)('tag', {
            embedded: 'always',
            async: false
        }),

        primaryAuthor: Ember.computed('authors.[]', function () {
            return this.get('authors.firstObject');
        }),

        init() {
            // HACK: we can't use the defaultValue property on the mobiledoc attr
            // because it won't have access to `this` for the feature check so we do
            // it manually here instead
            if (!this.get('mobiledoc')) {
                let defaultValue;

                if (this.get('feature.koenigEditor')) {
                    defaultValue = Ember.copy(_koenigEditor.BLANK_DOC, true);
                } else {
                    defaultValue = Ember.copy(BLANK_MARKDOWN, true);
                }

                // using this.set() adds the property to the changedAttributes list
                // which means the editor always sees new posts as dirty. By setting
                // the internal model data property first it's not seen as having
                // changed so the changedAttributes key is removed
                this._internalModel._data.mobiledoc = defaultValue;
                this.set('mobiledoc', defaultValue);
            }

            this._super(...arguments);
        },

        scratch: null,
        titleScratch: null,

        // HACK: used for validation so that date/time can be validated based on
        // eventual status rather than current status
        statusScratch: null,

        // For use by date/time pickers - will be validated then converted to UTC
        // on save. Updated by an observer whenever publishedAtUTC changes.
        // Everything that revolves around publishedAtUTC only cares about the saved
        // value so this should be almost entirely internal
        publishedAtBlogDate: '',
        publishedAtBlogTime: '',

        customExcerptScratch: (0, _boundOneWay.default)('customExcerpt'),
        codeinjectionFootScratch: (0, _boundOneWay.default)('codeinjectionFoot'),
        codeinjectionHeadScratch: (0, _boundOneWay.default)('codeinjectionHead'),
        metaDescriptionScratch: (0, _boundOneWay.default)('metaDescription'),
        metaTitleScratch: (0, _boundOneWay.default)('metaTitle'),
        ogDescriptionScratch: (0, _boundOneWay.default)('ogDescription'),
        ogTitleScratch: (0, _boundOneWay.default)('ogTitle'),
        twitterDescriptionScratch: (0, _boundOneWay.default)('twitterDescription'),
        twitterTitleScratch: (0, _boundOneWay.default)('twitterTitle'),

        isPublished: Ember.computed.equal('status', 'published'),
        isDraft: Ember.computed.equal('status', 'draft'),
        internalTags: Ember.computed.filterBy('tags', 'isInternal', true),
        isScheduled: Ember.computed.equal('status', 'scheduled'),

        absoluteUrl: Ember.computed('url', 'ghostPaths.url', 'config.blogUrl', function () {
            let blogUrl = this.get('config.blogUrl');
            let postUrl = this.get('url');
            return this.get('ghostPaths.url').join(blogUrl, postUrl);
        }),

        previewUrl: Ember.computed('uuid', 'ghostPaths.url', 'config.blogUrl', function () {
            let blogUrl = this.get('config.blogUrl');
            let uuid = this.get('uuid');
            // routeKeywords.preview: 'p'
            let previewKeyword = 'p';
            // New posts don't have a preview
            if (!uuid) {
                return '';
            }
            return this.get('ghostPaths.url').join(blogUrl, previewKeyword, uuid);
        }),

        // check every second to see if we're past the scheduled time
        // will only re-compute if this property is being observed elsewhere
        pastScheduledTime: Ember.computed('isScheduled', 'publishedAtUTC', 'clock.second', function () {
            if (this.get('isScheduled')) {
                let now = _moment.default.utc();
                let publishedAtUTC = this.get('publishedAtUTC') || now;
                let pastScheduledTime = publishedAtUTC.diff(now, 'hours', true) < 0;

                // force a recompute
                this.get('clock.second');

                return pastScheduledTime;
            } else {
                return false;
            }
        }),

        publishedAtBlogTZ: Ember.computed('publishedAtBlogDate', 'publishedAtBlogTime', 'settings.activeTimezone', {
            get() {
                return this._getPublishedAtBlogTZ();
            },
            set(key, value) {
                let momentValue = value ? (0, _moment.default)(value) : null;
                this._setPublishedAtBlogStrings(momentValue);
                return this._getPublishedAtBlogTZ();
            }
        }),

        _getPublishedAtBlogTZ() {
            let publishedAtUTC = this.get('publishedAtUTC');
            let publishedAtBlogDate = this.get('publishedAtBlogDate');
            let publishedAtBlogTime = this.get('publishedAtBlogTime');
            let blogTimezone = this.get('settings.activeTimezone');

            if (!publishedAtUTC && Ember.isBlank(publishedAtBlogDate) && Ember.isBlank(publishedAtBlogTime)) {
                return null;
            }

            if (publishedAtBlogDate && publishedAtBlogTime) {
                let publishedAtBlog = _moment.default.tz(`${publishedAtBlogDate} ${publishedAtBlogTime}`, blogTimezone);

                /**
                 * Note:
                 * If you create a post and publish it, we send seconds to the database.
                 * If you edit the post afterwards, ember would send the date without seconds, because
                 * the `publishedAtUTC` is based on `publishedAtBlogTime`, which is only in seconds.
                 * The date time picker doesn't use seconds.
                 *
                 * This condition prevents the case:
                 *   - you edit a post, but you don't change the published_at time
                 *   - we keep the original date with seconds
                 *
                 * See https://github.com/TryGhost/Ghost/issues/8603#issuecomment-309538395.
                 */
                if (publishedAtUTC && publishedAtBlog.diff(publishedAtUTC.clone().startOf('minutes')) === 0) {
                    return publishedAtUTC;
                }

                return publishedAtBlog;
            } else {
                return _moment.default.tz(this.get('publishedAtUTC'), blogTimezone);
            }
        },

        // TODO: is there a better way to handle this?
        // eslint-disable-next-line ghost/ember/no-observers
        _setPublishedAtBlogTZ: Ember.observer('publishedAtUTC', 'settings.activeTimezone', function () {
            let publishedAtUTC = this.get('publishedAtUTC');
            this._setPublishedAtBlogStrings(publishedAtUTC);
        }).on('init'),

        _setPublishedAtBlogStrings(momentDate) {
            if (momentDate) {
                let blogTimezone = this.get('settings.activeTimezone');
                let publishedAtBlog = _moment.default.tz(momentDate, blogTimezone);

                this.set('publishedAtBlogDate', publishedAtBlog.format('YYYY-MM-DD'));
                this.set('publishedAtBlogTime', publishedAtBlog.format('HH:mm'));
            } else {
                this.set('publishedAtBlogDate', '');
                this.set('publishedAtBlogTime', '');
            }
        },

        // remove client-generated tags, which have `id: null`.
        // Ember Data won't recognize/update them automatically
        // when returned from the server with ids.
        // https://github.com/emberjs/data/issues/1829
        updateTags() {
            let tags = this.get('tags');
            let oldTags = tags.filterBy('id', null);

            tags.removeObjects(oldTags);
            oldTags.invoke('deleteRecord');
        },

        isAuthoredByUser(user) {
            return this.get('authors').includes(user);
        },

        // a custom sort function is needed in order to sort the posts list the same way the server would:
        //     status: scheduled, draft, published
        //     publishedAt: DESC
        //     updatedAt: DESC
        //     id: DESC
        compare(postA, postB) {
            let updated1 = postA.get('updatedAtUTC');
            let updated2 = postB.get('updatedAtUTC');
            let idResult, publishedAtResult, statusResult, updatedAtResult;

            // when `updatedAt` is undefined, the model is still
            // being written to with the results from the server
            if (postA.get('isNew') || !updated1) {
                return -1;
            }

            if (postB.get('isNew') || !updated2) {
                return 1;
            }

            // TODO: revisit the ID sorting because we no longer have auto-incrementing IDs
            idResult = Ember.compare(postA.get('id'), postB.get('id'));
            statusResult = statusCompare(postA, postB);
            updatedAtResult = Ember.compare(updated1.valueOf(), updated2.valueOf());
            publishedAtResult = publishedAtCompare(postA, postB);

            if (statusResult === 0) {
                if (publishedAtResult === 0) {
                    if (updatedAtResult === 0) {
                        // This should be DESC
                        return idResult * -1;
                    }
                    // This should be DESC
                    return updatedAtResult * -1;
                }
                // This should be DESC
                return publishedAtResult * -1;
            }

            return statusResult;
        },

        // this is a hook added by the ValidationEngine mixin and is called after
        // successful validation and before this.save()
        //
        // the publishedAtBlog{Date/Time} strings are set separately so they can be
        // validated, grab that time if it exists and set the publishedAtUTC
        beforeSave() {
            let publishedAtBlogTZ = this.get('publishedAtBlogTZ');
            let publishedAtUTC = publishedAtBlogTZ ? publishedAtBlogTZ.utc() : null;
            this.set('publishedAtUTC', publishedAtUTC);
        },

        // the markdown editor expects a very specific mobiledoc format, if it
        // doesn't match then we'll need to handle it by forcing Koenig
        isCompatibleWithMarkdownEditor() {
            let mobiledoc = this.get('mobiledoc');

            if (mobiledoc && mobiledoc.markups.length === 0 && mobiledoc.cards.length === 1 && mobiledoc.cards[0][0] === 'card-markdown' && mobiledoc.sections.length === 1 && mobiledoc.sections[0].length === 2 && mobiledoc.sections[0][0] === 10 && mobiledoc.sections[0][1] === 0) {
                return true;
            }

            return false;
        }
    });
});