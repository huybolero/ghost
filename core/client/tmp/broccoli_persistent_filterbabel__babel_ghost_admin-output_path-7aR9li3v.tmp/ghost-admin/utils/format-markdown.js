define('ghost-admin/utils/format-markdown', ['exports', 'ghost-admin/utils/caja-sanitizers', 'npm:markdown-it', 'npm:markdown-it-footnote', 'npm:markdown-it-lazy-headers', 'npm:markdown-it-mark'], function (exports, _cajaSanitizers, _npmMarkdownIt, _npmMarkdownItFootnote, _npmMarkdownItLazyHeaders, _npmMarkdownItMark) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = formatMarkdown;


    let slugify = function slugify(inputString, usedHeaders) {
        let slug = inputString.replace(/[^\w]/g, '').toLowerCase();
        if (usedHeaders[slug]) {
            usedHeaders[slug] += 1;
            slug += usedHeaders[slug];
        }
        return slug;
    };

    // originally from https://github.com/leff/markdown-it-named-headers
    // moved here to avoid pulling in http://stringjs.com dependency
    /* global html_sanitize */
    let markdownitNamedHeaders = function markdownitNamedHeaders(md) {
        let originalHeadingOpen = md.renderer.rules.heading_open;

        // eslint-disable-next-line camelcase
        md.renderer.rules.heading_open = function (tokens, idx, options, env, self) {
            let usedHeaders = {};

            tokens[idx].attrs = tokens[idx].attrs || [];

            let title = tokens[idx + 1].children.reduce(function (acc, t) {
                return acc + t.content;
            }, '');

            let slug = slugify(title, usedHeaders);
            tokens[idx].attrs.push(['id', slug]);

            if (originalHeadingOpen) {
                return originalHeadingOpen.apply(this, arguments);
            } else {
                return self.renderToken(...arguments);
            }
        };
    };

    let md = (0, _npmMarkdownIt.default)({
        html: true,
        breaks: true,
        linkify: true
    }).use(_npmMarkdownItFootnote.default).use(_npmMarkdownItLazyHeaders.default).use(_npmMarkdownItMark.default).use(markdownitNamedHeaders);

    // configure linkify-it
    md.linkify.set({
        fuzzyLink: false
    });

    function formatMarkdown(_markdown, replaceJS = true) {
        let markdown = _markdown || '';
        let escapedhtml = '';

        // convert markdown to HTML
        escapedhtml = md.render(markdown);

        // replace script and iFrame
        if (replaceJS) {
            escapedhtml = escapedhtml.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '<pre class="js-embed-placeholder">Embedded JavaScript</pre>');
            escapedhtml = escapedhtml.replace(/<iframe\b[^<]*(?:(?!<\/iframe>)<[^<]*)*<\/iframe>/gi, '<pre class="iframe-embed-placeholder">Embedded iFrame</pre>');
        }

        // sanitize html
        escapedhtml = html_sanitize(escapedhtml, _cajaSanitizers.default.url, _cajaSanitizers.default.id);

        return escapedhtml;
    }
});