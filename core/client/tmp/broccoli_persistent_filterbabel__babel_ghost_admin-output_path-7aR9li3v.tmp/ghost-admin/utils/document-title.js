define('ghost-admin/utils/document-title', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    exports.default = function () {
        Ember.Route.reopen({
            // `titleToken` can either be a static string or a function
            // that accepts a model object and returns a string (or array
            // of strings if there are multiple tokens).
            titleToken: null,

            // `title` can either be a static string or a function
            // that accepts an array of tokens and returns a string
            // that will be the document title. The `collectTitleTokens` action
            // stops bubbling once a route is encountered that has a `title`
            // defined.
            title: null,

            actions: {
                collectTitleTokens(tokens) {
                    let titleToken = this.titleToken;

                    let finalTitle;

                    if (typeof this.titleToken === 'function') {
                        titleToken = this.titleToken(this.currentModel);
                    }

                    if (Ember.isArray(titleToken)) {
                        tokens.unshift(...titleToken);
                    } else if (titleToken) {
                        tokens.unshift(titleToken);
                    }

                    if (this.title) {
                        if (typeof this.title === 'function') {
                            finalTitle = this.title(tokens);
                        } else {
                            finalTitle = this.title;
                        }

                        window.document.title = finalTitle;
                    } else {
                        return true;
                    }
                }
            }
        });

        Ember.Router.reopen({
            updateTitle: Ember.on('didTransition', function () {
                this.send('collectTitleTokens', []);
            })
        });
    };
});