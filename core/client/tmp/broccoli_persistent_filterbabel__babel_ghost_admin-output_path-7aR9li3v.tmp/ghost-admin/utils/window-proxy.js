define("ghost-admin/utils/window-proxy", ["exports"], function (exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = {
        changeLocation(url) {
            window.location = url;
        },

        replaceLocation(url) {
            window.location.replace(url);
        },

        replaceState(params, title, url) {
            window.history.replaceState(params, title, url);
        }
    };
});