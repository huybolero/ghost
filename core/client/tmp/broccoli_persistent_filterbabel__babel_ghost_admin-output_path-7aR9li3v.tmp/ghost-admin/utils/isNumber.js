define('ghost-admin/utils/isNumber', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (value) {
    return typeof value === 'number' || value && typeof value === 'object' && toString.call(value) === '[object Number]' || false;
  };

  const toString = Object.prototype.toString;
});