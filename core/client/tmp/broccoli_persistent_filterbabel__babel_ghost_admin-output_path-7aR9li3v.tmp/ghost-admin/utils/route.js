define('ghost-admin/utils/route', [], function () {
    'use strict';

    Ember.Route.reopen({
        actions: {
            willTransition(transition) {
                if (this.get('upgradeStatus.isRequired')) {
                    transition.abort();
                    this.get('upgradeStatus').requireUpgrade();
                    return false;
                } else {
                    return true;
                }
            }
        }
    });
});