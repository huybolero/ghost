define('ghost-admin/utils/ghost-paths', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    exports.default = function () {
        let path = window.location.pathname;
        let subdir = path.substr(0, path.search('/ghost/'));
        let adminRoot = `${subdir}/ghost/`;
        let assetRoot = `${subdir}/ghost/assets/`;
        let apiRoot = `${subdir}/ghost/api/v0.1`;

        function assetUrl(src) {
            return subdir + src;
        }

        return {
            adminRoot,
            assetRoot,
            apiRoot,
            subdir,
            blogRoot: `${subdir}/`,
            count: 'https://count.ghost.org/',

            url: {
                admin() {
                    return makeRoute(adminRoot, arguments);
                },

                api() {
                    return makeRoute(apiRoot, arguments);
                },

                join() {
                    if (arguments.length > 1) {
                        return makeRoute(arguments[0], Array.prototype.slice.call(arguments, 1));
                    } else if (arguments.length === 1) {
                        var _arguments = Array.prototype.slice.call(arguments);

                        let arg = _arguments[0];

                        return arg.slice(-1) === '/' ? arg : `${arg}/`;
                    }
                    return '/';
                },

                asset: assetUrl
            }
        };
    };

    let makeRoute = function makeRoute(root, args) {
        let slashAtStart = /^\//;
        let slashAtEnd = /\/$/;
        let parts = Array.prototype.slice.call(args, 0);
        let route = root.replace(slashAtEnd, '');

        parts.forEach(part => {
            if (part) {
                route = [route, part.replace(slashAtStart, '').replace(slashAtEnd, '')].join('/');
            }
        });

        return route += '/';
    };
});