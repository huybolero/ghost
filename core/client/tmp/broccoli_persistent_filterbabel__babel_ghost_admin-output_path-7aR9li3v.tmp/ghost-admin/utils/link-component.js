define('ghost-admin/utils/link-component', [], function () {
    'use strict';

    Ember.LinkComponent.reopen({
        active: Ember.computed('attrs.params', '_routing.currentState', function () {
            let isActive = this._super(...arguments);

            if (typeof this.get('alternateActive') === 'function') {
                this.get('alternateActive')(isActive);
            }

            return isActive;
        }),

        activeClass: Ember.computed('tagName', function () {
            return this.get('tagName') === 'button' ? '' : 'active';
        })
    });
});