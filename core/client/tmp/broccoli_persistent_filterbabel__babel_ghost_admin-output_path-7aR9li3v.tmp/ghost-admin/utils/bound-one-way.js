define('ghost-admin/utils/bound-one-way', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    exports.default = function (upstream, transform) {
        if (typeof transform !== 'function') {
            // default to the identity function
            transform = function transform(value) {
                return value;
            };
        }

        return Ember.computed(upstream, {
            get() {
                return transform(this.get(upstream));
            },
            set(key, value) {
                return value;
            }
        });
    };
});