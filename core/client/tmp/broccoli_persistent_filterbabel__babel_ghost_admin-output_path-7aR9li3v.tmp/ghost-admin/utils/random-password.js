define('ghost-admin/utils/random-password', ['exports', 'npm:password-generator'], function (exports, _npmPasswordGenerator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    exports.default = function () {
        let word = (0, _npmPasswordGenerator.default)(6);
        let randomN = Math.floor(Math.random() * 1000);

        return word + randomN;
    };
});