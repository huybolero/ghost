define('ghost-admin/authenticators/oauth2', ['exports', 'ember-simple-auth/authenticators/oauth2-password-grant'], function (exports, _oauth2PasswordGrant) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _oauth2PasswordGrant.default.extend({
        ajax: Ember.inject.service(),
        session: Ember.inject.service(),
        config: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),

        init() {
            this._super(...arguments);

            let handler = Ember.run.bind(this, () => {
                this.onOnline();
            });
            window.addEventListener('online', handler);
        },

        serverTokenEndpoint: Ember.computed('ghostPaths.apiRoot', function () {
            return `${this.get('ghostPaths.apiRoot')}/authentication/token`;
        }),

        // disable general token revocation because the requests will always 401
        // (revocation is triggered by invalid access token so it's already invalid)
        // we have a separate logout procedure that sends revocation requests
        serverTokenRevocationEndpoint: null,

        makeRequest(url, data) {
            /* eslint-disable camelcase */
            data.client_id = this.get('config.clientId');
            data.client_secret = this.get('config.clientSecret');
            /* eslint-enable camelcase */

            let options = {
                data,
                dataType: 'json',
                contentType: 'application/x-www-form-urlencoded'
            };

            return this.get('ajax').post(url, options);
        },

        /**
         * Invoked when "navigator.online" event is trigerred.
         * This is a helper function to handle intermittent internet connectivity. Token is refreshed
         * when browser status becomes "online".
         */
        onOnline() {
            if (this.get('session.isAuthenticated')) {
                let autoRefresh = this.get('refreshAccessTokens');
                if (autoRefresh) {
                    let expiresIn = this.get('session.data.authenticated.expires_in');
                    let token = this.get('session.data.authenticated.refresh_token');
                    return this._refreshAccessToken(expiresIn, token);
                }
            }
        },

        authenticate(identification, password, scope = [], headers = {}) {
            return new Ember.RSVP.Promise((resolve, reject) => {
                let data = { grant_type: 'password', username: identification, password };
                let serverTokenEndpoint = this.get('serverTokenEndpoint');
                let scopesString = Ember.makeArray(scope).join(' ');
                if (!Ember.isEmpty(scopesString)) {
                    data.scope = scopesString;
                }
                this.makeRequest(serverTokenEndpoint, data, headers).then(response => {
                    Ember.run(() => {
                        /* eslint-disable camelcase */
                        let expiresAt = this._absolutizeExpirationTime(response.expires_in);
                        this._scheduleAccessTokenRefresh(response.expires_in, expiresAt, response.refresh_token);
                        /* eslint-enable camelcase */

                        if (!Ember.isEmpty(expiresAt)) {
                            response = Ember.assign(response, { expires_at: expiresAt });
                        }

                        resolve(response);
                    });
                }, error => {
                    reject(error);
                });
            });
        }
    });
});