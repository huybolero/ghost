define('ghost-admin/initializers/trailing-hash', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    let trailingHash = Ember.HashLocation.extend({
        formatURL() {
            let url = this._super(...arguments);

            if (url.indexOf('?') > 0) {
                return url.replace(/([^/])\?/, '$1/?');
            } else {
                return url.replace(/\/?$/, '/');
            }
        }
    });

    exports.default = {
        name: 'registerTrailingHashLocation',

        initialize(application) {
            application.register('location:trailing-hash', trailingHash);
        }
    };
});