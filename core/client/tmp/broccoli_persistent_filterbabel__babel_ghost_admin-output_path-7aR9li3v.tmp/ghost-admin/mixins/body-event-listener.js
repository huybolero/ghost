define('ghost-admin/mixins/body-event-listener', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    function K() {
        return this;
    }

    // Code modified from Addepar/ember-widgets
    // https://github.com/Addepar/ember-widgets/blob/master/src/mixins.coffee#L39

    exports.default = Ember.Mixin.create({
        bodyElementSelector: 'html',
        bodyClick: K,

        init() {
            this._super(...arguments);

            return Ember.run.next(this, this._setupDocumentHandlers);
        },

        willDestroy() {
            this._super(...arguments);

            return this._removeDocumentHandlers();
        },

        _setupDocumentHandlers() {
            if (this._clickHandler) {
                return;
            }

            this._clickHandler = event => this.bodyClick(event);

            return Ember.$(this.get('bodyElementSelector')).on('click', this._clickHandler);
        },

        _removeDocumentHandlers() {
            Ember.$(this.get('bodyElementSelector')).off('click', this._clickHandler);
            this._clickHandler = null;
        },

        // http://stackoverflow.com/questions/152975/how-to-detect-a-click-outside-an-element
        click(event) {
            return event.stopPropagation();
        }
    });
});