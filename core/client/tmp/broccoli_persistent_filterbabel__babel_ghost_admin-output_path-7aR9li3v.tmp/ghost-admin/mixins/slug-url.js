define('ghost-admin/mixins/slug-url', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create({
        buildURL(_modelName, _id, _snapshot, _requestType, query) {
            let url = this._super(...arguments);

            if (query && !Ember.isBlank(query.slug)) {
                url += `slug/${query.slug}/`;
                delete query.slug;
            }

            return url;
        }
    });
});