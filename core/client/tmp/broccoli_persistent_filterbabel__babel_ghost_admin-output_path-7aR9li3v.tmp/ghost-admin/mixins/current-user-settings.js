define('ghost-admin/mixins/current-user-settings', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create({
        transitionAuthor() {
            return user => {
                if (user.get('isAuthorOrContributor')) {
                    return this.transitionTo('team.user', user);
                }

                return user;
            };
        },

        transitionEditor() {
            return user => {
                if (user.get('isEditor')) {
                    return this.transitionTo('team');
                }

                return user;
            };
        }
    });
});