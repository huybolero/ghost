define('ghost-admin/mixins/validation-state', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create({

        errors: null,
        property: '',
        hasValidated: Ember.A(),

        hasError: false,

        setHasError() {
            let property = this.get('property');
            let errors = this.get('errors');
            let hasValidated = this.get('hasValidated');

            // if we aren't looking at a specific property we always want an error class
            if (!property && errors && !errors.get('isEmpty')) {
                this.set('hasError', true);
                return;
            }

            // If we haven't yet validated this field, there is no validation class needed
            if (!hasValidated || !hasValidated.includes(property)) {
                this.set('hasError', false);
                return;
            }

            if (errors && !Ember.isEmpty(errors.errorsFor(property))) {
                this.set('hasError', true);
                return;
            }

            this.set('hasError', false);
        },

        // eslint-disable-next-line ghost/ember/no-observers
        hasErrorObserver: Ember.observer('errors.[]', 'property', 'hasValidated.[]', function () {
            Ember.run.once(this, 'setHasError');
            // this.setHasError();
        }).on('init')

    });
});