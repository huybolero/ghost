define('ghost-admin/mixins/style-body', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create({
        activate() {
            let cssClasses = this.get('classNames');

            this._super(...arguments);

            if (cssClasses) {
                Ember.run.schedule('afterRender', null, function () {
                    cssClasses.forEach(curClass => {
                        Ember.$('body').addClass(curClass);
                    });
                });
            }
        },

        deactivate() {
            let cssClasses = this.get('classNames');

            this._super(...arguments);

            Ember.run.schedule('afterRender', null, function () {
                cssClasses.forEach(curClass => {
                    Ember.$('body').removeClass(curClass);
                });
            });
        }
    });
});