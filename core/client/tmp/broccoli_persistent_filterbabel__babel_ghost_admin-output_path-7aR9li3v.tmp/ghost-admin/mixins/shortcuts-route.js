define('ghost-admin/mixins/shortcuts-route', ['exports', 'ghost-admin/mixins/shortcuts'], function (exports, _shortcuts) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create(_shortcuts.default, {
        activate() {
            this._super(...arguments);
            this.registerShortcuts();
        },

        deactivate() {
            this._super(...arguments);
            this.removeShortcuts();
        }
    });
});