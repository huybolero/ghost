define('ghost-admin/mixins/dropdown-mixin', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create(Ember.Evented, {
        classNameBindings: ['isOpen:open:closed'],
        isOpen: false,

        click(event) {
            this._super(event);

            return event.stopPropagation();
        }
    });
});