define('ghost-admin/mixins/settings-menu-component', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Mixin.create({
        showSettingsMenu: false,

        isViewingSubview: Ember.computed('showSettingsMenu', {
            get() {
                return false;
            },
            set(key, value) {
                // Not viewing a subview if we can't even see the PSM
                if (!this.get('showSettingsMenu')) {
                    return false;
                }
                return value;
            }
        }),

        actions: {
            showSubview() {
                this.set('isViewingSubview', true);
            },

            closeSubview() {
                this.set('isViewingSubview', false);
            }
        }
    });
});