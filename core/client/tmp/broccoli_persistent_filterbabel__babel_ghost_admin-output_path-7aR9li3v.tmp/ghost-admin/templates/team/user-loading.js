define("ghost-admin/templates/team/user-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "cbBxTvD9", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[10,\"class\",\"gh-canvas\"],[8],[0,\"\\n    \"],[6,\"header\"],[10,\"class\",\"gh-canvas-header\"],[8],[0,\"\\n        \"],[6,\"h2\"],[10,\"class\",\"gh-canvas-title\"],[10,\"data-test-screen-title\",\"\"],[8],[0,\"\\n            \"],[4,\"link-to\",[\"team\"],[[\"data-test-team-link\"],[true]],{\"statements\":[[0,\"Team\"]],\"parameters\":[]},null],[0,\"\\n            \"],[6,\"span\"],[8],[1,[26,\"svg-jar\",[\"arrow-right\"],null],false],[9],[0,\"\\n            \"],[1,[22,[\"user\",\"name\"]],false],[0,\"\\n        \"],[9],[0,\"\\n\\n        \"],[6,\"section\"],[10,\"class\",\"view-actions\"],[8],[0,\"\\n            \"],[6,\"div\"],[10,\"class\",\"gh-btn gh-btn-blue\"],[8],[6,\"span\"],[8],[0,\"Save\"],[9],[9],[0,\"\\n        \"],[9],[0,\"\\n    \"],[9],[0,\"\\n\\n    \"],[6,\"div\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/team/user-loading.hbs" } });
});