define("ghost-admin/templates/team/index-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "tUpas101", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[10,\"class\",\"gh-canvas\"],[8],[0,\"\\n    \"],[6,\"header\"],[10,\"class\",\"gh-canvas-header\"],[8],[0,\"\\n        \"],[6,\"h2\"],[10,\"class\",\"gh-canvas-title\"],[10,\"data-test-screen-title\",\"\"],[8],[0,\"Team members\"],[9],[0,\"\\n\"],[4,\"unless\",[[22,[\"session\",\"user\",\"isAuthorOrContributor\"]]],null,{\"statements\":[[0,\"            \"],[6,\"section\"],[10,\"class\",\"view-actions\"],[8],[0,\"\\n                \"],[6,\"button\"],[10,\"class\",\"gh-btn gh-btn-green\"],[3,\"action\",[[21,0,[]],\"toggleInviteUserModal\"]],[8],[6,\"span\"],[8],[0,\"Invite People\"],[9],[9],[0,\"\\n            \"],[9],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"    \"],[9],[0,\"\\n\\n    \"],[6,\"div\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/team/index-loading.hbs" } });
});