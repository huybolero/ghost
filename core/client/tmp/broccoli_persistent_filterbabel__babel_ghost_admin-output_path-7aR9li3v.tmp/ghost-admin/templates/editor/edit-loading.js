define("ghost-admin/templates/editor/edit-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "2rLhVroj", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[10,\"class\",\"gh-view\"],[8],[0,\"\\n    \"],[6,\"div\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/editor/edit-loading.hbs" } });
});