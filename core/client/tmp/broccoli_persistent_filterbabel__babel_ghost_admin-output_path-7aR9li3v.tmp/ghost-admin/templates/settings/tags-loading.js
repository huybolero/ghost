define("ghost-admin/templates/settings/tags-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "BSJd2Kh6", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[10,\"class\",\"gh-view\"],[8],[0,\"\\n    \"],[6,\"header\"],[10,\"class\",\"view-header\"],[8],[0,\"\\n        \"],[4,\"gh-view-title\",null,null,{\"statements\":[[6,\"span\"],[8],[0,\"Tags\"],[9]],\"parameters\":[]},null],[0,\"\\n        \"],[6,\"section\"],[10,\"class\",\"view-actions\"],[8],[0,\"\\n            \"],[4,\"link-to\",[\"settings.tags.new\"],[[\"class\"],[\"gh-btn gh-btn-green\"]],{\"statements\":[[6,\"span\"],[8],[0,\"New Tag\"],[9]],\"parameters\":[]},null],[0,\"\\n        \"],[9],[0,\"\\n    \"],[9],[0,\"\\n\\n    \"],[6,\"div\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/settings/tags-loading.hbs" } });
});