define("ghost-admin/templates/settings/tags/tag", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "Alw9FYwc", "block": "{\"symbols\":[],\"statements\":[[1,[26,\"gh-tag-settings-form\",null,[[\"tag\",\"setProperty\",\"showDeleteTagModal\"],[[22,[\"tag\"]],[26,\"action\",[[21,0,[]],\"setProperty\"],null],[26,\"action\",[[21,0,[]],\"toggleDeleteTagModal\"],null]]]],false],[0,\"\\n\\n\"],[4,\"if\",[[22,[\"showDeleteTagModal\"]]],null,{\"statements\":[[0,\"    \"],[1,[26,\"gh-fullscreen-modal\",[\"delete-tag\"],[[\"model\",\"confirm\",\"close\",\"modifier\"],[[22,[\"tag\"]],[26,\"action\",[[21,0,[]],\"deleteTag\"],null],[26,\"action\",[[21,0,[]],\"toggleDeleteTagModal\"],null],\"action wide\"]]],false],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/settings/tags/tag.hbs" } });
});