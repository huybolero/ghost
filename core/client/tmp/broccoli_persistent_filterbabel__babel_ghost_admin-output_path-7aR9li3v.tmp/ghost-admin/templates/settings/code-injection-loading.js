define("ghost-admin/templates/settings/code-injection-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "9Bigzs3X", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[10,\"class\",\"gh-canvas\"],[8],[0,\"\\n    \"],[6,\"header\"],[10,\"class\",\"gh-canvas-header\"],[8],[0,\"\\n        \"],[6,\"h2\"],[10,\"class\",\"gh-canvas-title\"],[10,\"data-test-screen-title\",\"\"],[8],[0,\"Code injection\"],[9],[0,\"\\n        \"],[6,\"section\"],[10,\"class\",\"view-actions\"],[8],[0,\"\\n            \"],[1,[26,\"gh-task-button\",null,[[\"task\",\"class\",\"disabled\",\"data-test-save-button\"],[[22,[\"save\"]],\"gh-btn gh-btn-blue gh-btn-icon\",true,true]]],false],[0,\"\\n        \"],[9],[0,\"\\n    \"],[9],[0,\"\\n\\n    \"],[6,\"section\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/settings/code-injection-loading.hbs" } });
});