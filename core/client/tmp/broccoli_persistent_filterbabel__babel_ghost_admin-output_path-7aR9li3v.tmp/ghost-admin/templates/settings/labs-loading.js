define("ghost-admin/templates/settings/labs-loading", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "x/t0MZAP", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[10,\"class\",\"gh-canvas\"],[8],[0,\"\\n    \"],[6,\"header\"],[10,\"class\",\"gh-canvas-header\"],[8],[0,\"\\n        \"],[6,\"h2\"],[10,\"class\",\"gh-canvas-title\"],[10,\"data-test-screen-title\",\"\"],[8],[0,\"Labs\"],[9],[0,\"\\n    \"],[9],[0,\"\\n\\n    \"],[6,\"div\"],[10,\"class\",\"gh-content\"],[8],[0,\"\\n        \"],[1,[20,\"gh-loading-spinner\"],false],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/settings/labs-loading.hbs" } });
});