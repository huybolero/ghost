define("ghost-admin/templates/settings/design/uploadtheme", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "vijhZugG", "block": "{\"symbols\":[],\"statements\":[[1,[26,\"gh-fullscreen-modal\",[\"upload-theme\"],[[\"model\",\"close\",\"modifier\"],[[26,\"hash\",null,[[\"themes\",\"activate\"],[[22,[\"themes\"]],[26,\"route-action\",[\"activateTheme\"],null]]]],[26,\"route-action\",[\"cancel\"],null],\"action wide\"]]],false],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/settings/design/uploadtheme.hbs" } });
});