define("ghost-admin/templates/components/gh-alert", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "EUIQV3uL", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[10,\"class\",\"gh-alert-content\"],[8],[0,\"\\n    \"],[1,[22,[\"message\",\"message\"]],false],[0,\"\\n\"],[9],[0,\"\\n\"],[6,\"button\"],[10,\"class\",\"gh-alert-close\"],[3,\"action\",[[21,0,[]],\"closeNotification\"]],[8],[1,[26,\"svg-jar\",[\"close\"],null],false],[6,\"span\"],[10,\"class\",\"hidden\"],[8],[0,\"Close\"],[9],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-alert.hbs" } });
});