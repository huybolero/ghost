define("ghost-admin/templates/components/gh-progress-bar", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "BNTAx0Xp", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[10,\"class\",\"gh-progress-container\"],[8],[0,\"\\n    \"],[6,\"div\"],[10,\"class\",\"gh-progress-container-progress\"],[8],[0,\"\\n        \"],[6,\"div\"],[11,\"class\",[27,[\"gh-progress-bar \",[26,\"if\",[[22,[\"isError\"]],\"-error\"],null]]]],[11,\"style\",[20,\"progressStyle\"],null],[10,\"data-test-progress-bar\",\"\"],[8],[9],[0,\"\\n    \"],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-progress-bar.hbs" } });
});