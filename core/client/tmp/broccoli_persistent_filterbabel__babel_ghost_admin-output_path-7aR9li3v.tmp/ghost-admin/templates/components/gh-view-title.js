define("ghost-admin/templates/components/gh-view-title", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "VRGlJ7O2", "block": "{\"symbols\":[\"&default\"],\"statements\":[[6,\"button\"],[10,\"class\",\"gh-mobilemenu-button\"],[10,\"role\",\"presentation\"],[3,\"action\",[[21,0,[]],\"openMobileMenu\"],[[\"target\"],[[22,[\"ui\"]]]]],[8],[1,[26,\"svg-jar\",[\"icon\"],[[\"class\"],[\"icon-gh\"]]],false],[6,\"span\"],[10,\"class\",\"sr-only\"],[8],[0,\"Menu\"],[9],[9],[0,\"\\n\"],[13,1],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-view-title.hbs" } });
});