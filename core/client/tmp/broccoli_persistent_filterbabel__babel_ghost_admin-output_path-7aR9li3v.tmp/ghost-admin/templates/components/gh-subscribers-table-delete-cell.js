define("ghost-admin/templates/components/gh-subscribers-table-delete-cell", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "0jh/asNj", "block": "{\"symbols\":[],\"statements\":[[6,\"button\"],[10,\"class\",\"gh-btn gh-btn-link gh-btn-sm\"],[3,\"action\",[[21,0,[]],[22,[\"tableActions\",\"delete\"]],[22,[\"row\",\"content\"]]]],[8],[6,\"span\"],[8],[1,[26,\"svg-jar\",[\"trash\"],null],false],[9],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-subscribers-table-delete-cell.hbs" } });
});