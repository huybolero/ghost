define("ghost-admin/templates/components/gh-file-upload", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "HNw3YfEG", "block": "{\"symbols\":[],\"statements\":[[6,\"input\"],[10,\"class\",\"gh-input gh-input-file q\"],[10,\"data-url\",\"upload\"],[10,\"name\",\"importfile\"],[11,\"accept\",[27,[[20,\"acceptEncoding\"]]]],[10,\"type\",\"file\"],[8],[9],[0,\"\\n\"],[6,\"button\"],[10,\"id\",\"startupload\"],[10,\"class\",\"gh-btn gh-btn-hover-blue\"],[11,\"disabled\",[20,\"uploadButtonDisabled\"],null],[10,\"type\",\"submit\"],[3,\"action\",[[21,0,[]],\"upload\"]],[8],[0,\"\\n    \"],[6,\"span\"],[8],[1,[20,\"uploadButtonText\"],false],[9],[0,\"\\n\"],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-file-upload.hbs" } });
});