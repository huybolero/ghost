define("ghost-admin/templates/components/gh-timezone-select", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "2NoxS2Du", "block": "{\"symbols\":[],\"statements\":[[6,\"span\"],[10,\"class\",\"gh-select\"],[11,\"data-select-text\",[27,[[22,[\"selectedTimezone\",\"label\"]]]]],[10,\"tabindex\",\"0\"],[8],[0,\"\\n    \"],[1,[26,\"one-way-select\",null,[[\"id\",\"name\",\"options\",\"optionValuePath\",\"optionLabelPath\",\"value\",\"update\"],[\"activeTimezone\",\"general[activeTimezone]\",[22,[\"selectableTimezones\"]],\"name\",\"label\",[22,[\"selectedTimezone\"]],[26,\"action\",[[21,0,[]],\"setTimezone\"],null]]]],false],[0,\"\\n    \"],[1,[26,\"svg-jar\",[\"arrow-down-small\"],null],false],[0,\"\\n\"],[9],[0,\"\\n\"],[4,\"if\",[[22,[\"hasTimezoneOverride\"]]],null,{\"statements\":[[0,\"    \"],[6,\"p\"],[8],[0,\"Your timezone has been automatically set to \"],[1,[20,\"activeTimezone\"],false],[0,\".\"],[9],[0,\"\\n\"]],\"parameters\":[]},null],[6,\"p\"],[8],[0,\"The local time here is currently \"],[1,[20,\"localTime\"],false],[9],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-timezone-select.hbs" } });
});