define("ghost-admin/templates/components/gh-cm-editor", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "4omwU6i9", "block": "{\"symbols\":[],\"statements\":[[4,\"if\",[[22,[\"isInitializingCodemirror\"]]],null,{\"statements\":[[0,\"    \"],[1,[26,\"gh-textarea\",null,[[\"class\",\"value\",\"input\"],[\"gh-cm-editor-textarea\",[26,\"readonly\",[[22,[\"_value\"]]],null],[26,\"action\",[[21,0,[]],\"updateFromTextarea\"],[[\"value\"],[\"target.value\"]]]]]],false],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-cm-editor.hbs" } });
});