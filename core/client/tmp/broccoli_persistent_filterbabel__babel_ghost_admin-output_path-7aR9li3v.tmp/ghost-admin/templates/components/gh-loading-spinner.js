define("ghost-admin/templates/components/gh-loading-spinner", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "7R8iPQPn", "block": "{\"symbols\":[],\"statements\":[[4,\"if\",[[22,[\"showSpinner\"]]],null,{\"statements\":[[0,\"    \"],[6,\"div\"],[10,\"class\",\"gh-loading-content\"],[8],[0,\"\\n        \"],[6,\"div\"],[10,\"class\",\"gh-loading-spinner\"],[8],[9],[0,\"\\n    \"],[9],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-loading-spinner.hbs" } });
});