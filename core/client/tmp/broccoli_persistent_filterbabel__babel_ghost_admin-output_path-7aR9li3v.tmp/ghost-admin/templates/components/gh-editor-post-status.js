define("ghost-admin/templates/components/gh-editor-post-status", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "DwWs/fhk", "block": "{\"symbols\":[],\"statements\":[[4,\"if\",[[22,[\"_isSaving\"]]],null,{\"statements\":[[0,\"    Saving...\\n\"]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[22,[\"isPublished\"]]],null,{\"statements\":[[0,\"    Published\\n\"]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[22,[\"isScheduled\"]]],null,{\"statements\":[[0,\"    Scheduled\\n\"]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[22,[\"isNew\"]]],null,{\"statements\":[[0,\"    New\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"    Draft\\n\"]],\"parameters\":[]}]],\"parameters\":[]}]],\"parameters\":[]}]],\"parameters\":[]}]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-editor-post-status.hbs" } });
});