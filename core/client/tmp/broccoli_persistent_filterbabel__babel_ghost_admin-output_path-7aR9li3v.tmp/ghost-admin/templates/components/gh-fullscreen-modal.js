define("ghost-admin/templates/components/gh-fullscreen-modal", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "5QdCx+fe", "block": "{\"symbols\":[\"&default\"],\"statements\":[[4,\"liquid-wormhole\",null,[[\"class\"],[\"fullscreen-modal-container\"]],{\"statements\":[[0,\"    \"],[6,\"div\"],[10,\"class\",\"fullscreen-modal-background\"],[3,\"action\",[[21,0,[]],\"clickOverlay\"]],[8],[9],[0,\"\\n    \"],[6,\"div\"],[11,\"class\",[20,\"modalClasses\"],null],[8],[0,\"\\n\"],[4,\"if\",[[23,1]],null,{\"statements\":[[0,\"          \"],[13,1],[0,\"\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"          \"],[1,[26,\"component\",[[22,[\"modalPath\"]]],[[\"model\",\"confirm\",\"closeModal\"],[[22,[\"model\"]],[26,\"action\",[[21,0,[]],\"confirm\"],null],[26,\"action\",[[21,0,[]],\"close\"],null]]]],false],[0,\"\\n\"]],\"parameters\":[]}],[0,\"    \"],[9],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/components/gh-fullscreen-modal.hbs" } });
});