define("ghost-admin/templates/setup/one", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "GfVFksY6", "block": "{\"symbols\":[],\"statements\":[[6,\"header\"],[8],[0,\"\\n    \"],[6,\"h1\"],[8],[0,\"Welcome to \"],[6,\"strong\"],[8],[0,\"Ghost\"],[9],[0,\"!\"],[9],[0,\"\\n    \"],[6,\"p\"],[8],[0,\"All over the world, people have started \"],[6,\"em\"],[8],[1,[20,\"gh-download-count\"],false],[9],[0,\" incredible blogs with Ghost. Today, we’re starting yours.\"],[9],[0,\"\\n\"],[9],[0,\"\\n\\n\"],[6,\"figure\"],[10,\"class\",\"gh-flow-screenshot\"],[8],[0,\"\\n    \"],[6,\"img\"],[10,\"src\",\"assets/img/install-welcome.png\"],[10,\"alt\",\"Ghost screenshot\"],[8],[9],[0,\"\\n\"],[9],[0,\"\\n\\n\"],[4,\"link-to\",[\"setup.two\"],[[\"classNames\"],[\"gh-btn gh-btn-green gh-btn-lg gh-btn-icon gh-btn-icon-right\"]],{\"statements\":[[0,\"    \"],[6,\"span\"],[8],[0,\"Create your account \"],[1,[26,\"svg-jar\",[\"arrow-right-small\"],null],false],[9],[0,\"\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "ghost-admin/templates/setup/one.hbs" } });
});