define('ghost-admin/koenig-editor/tests/addon.lint-test', [], function () {
  'use strict';

  describe('ESLint | addon', function () {

    it('addon/components/koenig-card-code.js', function () {
      // test passed
    });

    it('addon/components/koenig-card-hr.js', function () {
      // test passed
    });

    it('addon/components/koenig-card-html.js', function () {
      // test passed
    });

    it('addon/components/koenig-card-image.js', function () {
      // test passed
    });

    it('addon/components/koenig-card-markdown.js', function () {
      // test passed
    });

    it('addon/components/koenig-card.js', function () {
      // test passed
    });

    it('addon/components/koenig-editor.js', function () {
      // test passed
    });

    it('addon/components/koenig-link-input.js', function () {
      // test passed
    });

    it('addon/components/koenig-link-toolbar.js', function () {
      // test passed
    });

    it('addon/components/koenig-plus-menu.js', function () {
      // test passed
    });

    it('addon/components/koenig-slash-menu.js', function () {
      // test passed
    });

    it('addon/components/koenig-toolbar.js', function () {
      // test passed
    });

    it('addon/options/atoms.js', function () {
      // test passed
    });

    it('addon/options/cards.js', function () {
      // test passed
    });

    it('addon/options/key-commands.js', function () {
      // test passed
    });

    it('addon/options/parser-plugins.js', function () {
      // test passed
    });

    it('addon/options/text-expansions.js', function () {
      // test passed
    });

    it('addon/utils/create-component-card.js', function () {
      // test passed
    });

    it('addon/utils/markup-utils.js', function () {
      // test passed
    });
  });
});