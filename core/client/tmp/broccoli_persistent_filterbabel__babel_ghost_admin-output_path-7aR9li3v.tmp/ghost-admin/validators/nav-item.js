define('ghost-admin/validators/nav-item', ['exports', 'ghost-admin/validators/base', 'npm:validator'], function (exports, _base, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _base.default.create({
        properties: ['label', 'url'],

        label(model) {
            let label = model.get('label');
            let hasValidated = model.get('hasValidated');

            if (Ember.isBlank(label)) {
                model.get('errors').add('label', 'You must specify a label');
                this.invalidate();
            }

            hasValidated.addObject('label');
        },

        url(model) {
            let url = model.get('url');
            let hasValidated = model.get('hasValidated');
            /* eslint-disable camelcase */
            let validatorOptions = { require_protocol: true };
            /* eslint-enable camelcase */
            let urlRegex = new RegExp(/^(\/|#|[a-zA-Z0-9-]+:)/);

            if (Ember.isBlank(url)) {
                model.get('errors').add('url', 'You must specify a URL or relative path');
                this.invalidate();
            } else if (url.match(/\s/) || !_npmValidator.default.isURL(url, validatorOptions) && !url.match(urlRegex)) {
                model.get('errors').add('url', 'You must specify a valid URL or relative path');
                this.invalidate();
            }

            hasValidated.addObject('url');
        }
    });
});