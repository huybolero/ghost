define('ghost-admin/validators/invite-user', ['exports', 'ghost-admin/validators/base', 'npm:validator'], function (exports, _base, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _base.default.create({
        properties: ['email'],

        email(model) {
            let email = model.get('email');

            if (Ember.isBlank(email)) {
                model.get('errors').add('email', 'Please enter an email.');
                this.invalidate();
            } else if (!_npmValidator.default.isEmail(email)) {
                model.get('errors').add('email', 'Invalid Email.');
                this.invalidate();
            }
        }
    });
});