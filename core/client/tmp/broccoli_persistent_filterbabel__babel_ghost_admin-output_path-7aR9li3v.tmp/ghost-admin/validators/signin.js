define('ghost-admin/validators/signin', ['exports', 'ghost-admin/validators/base', 'npm:validator'], function (exports, _base, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _base.default.create({
        properties: ['identification', 'signin', 'forgotPassword'],
        invalidMessage: 'Email address is not valid',

        identification(model) {
            let id = model.get('identification');

            if (!Ember.isBlank(id) && !_npmValidator.default.isEmail(id)) {
                model.get('errors').add('identification', this.get('invalidMessage'));
                this.invalidate();
            }
        },

        signin(model) {
            let id = model.get('identification');
            let password = model.get('password');

            model.get('errors').clear();

            if (Ember.isBlank(id)) {
                model.get('errors').add('identification', 'Please enter an email');
                this.invalidate();
            }

            if (!Ember.isBlank(id) && !_npmValidator.default.isEmail(id)) {
                model.get('errors').add('identification', this.get('invalidMessage'));
                this.invalidate();
            }

            if (Ember.isBlank(password)) {
                model.get('errors').add('password', 'Please enter a password');
                this.invalidate();
            }
        },

        forgotPassword(model) {
            let id = model.get('identification');

            model.get('errors').clear();

            if (Ember.isBlank(id) || !_npmValidator.default.isEmail(id)) {
                model.get('errors').add('identification', this.get('invalidMessage'));
                this.invalidate();
            }
        }
    });
});