define('ghost-admin/validators/subscriber', ['exports', 'ghost-admin/validators/base', 'npm:validator'], function (exports, _base, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _base.default.create({
        properties: ['email'],

        email(model) {
            let email = model.get('email');

            if (Ember.isBlank(email)) {
                model.get('errors').add('email', 'Please enter an email.');
                model.get('hasValidated').pushObject('email');
                this.invalidate();
            } else if (!_npmValidator.default.isEmail(email)) {
                model.get('errors').add('email', 'Invalid email.');
                model.get('hasValidated').pushObject('email');
                this.invalidate();
            } else if (!_npmValidator.default.isLength(email, 0, 191)) {
                model.get('errors').add('email', 'Email is too long');
                model.get('hasValidated').pushObject('email');
                this.invalidate();
            }
        }
    });
});