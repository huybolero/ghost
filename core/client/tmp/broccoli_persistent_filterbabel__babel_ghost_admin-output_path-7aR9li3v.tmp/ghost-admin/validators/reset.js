define('ghost-admin/validators/reset', ['exports', 'ghost-admin/validators/password', 'npm:validator'], function (exports, _password, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _password.default.create({
        properties: ['newPassword'],

        newPassword(model) {
            let p1 = model.get('newPassword');
            let p2 = model.get('ne2Password');

            if (Ember.isBlank(p1)) {
                model.get('errors').add('newPassword', 'Please enter a password.');
                this.invalidate();
            } else if (!_npmValidator.default.equals(p1, p2 || '')) {
                model.get('errors').add('ne2Password', 'The two new passwords don\'t match.');
                this.invalidate();
            }

            this.passwordValidation(model, p1, 'newPassword');
        }
    });
});