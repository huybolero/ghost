define('ghost-admin/validators/setup', ['exports', 'ghost-admin/validators/new-user', 'npm:validator'], function (exports, _newUser, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _newUser.default.create({
        properties: ['name', 'email', 'password', 'blogTitle'],

        blogTitle(model) {
            let blogTitle = model.get('blogTitle');

            if (!_npmValidator.default.isLength(blogTitle || '', 1)) {
                model.get('errors').add('blogTitle', 'Please enter a blog title.');
                this.invalidate();
            }

            if (!_npmValidator.default.isLength(blogTitle || '', 0, 150)) {
                model.get('errors').add('blogTitle', 'Title is too long');
                this.invalidate();
            }
        }
    });
});