define('ghost-admin/validators/slack-integration', ['exports', 'ghost-admin/validators/base', 'npm:validator'], function (exports, _base, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _base.default.create({
        properties: ['url'],

        url(model) {
            let url = model.get('url');
            let hasValidated = model.get('hasValidated');

            // eslint-disable-next-line camelcase
            if (!Ember.isBlank(url) && !_npmValidator.default.isURL(url, { require_protocol: true })) {
                model.get('errors').add('url', 'The URL must be in a format like https://hooks.slack.com/services/<your personal key>');

                this.invalidate();
            }

            hasValidated.addObject('url');
        }
    });
});