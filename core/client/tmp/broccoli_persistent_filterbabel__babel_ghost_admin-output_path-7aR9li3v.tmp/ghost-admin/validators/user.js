define('ghost-admin/validators/user', ['exports', 'ghost-admin/validators/password', 'npm:validator'], function (exports, _password, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _password.default.create({
        properties: ['name', 'bio', 'email', 'location', 'website', 'roles'],

        isActive(model) {
            return model.get('status') === 'active';
        },

        name(model) {
            let name = model.get('name');

            if (this.isActive(model)) {
                if (Ember.isBlank(name)) {
                    model.get('errors').add('name', 'Please enter a name.');
                    this.invalidate();
                } else if (!_npmValidator.default.isLength(name, 0, 191)) {
                    model.get('errors').add('name', 'Name is too long');
                    this.invalidate();
                }
            }
        },

        bio(model) {
            let bio = model.get('bio');

            if (this.isActive(model)) {
                if (!_npmValidator.default.isLength(bio || '', 0, 200)) {
                    model.get('errors').add('bio', 'Bio is too long');
                    this.invalidate();
                }
            }
        },

        email(model) {
            let email = model.get('email');

            if (!_npmValidator.default.isEmail(email || '')) {
                model.get('errors').add('email', 'Please supply a valid email address');
                this.invalidate();
            }

            if (!_npmValidator.default.isLength(email || '', 0, 191)) {
                model.get('errors').add('email', 'Email is too long');
                this.invalidate();
            }
        },

        location(model) {
            let location = model.get('location');

            if (this.isActive(model)) {
                if (!_npmValidator.default.isLength(location || '', 0, 150)) {
                    model.get('errors').add('location', 'Location is too long');
                    this.invalidate();
                }
            }
        },

        website(model) {
            let website = model.get('website');
            // eslint-disable-next-line camelcase
            let isInvalidWebsite = !_npmValidator.default.isURL(website || '', { require_protocol: false }) || !_npmValidator.default.isLength(website || '', 0, 2000);

            if (this.isActive(model)) {
                if (!Ember.isBlank(website) && isInvalidWebsite) {
                    model.get('errors').add('website', 'Website is not a valid url');
                    this.invalidate();
                }
            }
        },

        roles(model) {
            if (!this.isActive(model)) {
                let roles = model.get('roles');

                if (roles.length < 1) {
                    model.get('errors').add('role', 'Please select a role');
                    this.invalidate();
                }
            }
        },

        passwordChange(model) {
            let newPassword = model.get('newPassword');
            let ne2Password = model.get('ne2Password');

            // validation only marks the requested property as validated so we
            // have to add properties manually
            model.get('hasValidated').addObject('newPassword');
            model.get('hasValidated').addObject('ne2Password');

            if (Ember.isBlank(newPassword) && Ember.isBlank(ne2Password)) {
                model.get('errors').add('newPassword', 'Sorry, passwords can\'t be blank');
                this.invalidate();
            } else {
                if (!_npmValidator.default.equals(newPassword, ne2Password || '')) {
                    model.get('errors').add('ne2Password', 'Your new passwords do not match');
                    this.invalidate();
                }

                this.passwordValidation(model, newPassword, 'newPassword');
            }
        },

        ownPasswordChange(model) {
            let oldPassword = model.get('password');

            this.passwordChange(model);

            // validation only marks the requested property as validated so we
            // have to add properties manually
            model.get('hasValidated').addObject('password');

            if (Ember.isBlank(oldPassword)) {
                model.get('errors').add('password', 'Your current password is required to set a new one');
                this.invalidate();
            }
        }
    });
});