define('ghost-admin/validators/new-user', ['exports', 'ghost-admin/validators/password', 'npm:validator'], function (exports, _password, _npmValidator) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _password.default.extend({
        init() {
            this._super(...arguments);
            this.properties = this.properties || ['name', 'email', 'password'];
        },

        name(model) {
            let name = model.get('name');

            if (!_npmValidator.default.isLength(name || '', 1)) {
                model.get('errors').add('name', 'Please enter a name.');
                this.invalidate();
            }
        },

        email(model) {
            let email = model.get('email');

            if (Ember.isBlank(email)) {
                model.get('errors').add('email', 'Please enter an email.');
                this.invalidate();
            } else if (!_npmValidator.default.isEmail(email)) {
                model.get('errors').add('email', 'Invalid Email.');
                this.invalidate();
            }
        },

        password(model) {
            this.passwordValidation(model);
        }
    });
});