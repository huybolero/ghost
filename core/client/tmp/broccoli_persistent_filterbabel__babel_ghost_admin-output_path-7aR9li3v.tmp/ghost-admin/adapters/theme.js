define('ghost-admin/adapters/theme', ['exports', 'ghost-admin/adapters/application'], function (exports, _application) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend({

        activate(model) {
            let url = `${this.buildURL('theme', model.get('id'))}activate/`;

            return this.ajax(url, 'PUT', { data: {} }).then(data => {
                this.store.pushPayload(data);
                return model;
            });
        }

    });
});