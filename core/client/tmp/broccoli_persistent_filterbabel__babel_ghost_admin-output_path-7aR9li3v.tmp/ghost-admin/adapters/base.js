define('ghost-admin/adapters/base', ['exports', 'ember-ajax/mixins/ajax-support', 'ember-simple-auth/mixins/data-adapter-mixin', 'ember-data/adapters/rest', 'ghost-admin/utils/ghost-paths'], function (exports, _ajaxSupport, _dataAdapterMixin, _rest, _ghostPaths) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _rest.default.extend(_dataAdapterMixin.default, _ajaxSupport.default, {
        host: window.location.origin,
        namespace: (0, _ghostPaths.default)().apiRoot.slice(1),

        session: Ember.inject.service(),

        shouldBackgroundReloadRecord() {
            return false;
        },

        /* eslint-disable camelcase */
        authorize(xhr) {
            if (this.get('session.isAuthenticated')) {
                var _get = this.get('session.data.authenticated');

                let access_token = _get.access_token;

                xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
            }
        },
        /* eslint-enable camelcase */

        query(store, type, query) {
            let id;

            if (query.id) {
                id = query.id;
                delete query.id;
            }

            return this.ajax(this.buildURL(type.modelName, id), 'GET', { data: query });
        },

        buildURL() {
            // Ensure trailing slashes
            let url = this._super(...arguments);

            if (url.slice(-1) !== '/') {
                url += '/';
            }

            return url;
        }
    });
});