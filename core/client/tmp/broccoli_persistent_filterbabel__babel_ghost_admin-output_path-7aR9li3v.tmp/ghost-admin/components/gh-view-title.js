define('ghost-admin/components/gh-view-title', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        ui: Ember.inject.service(),

        tagName: 'h2',
        classNames: ['view-title']
    });
});