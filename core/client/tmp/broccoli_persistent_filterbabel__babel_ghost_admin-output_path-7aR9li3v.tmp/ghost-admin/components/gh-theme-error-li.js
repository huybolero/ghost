define('ghost-admin/components/gh-theme-error-li', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: '',
        error: null,
        showDetails: false,

        actions: {
            toggleDetails() {
                this.toggleProperty('showDetails');
            }
        }
    });
});