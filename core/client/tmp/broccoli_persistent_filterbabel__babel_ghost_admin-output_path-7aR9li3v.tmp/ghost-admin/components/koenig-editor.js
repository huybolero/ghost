define('ghost-admin/components/koenig-editor', ['exports', 'koenig-editor/components/koenig-editor'], function (exports, _koenigEditor) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigEditor.default;
    }
  });
});