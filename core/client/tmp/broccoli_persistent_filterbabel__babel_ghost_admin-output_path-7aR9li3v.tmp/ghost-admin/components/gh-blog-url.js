define('ghost-admin/components/gh-blog-url', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        config: Ember.inject.service(),

        tagName: ''
    });
});