define('ghost-admin/components/koenig-toolbar', ['exports', 'koenig-editor/components/koenig-toolbar'], function (exports, _koenigToolbar) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigToolbar.default;
    }
  });
});