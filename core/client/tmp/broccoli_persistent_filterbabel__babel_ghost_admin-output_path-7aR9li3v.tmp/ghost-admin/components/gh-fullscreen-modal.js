define('ghost-admin/components/gh-fullscreen-modal', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    const FullScreenModalComponent = Ember.Component.extend({
        dropdown: Ember.inject.service(),

        model: null,
        modifier: null,

        modalPath: Ember.computed('modal', function () {
            return `modal-${this.get('modal') || 'unknown'}`;
        }),

        modalClasses: Ember.computed('modifiers', function () {
            let modalClass = 'fullscreen-modal';
            let modifiers = (this.get('modifier') || '').split(' ');
            let modalClasses = Ember.A([modalClass]);

            modifiers.forEach(modifier => {
                if (!Ember.isBlank(modifier)) {
                    let className = `${modalClass}-${modifier}`;
                    modalClasses.push(className);
                }
            });

            return modalClasses.join(' ');
        }),

        didInsertElement() {
            Ember.run.schedule('afterRender', this, function () {
                this.get('dropdown').closeDropdowns();
            });
        },

        actions: {
            close() {
                return this.close();
            },

            confirm() {
                return this.confirm();
            },

            clickOverlay() {
                this.send('close');
            }
        },

        // Allowed actions
        close: () => Ember.RSVP.resolve(),
        confirm: () => Ember.RSVP.resolve()
    });

    FullScreenModalComponent.reopenClass({
        positionalParams: ['modal']
    });

    exports.default = FullScreenModalComponent;
});