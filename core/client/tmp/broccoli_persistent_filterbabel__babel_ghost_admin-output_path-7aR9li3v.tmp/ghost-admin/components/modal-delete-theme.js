define('ghost-admin/components/modal-delete-theme', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        // Allowed actions
        confirm: () => {},

        theme: Ember.computed.alias('model.theme'),
        download: Ember.computed.alias('model.download'),

        actions: {
            confirm() {
                this.get('deleteTheme').perform();
            }
        },

        deleteTheme: (0, _emberConcurrency.task)(function* () {
            try {
                yield this.confirm();
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});