define('ghost-admin/components/modal-delete-post', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        notifications: Ember.inject.service(),

        post: Ember.computed.alias('model.post'),
        onSuccess: Ember.computed.alias('model.onSuccess'),

        actions: {
            confirm() {
                this.get('deletePost').perform();
            }
        },

        _deletePost() {
            let post = this.get('post');

            // definitely want to clear the data store and post of any unsaved,
            // client-generated tags
            post.updateTags();

            return post.destroyRecord();
        },

        _success() {
            // clear any previous error messages
            this.get('notifications').closeAlerts('post.delete');

            // trigger the success action
            if (this.get('onSuccess')) {
                this.get('onSuccess')();
            }
        },

        _failure(error) {
            this.get('notifications').showAPIError(error, { key: 'post.delete.failed' });
        },

        deletePost: (0, _emberConcurrency.task)(function* () {
            try {
                yield this._deletePost();
                this._success();
            } catch (e) {
                this._failure(e);
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});