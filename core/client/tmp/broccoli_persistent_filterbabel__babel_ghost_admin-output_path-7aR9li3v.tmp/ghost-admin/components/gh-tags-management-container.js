define('ghost-admin/components/gh-tags-management-container', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        mediaQueries: Ember.inject.service(),

        classNames: ['view-container'],
        classNameBindings: ['isMobile'],

        tags: null,
        selectedTag: null,

        isMobile: Ember.computed.reads('mediaQueries.maxWidth600'),
        isEmpty: Ember.computed.equal('tags.length', 0),

        displaySettingsPane: Ember.computed('isEmpty', 'selectedTag', 'isMobile', function () {
            let isEmpty = this.get('isEmpty');
            let selectedTag = this.get('selectedTag');
            let isMobile = this.get('isMobile');

            // always display settings pane for blank-slate on mobile
            if (isMobile && isEmpty) {
                return true;
            }

            // display list if no tag is selected on mobile
            if (isMobile && Ember.isBlank(selectedTag)) {
                return false;
            }

            // default to displaying settings pane
            return true;
        }),

        init() {
            this._super(...arguments);
            this.get('mediaQueries').on('change', this, this._fireMobileChangeActions);
        },

        willDestroyElement() {
            this._super(...arguments);
            this.get('mediaQueries').off('change', this, this._fireMobileChangeActions);
        },

        _fireMobileChangeActions(key, value) {
            if (key === 'maxWidth600') {
                let leftMobileAction = this.get('leftMobile');

                this.set('isMobile', value);

                if (!value && leftMobileAction) {
                    leftMobileAction();
                }
            }
        }
    });
});