define('ghost-admin/components/gh-psm-template-select', ['exports', 'ember-concurrency'], function (exports, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    exports.default = Ember.Component.extend({

        store: Ember.inject.service(),

        // public attributes
        tagName: '',
        post: null,

        // internal properties
        activeTheme: null,

        // closure actions
        onTemplateSelect() {},

        // computed properties
        customTemplates: Ember.computed('activeTheme.customTemplates.[]', function () {
            let templates = this.get('activeTheme.customTemplates') || [];
            let defaultTemplate = {
                filename: '',
                name: 'Default'
            };

            return Ember.isEmpty(templates) ? templates : [defaultTemplate, ...templates.sortBy('name')];
        }),

        matchedSlugTemplate: Ember.computed('post.{page,slug}', 'activeTheme.slugTemplates.[]', function () {
            let slug = this.get('post.slug');
            let type = this.get('post.page') ? 'page' : 'post';

            var _get$filter = this.get('activeTheme.slugTemplates').filter(function (template) {
                return template.for.includes(type) && template.slug === slug;
            }),
                _get$filter2 = _slicedToArray(_get$filter, 1);

            let matchedTemplate = _get$filter2[0];


            return matchedTemplate;
        }),

        selectedTemplate: Ember.computed('post.customTemplate', 'customTemplates.[]', function () {
            let templates = this.get('customTemplates');
            let filename = this.get('post.customTemplate');

            return templates.findBy('filename', filename);
        }),

        // hooks
        didInsertElement() {
            this._super(...arguments);
            this.get('loadActiveTheme').perform();
        },

        actions: {
            selectTemplate(template) {
                this.onTemplateSelect(template.filename);
            }
        },

        // tasks
        loadActiveTheme: (0, _emberConcurrency.task)(function* () {
            let store = this.get('store');
            let themes = yield store.peekAll('theme');

            if (Ember.isEmpty(themes)) {
                themes = yield store.findAll('theme');
            }

            let activeTheme = themes.filterBy('active', true).get('firstObject');

            this.set('activeTheme', activeTheme);
        })
    });
});