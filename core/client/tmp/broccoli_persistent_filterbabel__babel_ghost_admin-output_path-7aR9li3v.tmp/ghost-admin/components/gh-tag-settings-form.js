define('ghost-admin/components/gh-tag-settings-form', ['exports', 'ghost-admin/utils/bound-one-way'], function (exports, _boundOneWay) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    const Handlebars = Ember.Handlebars;
    exports.default = Ember.Component.extend({
        feature: Ember.inject.service(),
        config: Ember.inject.service(),
        mediaQueries: Ember.inject.service(),

        tag: null,

        isViewingSubview: false,

        // Allowed actions
        setProperty: () => {},
        showDeleteTagModal: () => {},

        scratchName: (0, _boundOneWay.default)('tag.name'),
        scratchSlug: (0, _boundOneWay.default)('tag.slug'),
        scratchDescription: (0, _boundOneWay.default)('tag.description'),
        scratchMetaTitle: (0, _boundOneWay.default)('tag.metaTitle'),
        scratchMetaDescription: (0, _boundOneWay.default)('tag.metaDescription'),

        isMobile: Ember.computed.reads('mediaQueries.maxWidth600'),

        title: Ember.computed('tag.isNew', function () {
            if (this.get('tag.isNew')) {
                return 'New Tag';
            } else {
                return 'Tag Settings';
            }
        }),

        seoTitle: Ember.computed('scratchName', 'scratchMetaTitle', function () {
            let metaTitle = this.get('scratchMetaTitle') || '';

            metaTitle = metaTitle.length > 0 ? metaTitle : this.get('scratchName');

            if (metaTitle && metaTitle.length > 70) {
                metaTitle = metaTitle.substring(0, 70).trim();
                metaTitle = Handlebars.Utils.escapeExpression(metaTitle);
                metaTitle = Ember.String.htmlSafe(`${metaTitle}&hellip;`);
            }

            return metaTitle;
        }),

        seoURL: Ember.computed('scratchSlug', function () {
            let blogUrl = this.get('config.blogUrl');
            let seoSlug = this.get('scratchSlug') || '';

            let seoURL = `${blogUrl}/tag/${seoSlug}`;

            // only append a slash to the URL if the slug exists
            if (seoSlug) {
                seoURL += '/';
            }

            if (seoURL.length > 70) {
                seoURL = seoURL.substring(0, 70).trim();
                seoURL = Handlebars.Utils.escapeExpression(seoURL);
                seoURL = Ember.String.htmlSafe(`${seoURL}&hellip;`);
            }

            return seoURL;
        }),

        seoDescription: Ember.computed('scratchDescription', 'scratchMetaDescription', function () {
            let metaDescription = this.get('scratchMetaDescription') || '';

            metaDescription = metaDescription.length > 0 ? metaDescription : this.get('scratchDescription');

            if (metaDescription && metaDescription.length > 156) {
                metaDescription = metaDescription.substring(0, 156).trim();
                metaDescription = Handlebars.Utils.escapeExpression(metaDescription);
                metaDescription = Ember.String.htmlSafe(`${metaDescription}&hellip;`);
            }

            return metaDescription;
        }),

        didReceiveAttrs() {
            this._super(...arguments);

            let oldTagId = this._oldTagId;
            let newTagId = this.get('tag.id');

            if (newTagId !== oldTagId) {
                this.reset();
            }

            this._oldTagId = newTagId;
        },

        actions: {
            setProperty(property, value) {
                this.setProperty(property, value);
            },

            setCoverImage(image) {
                this.setProperty('featureImage', image);
            },

            clearCoverImage() {
                this.setProperty('featureImage', '');
            },

            openMeta() {
                this.set('isViewingSubview', true);
            },

            closeMeta() {
                this.set('isViewingSubview', false);
            },

            deleteTag() {
                this.showDeleteTagModal();
            }
        },

        reset() {
            this.set('isViewingSubview', false);
            if (this.$()) {
                this.$('.settings-menu-pane').scrollTop(0);
            }
        },

        focusIn() {
            key.setScope('tag-settings-form');
        },

        focusOut() {
            key.setScope('default');
        }

    });
});