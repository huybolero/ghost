define('ghost-admin/components/gh-notification', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        notifications: Ember.inject.service(),

        tagName: 'article',
        classNames: ['gh-notification', 'gh-notification-passive'],
        classNameBindings: ['typeClass'],

        message: null,

        typeClass: Ember.computed('message.type', function () {
            let type = this.get('message.type');
            let classes = '';
            let typeMapping;

            typeMapping = {
                success: 'green',
                error: 'red',
                warn: 'yellow'
            };

            if (typeMapping[type] !== undefined) {
                classes += `gh-notification-${typeMapping[type]}`;
            }

            return classes;
        }),

        didInsertElement() {
            this._super(...arguments);

            this.$().on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', event => {
                if (event.originalEvent.animationName === 'fade-out') {
                    this.get('notifications').closeNotification(this.get('message'));
                }
            });
        },

        willDestroyElement() {
            this._super(...arguments);
            this.$().off('animationend webkitAnimationEnd oanimationend MSAnimationEnd');
        },

        actions: {
            closeNotification() {
                this.get('notifications').closeNotification(this.get('message'));
            }
        }
    });
});