define('ghost-admin/components/gh-validation-status-container', ['exports', 'ghost-admin/mixins/validation-state'], function (exports, _validationState) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend(_validationState.default, {
        classNameBindings: ['errorClass'],

        errorClass: Ember.computed('property', 'hasError', 'hasValidated.[]', function () {
            let hasValidated = this.get('hasValidated');
            let property = this.get('property');

            if (hasValidated && hasValidated.includes(property)) {
                return this.get('hasError') ? 'error' : 'success';
            } else {
                return '';
            }
        })
    });
});