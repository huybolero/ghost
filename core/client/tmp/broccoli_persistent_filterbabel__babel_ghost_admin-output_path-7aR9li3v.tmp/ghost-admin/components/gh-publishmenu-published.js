define('ghost-admin/components/gh-publishmenu-published', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({

        'data-test-publishmenu-published': true,

        didInsertElement() {
            this.get('setSaveType')('publish');
        }
    });
});