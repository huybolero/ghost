define('ghost-admin/components/gh-text-input', ['exports', 'ghost-admin/mixins/text-input'], function (exports, _textInput) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.TextField.extend(_textInput.default, {
        classNames: 'gh-input'
    });
});