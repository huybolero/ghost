define('ghost-admin/components/modal-new-subscriber', ['exports', 'ghost-admin/components/modal-base', 'ember-ajax/errors', 'ember-concurrency'], function (exports, _modalBase, _errors, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    exports.default = _modalBase.default.extend({

        subscriber: Ember.computed.alias('model'),

        actions: {
            updateEmail(newEmail) {
                this.set('subscriber.email', newEmail);
                this.set('subscriber.hasValidated', Ember.A());
                this.get('subscriber.errors').clear();
            },

            confirm() {
                this.get('addSubscriber').perform();
            }
        },

        addSubscriber: (0, _emberConcurrency.task)(function* () {
            try {
                yield this.get('confirm')();
                this.send('closeModal');
            } catch (error) {
                // TODO: server-side validation errors should be serialized
                // properly so that errors are added to model.errors automatically
                if (error && (0, _errors.isInvalidError)(error)) {
                    var _error$payload$errors = _slicedToArray(error.payload.errors, 1);

                    let firstError = _error$payload$errors[0];
                    let message = firstError.message;


                    if (message && message.match(/email/i)) {
                        this.get('subscriber.errors').add('email', message);
                        this.get('subscriber.hasValidated').pushObject('email');
                        return;
                    }
                }

                // route action so it should bubble up to the global error handler
                if (error) {
                    throw error;
                }
            }
        }).drop()
    });
});