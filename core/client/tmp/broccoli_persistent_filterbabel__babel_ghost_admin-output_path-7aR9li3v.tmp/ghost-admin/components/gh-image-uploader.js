define('ghost-admin/components/gh-image-uploader', ['exports', 'ghost-admin/utils/ghost-paths', 'ghost-admin/services/ajax'], function (exports, _ghostPaths, _ajax) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.IMAGE_EXTENSIONS = exports.IMAGE_MIME_TYPES = undefined;

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    const IMAGE_MIME_TYPES = exports.IMAGE_MIME_TYPES = 'image/gif,image/jpg,image/jpeg,image/png,image/svg+xml';
    const IMAGE_EXTENSIONS = exports.IMAGE_EXTENSIONS = ['gif', 'jpg', 'jpeg', 'png', 'svg'];

    exports.default = Ember.Component.extend({
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),
        notifications: Ember.inject.service(),
        settings: Ember.inject.service(),

        tagName: 'section',
        classNames: ['gh-image-uploader'],
        classNameBindings: ['dragClass'],

        image: null,
        text: '',
        altText: '',
        saveButton: true,
        accept: null,
        extensions: null,
        uploadUrl: null,
        validate: null,
        allowUnsplash: false,

        dragClass: null,
        failureMessage: null,
        file: null,
        url: null,
        uploadPercentage: 0,

        _defaultAccept: IMAGE_MIME_TYPES,
        _defaultExtensions: IMAGE_EXTENSIONS,
        _defaultUploadUrl: '/uploads/',
        _showUnsplash: false,

        // Allowed actions
        fileSelected: () => {},
        update: () => {},
        uploadStarted: () => {},
        uploadFinished: () => {},
        uploadSuccess: () => {},
        uploadFailed: () => {},

        // TODO: this wouldn't be necessary if the server could accept direct
        // file uploads
        formData: Ember.computed('file', function () {
            let file = this.get('file');
            let formData = new FormData();

            formData.append('uploadimage', file);

            return formData;
        }),

        description: Ember.computed('text', 'altText', function () {
            let altText = this.get('altText');

            return this.get('text') || (altText ? `Upload image of "${altText}"` : 'Upload an image');
        }),

        progressStyle: Ember.computed('uploadPercentage', function () {
            let percentage = this.get('uploadPercentage');
            let width = '';

            if (percentage > 0) {
                width = `${percentage}%`;
            } else {
                width = '0';
            }

            return Ember.String.htmlSafe(`width: ${width}`);
        }),

        didReceiveAttrs() {
            let image = this.get('image');
            this.set('url', image);

            if (!this.get('accept')) {
                this.set('accept', this.get('_defaultAccept'));
            }
            if (!this.get('extensions')) {
                this.set('extensions', this.get('_defaultExtensions'));
            }
            if (!this.get('uploadUrl')) {
                this.set('uploadUrl', this.get('_defaultUploadUrl'));
            }
        },

        actions: {
            fileSelected(fileList, resetInput) {
                // can't use array destructuring here as FileList is not a strict
                // array and fails in Safari
                // eslint-disable-next-line ember-suave/prefer-destructuring
                let file = fileList[0];
                let validationResult = this._validate(file);

                this.set('file', file);
                this.fileSelected(file);

                if (validationResult === true) {
                    Ember.run.schedule('actions', this, function () {
                        this.generateRequest();

                        if (resetInput) {
                            resetInput();
                        }
                    });
                } else {
                    this._uploadFailed(validationResult);

                    if (resetInput) {
                        resetInput();
                    }
                }
            },

            addUnsplashPhoto(photo) {
                this.set('url', photo.urls.regular);
                this.send('saveUrl');
            },

            reset() {
                this.set('file', null);
                this.set('uploadPercentage', 0);
            },

            saveUrl() {
                let url = this.get('url');
                this.update(url);
            }
        },

        dragOver(event) {
            if (!event.dataTransfer) {
                return;
            }

            // this is needed to work around inconsistencies with dropping files
            // from Chrome's downloads bar
            if (navigator.userAgent.indexOf('Chrome') > -1) {
                let eA = event.dataTransfer.effectAllowed;
                event.dataTransfer.dropEffect = eA === 'move' || eA === 'linkMove' ? 'move' : 'copy';
            }

            event.stopPropagation();
            event.preventDefault();

            this.set('dragClass', '-drag-over');
        },

        dragLeave(event) {
            event.preventDefault();
            this.set('dragClass', null);
        },

        drop(event) {
            event.preventDefault();

            this.set('dragClass', null);

            if (event.dataTransfer.files) {
                this.send('fileSelected', event.dataTransfer.files);
            }
        },

        _uploadProgress(event) {
            if (event.lengthComputable) {
                Ember.run(() => {
                    let percentage = Math.round(event.loaded / event.total * 100);
                    this.set('uploadPercentage', percentage);
                });
            }
        },

        _uploadSuccess(response) {
            this.set('url', response);
            this.send('saveUrl');
            this.send('reset');
            this.uploadSuccess(response);
        },

        _uploadFailed(error) {
            let message;

            if ((0, _ajax.isVersionMismatchError)(error)) {
                this.get('notifications').showAPIError(error);
            }

            if ((0, _ajax.isUnsupportedMediaTypeError)(error)) {
                let validExtensions = this.get('extensions').join(', .').toUpperCase();
                validExtensions = `.${validExtensions}`;

                message = `The image type you uploaded is not supported. Please use ${validExtensions}`;
            } else if ((0, _ajax.isRequestEntityTooLargeError)(error)) {
                message = 'The image you uploaded was larger than the maximum file size your server allows.';
            } else if (error.payload.errors && !Ember.isBlank(error.payload.errors[0].message)) {
                message = error.payload.errors[0].message;
            } else {
                message = 'Something went wrong :(';
            }

            this.set('failureMessage', message);
            this.uploadFailed(error);
        },

        generateRequest() {
            let ajax = this.get('ajax');
            let formData = this.get('formData');
            let uploadUrl = this.get('uploadUrl');
            // CASE: we want to upload an icon and we have to POST it to a different endpoint, expecially for icons
            let url = `${(0, _ghostPaths.default)().apiRoot}${uploadUrl}`;

            this.uploadStarted();

            ajax.post(url, {
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'text',
                xhr: () => {
                    let xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener('progress', event => {
                        this._uploadProgress(event);
                    }, false);

                    return xhr;
                }
            }).then(response => {
                let url = JSON.parse(response);
                this._uploadSuccess(url);
            }).catch(error => {
                this._uploadFailed(error);
            }).finally(() => {
                this.uploadFinished();
            });
        },

        _validate(file) {
            if (this.validate) {
                return this.validate(file);
            } else {
                return this._defaultValidator(file);
            }
        },

        _defaultValidator(file) {
            let extensions = this.get('extensions');

            var _$exec = /(?:\.([^.]+))?$/.exec(file.name),
                _$exec2 = _slicedToArray(_$exec, 2);

            let extension = _$exec2[1];


            if (!Ember.isArray(extensions)) {
                extensions = extensions.split(',');
            }

            if (!extension || extensions.indexOf(extension.toLowerCase()) === -1) {
                return new _ajax.UnsupportedMediaTypeError();
            }

            return true;
        }
    });
});