define('ghost-admin/components/koenig-card-html', ['exports', 'koenig-editor/components/koenig-card-html'], function (exports, _koenigCardHtml) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCardHtml.default;
    }
  });
});