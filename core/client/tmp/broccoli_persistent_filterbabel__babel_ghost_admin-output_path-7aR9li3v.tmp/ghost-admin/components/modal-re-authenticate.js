define('ghost-admin/components/modal-re-authenticate', ['exports', 'ghost-admin/components/modal-base', 'ghost-admin/mixins/validation-engine', 'ghost-admin/services/ajax', 'ember-concurrency'], function (exports, _modalBase, _validationEngine, _ajax, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend(_validationEngine.default, {
        config: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),

        validationType: 'signin',

        authenticationError: null,

        identification: Ember.computed('session.user.email', function () {
            return this.get('session.user.email');
        }),

        actions: {
            confirm() {
                this.get('reauthenticate').perform();
            }
        },

        _authenticate() {
            let session = this.get('session');
            let authStrategy = 'authenticator:oauth2';
            let identification = this.get('identification');
            let password = this.get('password');

            session.set('skipAuthSuccessHandler', true);

            this.toggleProperty('submitting');

            return session.authenticate(authStrategy, identification, password).finally(() => {
                this.toggleProperty('submitting');
                session.set('skipAuthSuccessHandler', undefined);
            });
        },

        _passwordConfirm() {
            // Manually trigger events for input fields, ensuring legacy compatibility with
            // browsers and password managers that don't send proper events on autofill
            Ember.$('#login').find('input').trigger('change');

            this.set('authenticationError', null);

            return this.validate({ property: 'signin' }).then(() => this._authenticate().then(() => {
                this.get('notifications').closeAlerts();
                this.send('closeModal');
                return true;
            }).catch(error => {
                if (error && error.payload && error.payload.errors) {
                    error.payload.errors.forEach(err => {
                        if ((0, _ajax.isVersionMismatchError)(err)) {
                            return this.get('notifications').showAPIError(error);
                        }
                        err.message = Ember.String.htmlSafe(err.context || err.message);
                    });

                    this.get('errors').add('password', 'Incorrect password');
                    this.get('hasValidated').pushObject('password');
                    this.set('authenticationError', error.payload.errors[0].message);
                }
            }), () => {
                this.get('hasValidated').pushObject('password');
                return false;
            });
        },

        reauthenticate: (0, _emberConcurrency.task)(function* () {
            return yield this._passwordConfirm();
        }).drop()
    });
});