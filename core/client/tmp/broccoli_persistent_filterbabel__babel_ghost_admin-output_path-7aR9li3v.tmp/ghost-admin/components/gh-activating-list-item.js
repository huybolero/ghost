define('ghost-admin/components/gh-activating-list-item', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        active: false,
        classNameBindings: ['active'],
        linkClasses: null,
        tagName: 'li',

        actions: {
            setActive(value) {
                Ember.run.schedule('afterRender', this, function () {
                    this.set('active', value);
                });
            }
        },

        click() {
            this.$('a').blur();
        }
    });
});