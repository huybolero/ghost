define('ghost-admin/components/gh-file-uploader', ['exports', 'ghost-admin/services/ajax'], function (exports, _ajax) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    const DEFAULTS = {
        accept: ['text/csv'],
        extensions: ['csv']
    };

    exports.default = Ember.Component.extend({
        ajax: Ember.inject.service(),
        eventBus: Ember.inject.service(),
        notifications: Ember.inject.service(),

        tagName: 'section',
        classNames: ['gh-image-uploader'],
        classNameBindings: ['dragClass'],

        labelText: 'Select or drag-and-drop a file',
        url: null,
        paramName: 'file',
        accept: null,
        extensions: null,
        validate: null,

        file: null,
        response: null,

        dragClass: null,
        failureMessage: null,
        uploadPercentage: 0,

        // Allowed actions
        fileSelected: () => {},
        uploadStarted: () => {},
        uploadFinished: () => {},
        uploadSuccess: () => {},
        uploadFailed: () => {},

        formData: Ember.computed('file', function () {
            let paramName = this.get('paramName');
            let file = this.get('file');
            let formData = new FormData();

            formData.append(paramName, file);

            return formData;
        }),

        progressStyle: Ember.computed('uploadPercentage', function () {
            let percentage = this.get('uploadPercentage');
            let width = '';

            if (percentage > 0) {
                width = `${percentage}%`;
            } else {
                width = '0';
            }

            return Ember.String.htmlSafe(`width: ${width}`);
        }),

        // we can optionally listen to a named event bus channel so that the upload
        // process can be triggered externally
        init() {
            this._super(...arguments);
            let listenTo = this.get('listenTo');

            this.accept = this.accept || DEFAULTS.accept;
            this.extensions = this.extensions || DEFAULTS.extensions;

            if (listenTo) {
                this.get('eventBus').subscribe(`${listenTo}:upload`, this, function (file) {
                    if (file) {
                        this.set('file', file);
                    }
                    this.send('upload');
                });
            }
        },

        didReceiveAttrs() {
            this._super(...arguments);
            let accept = this.get('accept');
            let extensions = this.get('extensions');

            this._accept = !Ember.isBlank(accept) && !Ember.isArray(accept) ? accept.split(',') : accept;
            this._extensions = !Ember.isBlank(extensions) && !Ember.isArray(extensions) ? extensions.split(',') : extensions;
        },

        willDestroyElement() {
            let listenTo = this.get('listenTo');

            this._super(...arguments);

            if (listenTo) {
                this.get('eventBus').unsubscribe(`${listenTo}:upload`);
            }
        },

        actions: {
            fileSelected(fileList, resetInput) {
                var _Array$from = Array.from(fileList),
                    _Array$from2 = _slicedToArray(_Array$from, 1);

                let file = _Array$from2[0];

                let validationResult = this._validate(file);

                this.set('file', file);
                this.fileSelected(file);

                if (validationResult === true) {
                    Ember.run.schedule('actions', this, function () {
                        this.generateRequest();

                        if (resetInput) {
                            resetInput();
                        }
                    });
                } else {
                    this._uploadFailed(validationResult);

                    if (resetInput) {
                        resetInput();
                    }
                }
            },

            upload() {
                if (this.get('file')) {
                    this.generateRequest();
                }
            },

            reset() {
                this.set('file', null);
                this.set('uploadPercentage', 0);
                this.set('failureMessage', null);
            }
        },

        dragOver(event) {
            if (!event.dataTransfer) {
                return;
            }

            // this is needed to work around inconsistencies with dropping files
            // from Chrome's downloads bar
            if (navigator.userAgent.indexOf('Chrome') > -1) {
                let eA = event.dataTransfer.effectAllowed;
                event.dataTransfer.dropEffect = eA === 'move' || eA === 'linkMove' ? 'move' : 'copy';
            }

            event.stopPropagation();
            event.preventDefault();

            this.set('dragClass', '-drag-over');
        },

        dragLeave(event) {
            event.preventDefault();
            this.set('dragClass', null);
        },

        drop(event) {
            event.preventDefault();
            this.set('dragClass', null);
            if (event.dataTransfer.files) {
                this.send('fileSelected', event.dataTransfer.files);
            }
        },

        generateRequest() {
            let ajax = this.get('ajax');
            let formData = this.get('formData');
            let url = this.get('url');

            this.uploadStarted();

            ajax.post(url, {
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'text',
                xhr: () => {
                    let xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener('progress', event => {
                        this._uploadProgress(event);
                    }, false);

                    return xhr;
                }
            }).then(response => {
                this._uploadSuccess(JSON.parse(response));
            }).catch(error => {
                this._uploadFailed(error);
            }).finally(() => {
                this.uploadFinished();
            });
        },

        _uploadProgress(event) {
            if (event.lengthComputable) {
                Ember.run(() => {
                    let percentage = Math.round(event.loaded / event.total * 100);
                    this.set('uploadPercentage', percentage);
                });
            }
        },

        _uploadSuccess(response) {
            this.uploadSuccess(response);
            this.send('reset');
        },

        _uploadFailed(error) {
            let message;

            if ((0, _ajax.isVersionMismatchError)(error)) {
                this.get('notifications').showAPIError(error);
            }

            if ((0, _ajax.isUnsupportedMediaTypeError)(error)) {
                message = 'The file type you uploaded is not supported.';
            } else if ((0, _ajax.isRequestEntityTooLargeError)(error)) {
                message = 'The file you uploaded was larger than the maximum file size your server allows.';
            } else if (error.payload && error.payload.errors && !Ember.isBlank(error.payload.errors[0].message)) {
                message = Ember.String.htmlSafe(error.payload.errors[0].message);
            } else {
                message = 'Something went wrong :(';
            }

            this.set('failureMessage', message);
            this.uploadFailed(error);
        },

        _validate(file) {
            if (this.validate) {
                return this.validate(file);
            } else {
                return this._defaultValidator(file);
            }
        },

        _defaultValidator(file) {
            var _$exec = /(?:\.([^.]+))?$/.exec(file.name),
                _$exec2 = _slicedToArray(_$exec, 2);

            let extension = _$exec2[1];

            let extensions = this._extensions;

            if (!extension || extensions.indexOf(extension.toLowerCase()) === -1) {
                return new _ajax.UnsupportedMediaTypeError();
            }

            return true;
        }
    });
});