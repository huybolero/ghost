define('ghost-admin/components/gh-alert', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        notifications: Ember.inject.service(),

        classNameBindings: ['typeClass'],
        classNames: ['gh-alert'],
        tagName: 'article',

        typeClass: Ember.computed('message.type', function () {
            let type = this.get('message.type');
            let classes = '';
            let typeMapping;

            typeMapping = {
                success: 'green',
                error: 'red',
                warn: 'blue',
                info: 'blue'
            };

            if (typeMapping[type] !== undefined) {
                classes += `gh-alert-${typeMapping[type]}`;
            }

            return classes;
        }),

        actions: {
            closeNotification() {
                this.get('notifications').closeNotification(this.get('message'));
            }
        }
    });
});