define('ghost-admin/components/modal-theme-warnings', ['exports', 'ghost-admin/components/modal-base'], function (exports, _modalBase) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        'data-test-theme-warnings-modal': true,

        title: Ember.computed.reads('model.title'),
        message: Ember.computed.reads('model.message'),
        warnings: Ember.computed.reads('model.warnings'),
        errors: Ember.computed.reads('model.errors'),
        fatalErrors: Ember.computed.reads('model.fatalErrors'),
        canActivate: Ember.computed.reads('model.canActivate')
    });
});