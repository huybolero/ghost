define('ghost-admin/components/gh-content-cover', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        ui: Ember.inject.service(),

        classNames: ['content-cover'],

        onMouseEnter: null,

        click() {
            this.get('ui').closeMenus();
        },

        mouseEnter() {
            this.get('ui').closeAutoNav();
        }
    });
});