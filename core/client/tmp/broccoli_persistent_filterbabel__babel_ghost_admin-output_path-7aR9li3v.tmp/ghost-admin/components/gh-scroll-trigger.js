define('ghost-admin/components/gh-scroll-trigger', ['exports', 'ember-in-viewport'], function (exports, _emberInViewport) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend(_emberInViewport.default, {

        enter() {},
        exit() {},
        registerElement() {},

        didInsertElement() {
            let offset = this.get('triggerOffset') || {};

            // if triggerOffset is a number we use it for all dimensions
            if (typeof offset === 'number') {
                offset = {
                    top: offset,
                    bottom: offset,
                    left: offset,
                    right: offset
                };
            }

            this.set('viewportSpy', true);
            this.set('viewportTolerance', {
                top: offset.top,
                bottom: offset.bottom,
                left: offset.left,
                right: offset.right
            });

            this._super(...arguments);

            this.registerElement(this.element);
        },

        didEnterViewport() {
            return this.enter();
        },

        didExitViewport() {
            return this.exit();
        }

    });
});