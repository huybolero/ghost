define('ghost-admin/components/koenig-link-input', ['exports', 'koenig-editor/components/koenig-link-input'], function (exports, _koenigLinkInput) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigLinkInput.default;
    }
  });
});