define('ghost-admin/components/gh-subscribers-table', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        classNames: ['subscribers-table'],

        table: null,

        actions: {
            onScrolledToBottom() {
                let loadNextPage = this.get('loadNextPage');

                if (!this.get('isLoading')) {
                    loadNextPage();
                }
            }
        }
    });
});