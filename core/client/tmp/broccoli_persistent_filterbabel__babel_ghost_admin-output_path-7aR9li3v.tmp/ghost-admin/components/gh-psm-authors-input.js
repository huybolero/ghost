define('ghost-admin/components/gh-psm-authors-input', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({

        store: Ember.inject.service(),

        // public attrs
        selectedAuthors: null,
        tagName: '',
        triggerId: '',

        // internal attrs
        availableAuthors: null,

        // closure actions
        updateAuthors() {},

        availableAuthorNames: Ember.computed('availableAuthors.@each.name', function () {
            return this.get('availableAuthors').map(author => author.get('name').toLowerCase());
        }),

        init() {
            this._super(...arguments);
            // perform a background query to fetch all users and set `availableAuthors`
            // to a live-query that will be immediately populated with what's in the
            // store and be updated when the above query returns
            this.store.query('user', { limit: 'all' });
            this.set('availableAuthors', this.store.peekAll('user'));
        },

        actions: {
            updateAuthors(newAuthors) {
                this.updateAuthors(newAuthors);
            }
        }

    });
});