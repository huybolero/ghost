define('ghost-admin/components/gh-loading-spinner', ['exports', 'ember-concurrency'], function (exports, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: '',

        showSpinner: false,
        // ms until the loader is displayed,
        // prevents unnecessary flash of spinner
        slowLoadTimeout: 200,

        didInsertElement() {
            this.get('startSpinnerTimeout').perform();
        },

        startSpinnerTimeout: (0, _emberConcurrency.task)(function* () {
            yield (0, _emberConcurrency.timeout)(this.get('slowLoadTimeout'));
            this.set('showSpinner', true);
        })
    });
});