define('ghost-admin/components/modal-upload-theme', ['exports', 'ghost-admin/components/modal-base', 'ghost-admin/utils/ghost-paths', 'ghost-admin/services/ajax'], function (exports, _modalBase, _ghostPaths, _ajax) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    const DEFAULTS = {
        accept: ['application/zip', 'application/x-zip-compressed'],
        extensions: ['zip']
    };

    exports.default = _modalBase.default.extend({
        eventBus: Ember.inject.service(),
        store: Ember.inject.service(),

        accept: null,
        extensions: null,
        themes: null,
        closeDisabled: false,
        file: null,
        theme: false,
        displayOverwriteWarning: false,

        hideUploader: Ember.computed.or('theme', 'displayOverwriteWarning'),
        currentThemeNames: Ember.computed.mapBy('model.themes', 'name'),

        uploadUrl: Ember.computed(function () {
            return `${(0, _ghostPaths.default)().apiRoot}/themes/upload/`;
        }),

        themeName: Ember.computed('theme.{name,package.name}', function () {
            let themePackage = this.get('theme.package');
            let name = this.get('theme.name');

            return themePackage ? `${themePackage.name} - ${themePackage.version}` : name;
        }),

        fileThemeName: Ember.computed('file', function () {
            let file = this.get('file');
            return file.name.replace(/\.zip$/, '');
        }),

        canActivateTheme: Ember.computed('theme', function () {
            let theme = this.get('theme');
            return theme && !theme.get('active');
        }),

        init() {
            this._super(...arguments);

            this.accept = this.accept || DEFAULTS.accept;
            this.extensions = this.extensions || DEFAULTS.extensions;
        },

        actions: {
            validateTheme(file) {
                let themeName = file.name.replace(/\.zip$/, '').replace(/[^\w@.]/gi, '-').toLowerCase();

                let currentThemeNames = this.get('currentThemeNames');

                this.set('file', file);

                var _$exec = /(?:\.([^.]+))?$/.exec(file.name),
                    _$exec2 = _slicedToArray(_$exec, 2);

                let extension = _$exec2[1];

                let extensions = this.get('extensions');

                if (!extension || extensions.indexOf(extension.toLowerCase()) === -1) {
                    return new _ajax.UnsupportedMediaTypeError();
                }

                if (file.name.match(/^casper\.zip$/i)) {
                    return { payload: { errors: [{ message: 'Sorry, the default Casper theme cannot be overwritten.<br>Please rename your zip file and try again.' }] } };
                }

                if (!this._allowOverwrite && currentThemeNames.includes(themeName)) {
                    this.set('displayOverwriteWarning', true);
                    return false;
                }

                return true;
            },

            confirmOverwrite() {
                this._allowOverwrite = true;
                this.set('displayOverwriteWarning', false);

                // we need to schedule afterRender so that the upload component is
                // displayed again in order to subscribe/respond to the event bus
                Ember.run.schedule('afterRender', this, function () {
                    this.get('eventBus').publish('themeUploader:upload', this.get('file'));
                });
            },

            uploadStarted() {
                this.set('closeDisabled', true);
            },

            uploadFinished() {
                this.set('closeDisabled', false);
            },

            uploadSuccess(response) {
                this.get('store').pushPayload(response);

                let theme = this.get('store').peekRecord('theme', response.themes[0].name);

                this.set('theme', theme);

                if (Ember.get(theme, 'warnings.length') > 0) {
                    this.set('validationWarnings', Ember.get(theme, 'warnings'));
                }

                // Ghost differentiates between errors and fatal errors
                // You can't activate a theme with fatal errors, but with errors.
                if (Ember.get(theme, 'errors.length') > 0) {
                    this.set('validationErrors', Ember.get(theme, 'errors'));
                }

                this.set('hasWarningsOrErrors', this.get('validationErrors.length') || this.get('validationWarnings.length'));

                // invoke the passed in confirm action
                this.get('model.uploadSuccess')(theme);
            },

            uploadFailed(error) {
                if ((0, _ajax.isThemeValidationError)(error)) {
                    let errors = error.payload.errors[0].errorDetails;
                    let fatalErrors = [];
                    let normalErrors = [];

                    // to have a proper grouping of fatal errors and none fatal, we need to check
                    // our errors for the fatal property
                    if (errors && errors.length > 0) {
                        for (let i = 0; i < errors.length; i += 1) {
                            if (errors[i].fatal) {
                                fatalErrors.push(errors[i]);
                            } else {
                                normalErrors.push(errors[i]);
                            }
                        }
                    }

                    this.set('fatalValidationErrors', fatalErrors);
                    this.set('validationErrors', normalErrors);
                }
            },

            confirm() {
                // noop - we don't want the enter key doing anything
            },

            activate() {
                this.get('model.activate')(this.get('theme'));
                this.closeModal();
            },

            closeModal() {
                if (!this.get('closeDisabled')) {
                    this._super(...arguments);
                }
            },

            reset() {
                this.set('validationWarnings', []);
                this.set('validationErrors', []);
                this.set('fatalValidationErrors', []);
                this.set('hasWarningsOrErrors', false);
            }
        }
    });
});