define('ghost-admin/components/koenig-card-markdown', ['exports', 'koenig-editor/components/koenig-card-markdown'], function (exports, _koenigCardMarkdown) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCardMarkdown.default;
    }
  });
});