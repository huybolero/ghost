define('ghost-admin/components/gh-cm-editor', ['exports', 'ghost-admin/utils/bound-one-way', 'ember-concurrency'], function (exports, _boundOneWay, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    /* global CodeMirror */
    const CmEditorComponent = Ember.Component.extend({
        lazyLoader: Ember.inject.service(),

        classNameBindings: ['isFocused:focus'],

        isFocused: false,
        isInitializingCodemirror: true,

        // options for the editor
        autofocus: false,
        indentUnit: 4,
        lineNumbers: true,
        lineWrapping: false,
        mode: 'htmlmixed',
        theme: 'xq-light',

        _editor: null, // reference to CodeMirror editor

        // Allowed actions
        'focus-in': () => {},
        update: () => {},

        _value: (0, _boundOneWay.default)('value'), // make sure a value exists

        didReceiveAttrs() {
            if (this.get('value') === null || undefined) {
                this.set('value', '');
            }
        },

        didInsertElement() {
            this._super(...arguments);
            this.get('initCodeMirror').perform();
        },

        willDestroyElement() {
            this._super(...arguments);

            // Ensure the editor exists before trying to destroy it. This fixes
            // an error that occurs if codemirror hasn't finished loading before
            // the component is destroyed.
            if (this._editor) {
                let editor = this._editor.getWrapperElement();
                editor.parentNode.removeChild(editor);
                this._editor = null;
            }
        },

        actions: {
            updateFromTextarea(value) {
                this.update(value);
            }
        },

        initCodeMirror: (0, _emberConcurrency.task)(function* () {
            let loader = this.get('lazyLoader');

            yield Ember.RSVP.all([loader.loadStyle('codemirror', 'assets/codemirror/codemirror.css'), loader.loadScript('codemirror', 'assets/codemirror/codemirror.js')]);

            Ember.run.scheduleOnce('afterRender', this, function () {
                this._initCodeMirror();
            });
        }),

        _initCodeMirror() {
            let options = this.getProperties('lineNumbers', 'lineWrapping', 'indentUnit', 'mode', 'theme', 'autofocus');
            Ember.assign(options, { value: this.get('_value') });

            let textarea = this.element.querySelector('textarea');
            if (textarea && textarea === document.activeElement) {
                options.autofocus = true;
            }

            this.set('isInitializingCodemirror', false);
            this._editor = new CodeMirror(this.element, options);

            // by default CodeMirror will place the cursor at the beginning of the
            // content, it makes more sense for the cursor to be at the end
            if (options.autofocus) {
                this._editor.setCursor(this._editor.lineCount(), 0);
            }

            // events
            this._setupCodeMirrorEventHandler('focus', this, this._focus);
            this._setupCodeMirrorEventHandler('blur', this, this._blur);
            this._setupCodeMirrorEventHandler('change', this, this._update);
        },

        _setupCodeMirrorEventHandler(event, target, method) {
            let callback = Ember.run.bind(target, method);

            this._editor.on(event, callback);

            this.one('willDestroyElement', this, function () {
                this._editor.off(event, callback);
            });
        },

        _update(codeMirror, changeObj) {
            Ember.run.once(this, this.update, codeMirror.getValue(), codeMirror, changeObj);
        },

        _focus(codeMirror, event) {
            this.set('isFocused', true);
            Ember.run.once(this, this.get('focus-in'), codeMirror.getValue(), codeMirror, event);
        },

        _blur() /* codeMirror, event */{
            this.set('isFocused', false);
        }
    });

    CmEditorComponent.reopenClass({
        positionalParams: ['value']
    });

    exports.default = CmEditorComponent;
});