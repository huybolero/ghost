define('ghost-admin/components/gh-basic-dropdown', ['exports', 'ember-basic-dropdown/components/basic-dropdown', 'ember-basic-dropdown/templates/components/basic-dropdown'], function (exports, _basicDropdown, _basicDropdown2) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _basicDropdown.default.extend({
        dropdown: Ember.inject.service(),

        layout: _basicDropdown2.default,

        didInsertElement() {
            this._super(...arguments);
            this.get('dropdown').on('close', this, this.close);
        },

        willDestroyElement() {
            this._super(...arguments);
            this.get('dropdown').off('close');
        }
    });
});