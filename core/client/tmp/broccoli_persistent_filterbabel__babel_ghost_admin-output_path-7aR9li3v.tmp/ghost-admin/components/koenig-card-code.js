define('ghost-admin/components/koenig-card-code', ['exports', 'koenig-editor/components/koenig-card-code'], function (exports, _koenigCardCode) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCardCode.default;
    }
  });
});