define('ghost-admin/components/gh-file-input', ['exports', 'emberx-file-input/components/x-file-input'], function (exports, _xFileInput) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _xFileInput.default.extend({
        change(e) {
            let action = this.get('action');
            let files = this.files(e);

            if (files.length && action) {
                action(files, this.resetInput.bind(this));
            }
        },

        /**
        * Gets files from event object.
        *
        * @method
        * @private
        * @param {$.Event || Event}
        */
        files(e) {
            return (e.originalEvent || e).testingFiles || e.target.files;
        }
    });
});