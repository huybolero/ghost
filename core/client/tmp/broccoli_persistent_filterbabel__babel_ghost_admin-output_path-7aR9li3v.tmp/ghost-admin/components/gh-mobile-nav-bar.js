define('ghost-admin/components/gh-mobile-nav-bar', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        ui: Ember.inject.service(),

        tagName: 'nav',
        classNames: ['gh-mobile-nav-bar']
    });
});