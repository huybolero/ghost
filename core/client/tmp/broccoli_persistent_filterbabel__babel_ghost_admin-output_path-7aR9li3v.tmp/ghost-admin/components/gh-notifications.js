define('ghost-admin/components/gh-notifications', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        notifications: Ember.inject.service(),

        tagName: 'aside',
        classNames: 'gh-notifications',

        messages: Ember.computed.alias('notifications.notifications')
    });
});