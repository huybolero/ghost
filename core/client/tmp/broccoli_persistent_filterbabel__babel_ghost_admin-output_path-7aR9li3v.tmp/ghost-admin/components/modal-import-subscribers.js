define('ghost-admin/components/modal-import-subscribers', ['exports', 'ghost-admin/components/modal-base', 'ghost-admin/utils/ghost-paths'], function (exports, _modalBase, _ghostPaths) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        labelText: 'Select or drag-and-drop a CSV File',

        response: null,
        closeDisabled: false,

        // Allowed actions
        confirm: () => {},

        uploadUrl: Ember.computed(function () {
            return `${(0, _ghostPaths.default)().apiRoot}/subscribers/csv/`;
        }),

        actions: {
            uploadStarted() {
                this.set('closeDisabled', true);
            },

            uploadFinished() {
                this.set('closeDisabled', false);
            },

            uploadSuccess(response) {
                this.set('response', response.meta.stats);
                // invoke the passed in confirm action
                this.confirm();
            },

            confirm() {
                // noop - we don't want the enter key doing anything
            },

            closeModal() {
                if (!this.get('closeDisabled')) {
                    this._super(...arguments);
                }
            }
        }
    });
});