define('ghost-admin/components/gh-menu-toggle', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        mediaQueries: Ember.inject.service(),

        classNames: ['gh-menu-toggle'],
        maximise: false,

        // closure actions
        desktopAction() {},
        mobileAction() {},

        isMobile: Ember.computed.reads('mediaQueries.isMobile'),

        iconClass: Ember.computed('maximise', 'isMobile', function () {
            if (this.get('maximise') && !this.get('isMobile')) {
                return 'icon-maximise';
            } else {
                return 'icon-minimise';
            }
        }),

        click() {
            if (this.get('isMobile')) {
                this.mobileAction();
            } else {
                this.toggleProperty('maximise');
                this.desktopAction();
            }
        }
    });
});