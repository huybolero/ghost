define('ghost-admin/components/gh-trim-focus-input', ['exports', 'ghost-admin/components/gh-text-input'], function (exports, _ghTextInput) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    /**
     * This doesn't override the OneWayInput component because
     * we need finer control. It borrows
     * parts from both the OneWayInput component and Ember's default
     * input component
     */
    const TrimFocusInputComponent = _ghTextInput.default.extend({

        shouldFocus: true,

        focusOut(event) {
            this._trimInput(event.target.value, event);
            this._super(...arguments);
        },

        _trimInput(value, event) {
            if (value && typeof value.trim === 'function') {
                value = value.trim();
            }

            this.element.value = value;
            this._elementValueDidChange(event);

            let inputMethod = this.get('input');
            if (inputMethod) {
                inputMethod(event);
            }
        }
    });

    exports.default = TrimFocusInputComponent;
});