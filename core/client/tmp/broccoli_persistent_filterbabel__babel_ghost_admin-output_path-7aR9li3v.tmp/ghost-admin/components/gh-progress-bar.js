define('ghost-admin/components/gh-progress-bar', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: '',

        // Public attributes
        percentage: 0,
        isError: false,

        // Internal attributes
        progressStyle: '',

        didReceiveAttrs() {
            this._super(...arguments);

            let percentage = this.get('percentage');
            let width = percentage > 0 ? `${percentage}%` : '0';

            this.set('progressStyle', Ember.String.htmlSafe(`width: ${width}`));
        }

    });
});