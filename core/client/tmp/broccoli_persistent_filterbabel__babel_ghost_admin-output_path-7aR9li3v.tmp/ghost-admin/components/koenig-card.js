define('ghost-admin/components/koenig-card', ['exports', 'koenig-editor/components/koenig-card'], function (exports, _koenigCard) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCard.default;
    }
  });
});