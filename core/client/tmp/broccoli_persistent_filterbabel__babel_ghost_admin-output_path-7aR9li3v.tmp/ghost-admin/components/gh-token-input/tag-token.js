define('ghost-admin/components/gh-token-input/tag-token', ['exports', 'ember-drag-drop/components/draggable-object'], function (exports, _draggableObject) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _draggableObject.default.extend({

        attributeBindings: ['title'],
        classNames: ['tag-token'],
        classNameBindings: ['internal:tag-token--internal'],

        internal: Ember.computed.readOnly('content.isInternal'),

        primary: Ember.computed('idx', 'internal', function () {
            return !this.get('internal') && this.get('idx') === 0;
        }),

        title: Ember.computed('internal', function () {
            if (this.get('internal')) {
                return `Internal tag`;
            }
        })

    });
});