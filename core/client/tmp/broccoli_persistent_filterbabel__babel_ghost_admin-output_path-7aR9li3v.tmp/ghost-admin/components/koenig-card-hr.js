define('ghost-admin/components/koenig-card-hr', ['exports', 'koenig-editor/components/koenig-card-hr'], function (exports, _koenigCardHr) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCardHr.default;
    }
  });
});