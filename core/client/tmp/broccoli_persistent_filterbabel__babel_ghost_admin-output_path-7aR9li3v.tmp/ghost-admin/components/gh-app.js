define('ghost-admin/components/gh-app', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        classNames: ['gh-app'],

        showSettingsMenu: false,

        didReceiveAttrs() {
            this._super(...arguments);
            let showSettingsMenu = this.get('showSettingsMenu');

            Ember.$('body').toggleClass('settings-menu-expanded', showSettingsMenu);
        }
    });
});