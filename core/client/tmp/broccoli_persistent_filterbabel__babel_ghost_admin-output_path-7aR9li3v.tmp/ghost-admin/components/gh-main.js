define('ghost-admin/components/gh-main', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: 'main',
        classNames: ['gh-main'],
        ariaRole: 'main',

        mouseEnter() {
            let action = this.get('onMouseEnter');
            if (action) {
                action();
            }
        }
    });
});