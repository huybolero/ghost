define('ghost-admin/components/koenig-card-image', ['exports', 'koenig-editor/components/koenig-card-image'], function (exports, _koenigCardImage) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigCardImage.default;
    }
  });
});