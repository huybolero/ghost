define('ghost-admin/components/modal-delete-user', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        // Allowed actions
        confirm: () => {},

        user: Ember.computed.alias('model'),

        actions: {
            confirm() {
                this.get('deleteUser').perform();
            }
        },

        deleteUser: (0, _emberConcurrency.task)(function* () {
            try {
                yield this.confirm();
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});