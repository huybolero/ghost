define('ghost-admin/components/modal-delete-all', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({

        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        store: Ember.inject.service(),
        ajax: Ember.inject.service(),

        actions: {
            confirm() {
                this.get('deleteAll').perform();
            }
        },

        _deleteAll() {
            let deleteUrl = this.get('ghostPaths.url').api('db');
            return this.get('ajax').del(deleteUrl);
        },

        _unloadData() {
            this.get('store').unloadAll('post');
            this.get('store').unloadAll('tag');
        },

        _showSuccess() {
            this.get('notifications').showAlert('All content deleted from database.', { type: 'success', key: 'all-content.delete.success' });
        },

        _showFailure(error) {
            this.get('notifications').showAPIError(error, { key: 'all-content.delete' });
        },

        deleteAll: (0, _emberConcurrency.task)(function* () {
            try {
                yield this._deleteAll();
                this._unloadData();
                this._showSuccess();
            } catch (error) {
                this._showFailure(error);
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});