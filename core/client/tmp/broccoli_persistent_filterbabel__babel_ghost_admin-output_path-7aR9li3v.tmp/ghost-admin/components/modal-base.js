define('ghost-admin/components/modal-base', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: 'section',
        classNames: 'modal-content',

        _previousKeymasterScope: null,

        // Allowed Actions
        closeModal: () => {},

        didInsertElement() {
            this._super(...arguments);
            this._setupShortcuts();
        },

        willDestroyElement() {
            this._super(...arguments);
            this._removeShortcuts();
        },

        actions: {
            confirm() {
                throw new Error('You must override the "confirm" action in your modal component');
            },

            closeModal() {
                this.closeModal();
            }
        },

        _setupShortcuts() {
            Ember.run(function () {
                document.activeElement.blur();
            });
            this._previousKeymasterScope = key.getScope();

            key('enter', 'modal', () => {
                this.send('confirm');
            });

            key('escape', 'modal', () => {
                this.send('closeModal');
            });

            key.setScope('modal');
        },

        _removeShortcuts() {
            key.unbind('enter', 'modal');
            key.unbind('escape', 'modal');

            key.setScope(this._previousKeymasterScope);
        }
    });
});