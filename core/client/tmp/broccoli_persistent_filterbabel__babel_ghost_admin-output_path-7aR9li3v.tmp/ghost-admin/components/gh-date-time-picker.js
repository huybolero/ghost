define('ghost-admin/components/gh-date-time-picker', ['exports', 'moment'], function (exports, _moment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        settings: Ember.inject.service(),

        tagName: '',

        date: '',
        time: '',
        errors: null,
        dateErrorProperty: null,
        timeErrorProperty: null,

        _time: '',
        _previousTime: '',
        _minDate: null,
        _maxDate: null,

        blogTimezone: Ember.computed.reads('settings.activeTimezone'),
        hasError: Ember.computed.or('dateError', 'timeError'),

        timezone: Ember.computed('blogTimezone', function () {
            let blogTimezone = this.get('blogTimezone');
            return _moment.default.utc().tz(blogTimezone).format('z');
        }),

        dateError: Ember.computed('errors.[]', 'dateErrorProperty', function () {
            let errors = this.get('errors');
            let property = this.get('dateErrorProperty');

            if (!Ember.isEmpty(errors.errorsFor(property))) {
                return errors.errorsFor(property).get('firstObject').message;
            }
        }),

        timeError: Ember.computed('errors.[]', 'timeErrorProperty', function () {
            let errors = this.get('errors');
            let property = this.get('timeErrorProperty');

            if (!Ember.isEmpty(errors.errorsFor(property))) {
                return errors.errorsFor(property).get('firstObject').message;
            }
        }),

        didReceiveAttrs() {
            let date = this.get('date');
            let time = this.get('time');
            let minDate = this.get('minDate');
            let maxDate = this.get('maxDate');
            let blogTimezone = this.get('blogTimezone');

            if (!Ember.isBlank(date)) {
                this.set('_date', (0, _moment.default)(date));
            } else {
                this.set('_date', (0, _moment.default)().tz(blogTimezone));
            }

            if (Ember.isBlank(time)) {
                this.set('_time', this.get('_date').format('HH:mm'));
            } else {
                this.set('_time', this.get('time'));
            }
            this.set('_previousTime', this.get('_time'));

            // unless min/max date is at midnight moment will diable that day
            if (minDate === 'now') {
                this.set('_minDate', (0, _moment.default)((0, _moment.default)().format('YYYY-MM-DD')));
            } else if (!Ember.isBlank(minDate)) {
                this.set('_minDate', (0, _moment.default)((0, _moment.default)(minDate).format('YYYY-MM-DD')));
            } else {
                this.set('_minDate', null);
            }

            if (maxDate === 'now') {
                this.set('_maxDate', (0, _moment.default)((0, _moment.default)().format('YYYY-MM-DD')));
            } else if (!Ember.isBlank(maxDate)) {
                this.set('_maxDate', (0, _moment.default)((0, _moment.default)(maxDate).format('YYYY-MM-DD')));
            } else {
                this.set('_maxDate', null);
            }
        },

        actions: {
            // if date or time is set and the other property is blank set that too
            // so that we don't get "can't be blank" errors
            setDate(date) {
                if (date !== this.get('_date')) {
                    this.get('setDate')(date);

                    if (Ember.isBlank(this.get('time'))) {
                        this.get('setTime')(this.get('_time'));
                    }
                }
            },

            setTime(time) {
                if (time.match(/^\d:\d\d$/)) {
                    time = `0${time}`;
                }

                if (time !== this.get('_previousTime')) {
                    this.get('setTime')(time);
                    this.set('_previousTime', time);

                    if (Ember.isBlank(this.get('date'))) {
                        this.get('setDate')(this.get('_date'));
                    }
                }
            }
        }
    });
});