define('ghost-admin/components/gh-url-preview', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        config: Ember.inject.service(),

        classNames: 'ghost-url-preview',
        prefix: null,
        slug: null,

        url: Ember.computed('slug', function () {
            // Get the blog URL and strip the scheme
            let blogUrl = this.get('config.blogUrl');
            // Remove `http[s]://`
            let noSchemeBlogUrl = blogUrl.substr(blogUrl.indexOf('://') + 3);

            // Get the prefix and slug values
            let prefix = this.get('prefix') ? `${this.get('prefix')}/` : '';
            let slug = this.get('slug') ? `${this.get('slug')}/` : '';

            // Join parts of the URL together with slashes
            let theUrl = `${noSchemeBlogUrl}/${prefix}${slug}`;

            return theUrl;
        })
    });
});