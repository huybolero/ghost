define('ghost-admin/components/modal-transfer-owner', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        user: null,

        // Allowed actions
        confirm: () => {},

        actions: {
            confirm() {
                this.get('transferOwnership').perform();
            }
        },

        transferOwnership: (0, _emberConcurrency.task)(function* () {
            try {
                yield this.confirm();
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});