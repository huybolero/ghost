define('ghost-admin/components/gh-alerts', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        notifications: Ember.inject.service(),

        classNames: 'gh-alerts',
        tagName: 'aside',

        messages: Ember.computed.alias('notifications.alerts')
    });
});