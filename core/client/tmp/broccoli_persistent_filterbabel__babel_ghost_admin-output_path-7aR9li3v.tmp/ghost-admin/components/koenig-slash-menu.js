define('ghost-admin/components/koenig-slash-menu', ['exports', 'koenig-editor/components/koenig-slash-menu'], function (exports, _koenigSlashMenu) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigSlashMenu.default;
    }
  });
});