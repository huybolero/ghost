define('ghost-admin/components/modal-delete-tag', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        // Allowed actions
        confirm: () => {},

        tag: Ember.computed.alias('model'),

        postInflection: Ember.computed('tag.count.posts', function () {
            return this.get('tag.count.posts') > 1 ? 'posts' : 'post';
        }),

        actions: {
            confirm() {
                this.get('deleteTag').perform();
            }
        },

        deleteTag: (0, _emberConcurrency.task)(function* () {
            try {
                yield this.confirm();
            } finally {
                this.send('closeModal');
            }
        }).drop()
    });
});