define('ghost-admin/components/gh-download-count', ['exports', 'ghost-admin/config/environment', 'ember-concurrency'], function (exports, _environment, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        ajax: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),

        tagName: '',
        count: '',

        didInsertElement() {
            this.get('_poll').perform();
        },

        _poll: (0, _emberConcurrency.task)(function* () {
            let url = this.get('ghostPaths.count');
            let pattern = /(-?\d+)(\d{3})/;

            try {
                let data = yield this.get('ajax').request(url);
                let count = data.count.toString();

                while (pattern.test(count)) {
                    count = count.replace(pattern, '$1,$2');
                }

                this.set('count', count);

                if (_environment.default.environment !== 'test') {
                    yield (0, _emberConcurrency.timeout)(2000);
                    this.get('_poll').perform();
                }
            } catch (e) {
                // no-op - we don't want to create noise for a failed download count
            }
        })
    });
});