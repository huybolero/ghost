define('ghost-admin/components/modal-leave-settings', ['exports', 'ghost-admin/components/modal-base'], function (exports, _modalBase) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        actions: {
            confirm() {
                this.confirm().finally(() => {
                    this.send('closeModal');
                });
            }
        },

        // Allowed actions
        confirm: () => Ember.RSVP.resolve()
    });
});