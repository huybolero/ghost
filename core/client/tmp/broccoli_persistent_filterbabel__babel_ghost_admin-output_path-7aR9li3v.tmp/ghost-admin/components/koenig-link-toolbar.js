define('ghost-admin/components/koenig-link-toolbar', ['exports', 'koenig-editor/components/koenig-link-toolbar'], function (exports, _koenigLinkToolbar) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigLinkToolbar.default;
    }
  });
});