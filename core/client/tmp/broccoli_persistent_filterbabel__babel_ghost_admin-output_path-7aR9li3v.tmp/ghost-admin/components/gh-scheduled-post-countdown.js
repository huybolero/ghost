define('ghost-admin/components/gh-scheduled-post-countdown', ['exports', 'moment'], function (exports, _moment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        clock: Ember.inject.service(),

        post: null,

        // countdown timer to show the time left until publish time for a scheduled post
        // starts 15 minutes before scheduled time
        countdown: Ember.computed('post.{publishedAtUTC,isScheduled}', 'clock.second', function () {
            let isScheduled = this.get('post.isScheduled');
            let publishTime = this.get('post.publishedAtUTC') || _moment.default.utc();
            let timeUntilPublished = publishTime.diff(_moment.default.utc(), 'minutes', true);
            let isPublishedSoon = timeUntilPublished > 0 && timeUntilPublished < 15;

            // force a recompute
            this.get('clock.second');

            if (isScheduled && isPublishedSoon) {
                return (0, _moment.default)(publishTime).fromNow();
            } else {
                return false;
            }
        })
    });
});