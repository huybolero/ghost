define('ghost-admin/components/gh-feature-flag', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    const FeatureFlagComponent = Ember.Component.extend({
        feature: Ember.inject.service(),

        tagName: 'label',
        classNames: 'checkbox',
        attributeBindings: ['for'],
        _flagValue: null,

        value: Ember.computed('_flagValue', {
            get() {
                return this.get('_flagValue');
            },
            set(key, value) {
                return this.set(`feature.${this.get('flag')}`, value);
            }
        }),

        for: Ember.computed('flag', function () {
            return `labs-${this.get('flag')}`;
        }),

        name: Ember.computed('flag', function () {
            return `labs[${this.get('flag')}]`;
        }),

        init() {
            this._super(...arguments);

            this.set('_flagValue', this.get(`feature.${this.get('flag')}`));
        }
    });

    FeatureFlagComponent.reopenClass({
        positionalParams: ['flag']
    });

    exports.default = FeatureFlagComponent;
});