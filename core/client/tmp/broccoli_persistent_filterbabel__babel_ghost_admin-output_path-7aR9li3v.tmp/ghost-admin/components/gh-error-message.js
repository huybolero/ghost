define('ghost-admin/components/gh-error-message', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: 'p',
        classNames: ['response'],

        errors: null,
        property: '',

        isVisible: Ember.computed.notEmpty('errors'),

        message: Ember.computed('errors.[]', 'property', function () {
            let property = this.get('property');
            let errors = this.get('errors');
            let messages = [];
            let index;

            if (!Ember.isEmpty(errors) && errors.get(property)) {
                errors.get(property).forEach(error => {
                    messages.push(error);
                });
                index = Math.floor(Math.random() * messages.length);
                return messages[index].message;
            }
        })
    });
});