define('ghost-admin/components/koenig-plus-menu', ['exports', 'koenig-editor/components/koenig-plus-menu'], function (exports, _koenigPlusMenu) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _koenigPlusMenu.default;
    }
  });
});