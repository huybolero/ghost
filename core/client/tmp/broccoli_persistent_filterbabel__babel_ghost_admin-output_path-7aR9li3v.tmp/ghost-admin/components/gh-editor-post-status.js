define('ghost-admin/components/gh-editor-post-status', ['exports', 'ghost-admin/config/environment', 'ember-concurrency'], function (exports, _environment, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        post: null,
        isSaving: false,

        'data-test-editor-post-status': true,

        _isSaving: false,

        isNew: Ember.computed.reads('post.isNew'),
        isScheduled: Ember.computed.reads('post.isScheduled'),

        isPublished: Ember.computed('post.{isPublished,pastScheduledTime}', function () {
            let isPublished = this.get('post.isPublished');
            let pastScheduledTime = this.get('post.pastScheduledTime');

            return isPublished || pastScheduledTime;
        }),

        // isSaving will only be true briefly whilst the post is saving,
        // we want to ensure that the "Saving..." message is shown for at least
        // a few seconds so that it's noticeable
        didReceiveAttrs() {
            if (this.get('isSaving')) {
                this.get('showSavingMessage').perform();
            }
        },

        showSavingMessage: (0, _emberConcurrency.task)(function* () {
            this.set('_isSaving', true);
            yield (0, _emberConcurrency.timeout)(_environment.default.environment === 'test' ? 0 : 3000);
            this.set('_isSaving', false);
        }).drop()
    });
});