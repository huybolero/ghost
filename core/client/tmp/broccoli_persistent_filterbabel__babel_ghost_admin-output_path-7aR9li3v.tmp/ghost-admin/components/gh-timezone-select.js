define('ghost-admin/components/gh-timezone-select', ['exports', 'moment'], function (exports, _moment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        clock: Ember.inject.service(),

        classNames: ['form-group', 'for-select'],

        activeTimezone: null,
        availableTimezones: null,

        // Allowed actions
        update: () => {},

        availableTimezoneNames: Ember.computed.mapBy('availableTimezones', 'name'),

        hasTimezoneOverride: Ember.computed('activeTimezone', 'availableTimezoneNames', function () {
            let activeTimezone = this.get('activeTimezone');
            let availableTimezoneNames = this.get('availableTimezoneNames');

            return !availableTimezoneNames.includes(activeTimezone);
        }),

        selectedTimezone: Ember.computed('activeTimezone', 'availableTimezones', 'hasTimezoneOverride', function () {
            let hasTimezoneOverride = this.get('hasTimezoneOverride');
            let activeTimezone = this.get('activeTimezone');
            let availableTimezones = this.get('availableTimezones');

            if (hasTimezoneOverride) {
                return { name: '', label: '' };
            }

            return availableTimezones.filterBy('name', activeTimezone).get('firstObject');
        }),

        selectableTimezones: Ember.computed('availableTimezones', 'hasTimezoneOverride', function () {
            let hasTimezoneOverride = this.get('hasTimezoneOverride');
            let availableTimezones = this.get('availableTimezones');

            if (hasTimezoneOverride) {
                return [{ name: '', label: '' }, ...availableTimezones];
            }

            return availableTimezones;
        }),

        localTime: Ember.computed('hasTimezoneOverride', 'activeTimezone', 'selectedTimezone', 'clock.second', function () {
            let hasTimezoneOverride = this.get('hasTimezoneOverride');
            let timezone = hasTimezoneOverride ? this.get('activeTimezone') : this.get('selectedTimezone.name');

            this.get('clock.second');
            return timezone ? (0, _moment.default)().tz(timezone).format('HH:mm:ss') : (0, _moment.default)().utc().format('HH:mm:ss');
        }),

        actions: {
            setTimezone(timezone) {
                this.update(timezone);
            }
        }
    });
});