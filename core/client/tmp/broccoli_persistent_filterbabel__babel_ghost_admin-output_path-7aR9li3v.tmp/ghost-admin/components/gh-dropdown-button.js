define('ghost-admin/components/gh-dropdown-button', ['exports', 'ghost-admin/mixins/dropdown-mixin'], function (exports, _dropdownMixin) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend(_dropdownMixin.default, {
        dropdown: Ember.inject.service(),

        tagName: 'button',
        attributeBindings: ['href', 'role'],
        role: 'button',

        // matches with the dropdown this button toggles
        dropdownName: null,

        // Notify dropdown service this dropdown should be toggled
        click(event) {
            this._super(event);
            this.get('dropdown').toggleDropdown(this.get('dropdownName'), this);

            if (this.get('tagName') === 'a') {
                return false;
            }
        }
    });
});