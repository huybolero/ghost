define('ghost-admin/components/modal-delete-subscriber', ['exports', 'ghost-admin/components/modal-base', 'ember-concurrency'], function (exports, _modalBase, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _modalBase.default.extend({
        // Allowed actions
        confirm: () => {},

        subscriber: Ember.computed.alias('model'),

        actions: {
            confirm() {
                this.get('deleteSubscriber').perform();
            }
        },

        deleteSubscriber: (0, _emberConcurrency.task)(function* () {
            yield this.confirm();
        }).drop()
    });
});