define('ghost-admin/components/gh-user-active', ['exports', 'moment'], function (exports, _moment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        tagName: '',

        user: null,

        lastLoginUTC: Ember.computed('user.lastLoginUTC', function () {
            let lastLoginUTC = this.get('user.lastLoginUTC');

            return lastLoginUTC ? (0, _moment.default)(lastLoginUTC).fromNow() : '(Never)';
        })
    });
});