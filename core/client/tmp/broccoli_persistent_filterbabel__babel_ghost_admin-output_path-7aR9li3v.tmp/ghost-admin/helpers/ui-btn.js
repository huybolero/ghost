define('ghost-admin/helpers/ui-btn', ['exports', 'ember-cli-ghost-spirit/helpers/ui-btn'], function (exports, _uiBtn) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _uiBtn.default;
    }
  });
  Object.defineProperty(exports, 'uiBtn', {
    enumerable: true,
    get: function () {
      return _uiBtn.uiBtn;
    }
  });
});