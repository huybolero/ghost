define('ghost-admin/helpers/ui-text', ['exports', 'ember-cli-ghost-spirit/helpers/ui-text'], function (exports, _uiText) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _uiText.default;
    }
  });
  Object.defineProperty(exports, 'uiText', {
    enumerable: true,
    get: function () {
      return _uiText.uiText;
    }
  });
});