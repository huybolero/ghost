define('ghost-admin/helpers/is-not', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.isNot = isNot;
    function isNot(params) {
        return !params;
    }

    exports.default = Ember.Helper.helper(function (params) {
        return isNot(params);
    });
});