define('ghost-admin/helpers/background-image-style', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.backgroundImageStyle = backgroundImageStyle;
    function backgroundImageStyle([url] /*, hash*/) {
        if (url) {
            let safeUrl = encodeURI(decodeURI(url));
            return Ember.String.htmlSafe(`background-image: url(${safeUrl});`);
        }

        return '';
    }

    exports.default = Ember.Helper.helper(backgroundImageStyle);
});