define('ghost-admin/helpers/gh-format-html', ['exports', 'ghost-admin/utils/caja-sanitizers'], function (exports, _cajaSanitizers) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Helper.helper(function (params) {
        if (!params || !params.length) {
            return;
        }

        let escapedhtml = params[0] || '';

        // replace script and iFrame
        escapedhtml = escapedhtml.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '<pre class="js-embed-placeholder">Embedded JavaScript</pre>');
        escapedhtml = escapedhtml.replace(/<iframe\b[^<]*(?:(?!<\/iframe>)<[^<]*)*<\/iframe>/gi, '<pre class="iframe-embed-placeholder">Embedded iFrame</pre>');

        // sanitize HTML
        /* eslint-disable camelcase */
        escapedhtml = html_sanitize(escapedhtml, _cajaSanitizers.default.url, _cajaSanitizers.default.id);
        /* eslint-enable camelcase */

        return Ember.String.htmlSafe(escapedhtml);
    });
});