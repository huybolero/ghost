define('ghost-admin/helpers/ui-btn-span', ['exports', 'ember-cli-ghost-spirit/helpers/ui-btn-span'], function (exports, _uiBtnSpan) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _uiBtnSpan.default;
    }
  });
  Object.defineProperty(exports, 'uiBtnSpan', {
    enumerable: true,
    get: function () {
      return _uiBtnSpan.uiBtnSpan;
    }
  });
});