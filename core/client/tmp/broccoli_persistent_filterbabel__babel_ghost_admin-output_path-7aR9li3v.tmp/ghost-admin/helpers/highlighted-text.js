define('ghost-admin/helpers/highlighted-text', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.highlightedText = highlightedText;
    function highlightedText([text, termToHighlight]) {
        // replace any non-word character with an escaped character
        let sanitisedTerm = termToHighlight.replace(new RegExp(/\W/ig), '\\$&');

        return Ember.String.htmlSafe(text.replace(new RegExp(sanitisedTerm, 'ig'), '<span class="highlight">$&</span>'));
    }

    exports.default = Ember.Helper.helper(highlightedText);
});