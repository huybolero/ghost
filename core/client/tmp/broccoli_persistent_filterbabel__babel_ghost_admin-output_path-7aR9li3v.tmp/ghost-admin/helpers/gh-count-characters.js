define('ghost-admin/helpers/gh-count-characters', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.countCharacters = countCharacters;
    function countCharacters(params) {
        if (!params || !params.length) {
            return;
        }

        let el = document.createElement('span');
        let content = params[0] || '';

        // convert to array so that we get accurate symbol counts for multibyte chars
        // this will still count emoji+modifer as two chars

        var _Array$from = Array.from(content);

        let length = _Array$from.length;


        el.className = 'word-count';

        if (length > 180) {
            el.style.color = '#f05230';
        } else {
            el.style.color = '#738a94';
        }

        el.innerHTML = 200 - length;

        return Ember.String.htmlSafe(el.outerHTML);
    }

    exports.default = Ember.Helper.helper(function (params) {
        return countCharacters(params);
    });
});