define('ghost-admin/helpers/kg-style', ['exports', 'ember-cli-ghost-spirit/helpers/kg-style'], function (exports, _kgStyle) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _kgStyle.default;
    }
  });
  Object.defineProperty(exports, 'kgStyle', {
    enumerable: true,
    get: function () {
      return _kgStyle.kgStyle;
    }
  });
});