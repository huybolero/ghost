define('ghost-admin/mirage/factories/invite', ['exports', 'moment', 'ember-cli-mirage'], function (exports, _moment, _emberCliMirage) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _emberCliMirage.Factory.extend({
        token(i) {
            return `${i}-token`;
        },
        email(i) {
            return `invited-user-${i}@example.com`;
        },
        expires() {
            return _moment.default.utc().add(1, 'day').valueOf();
        },
        createdAt() {
            return _moment.default.utc().format();
        },
        createdBy() {
            return 1;
        },
        updatedAt() {
            return _moment.default.utc().format();
        },
        updatedBy() {
            return 1;
        },
        status() {
            return 'sent';
        }
    });
});