define('ghost-admin/mirage/factories/subscriber', ['exports', 'moment', 'ember-cli-mirage'], function (exports, _moment, _emberCliMirage) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    let randomDate = function randomDate(start = (0, _moment.default)().subtract(30, 'days').toDate(), end = new Date()) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    };

    let statuses = ['pending', 'subscribed'];

    exports.default = _emberCliMirage.Factory.extend({
        name() {
            return `${_emberCliMirage.faker.name.firstName()} ${_emberCliMirage.faker.name.lastName()}`;
        },
        email: _emberCliMirage.faker.internet.email,
        status() {
            return statuses[Math.floor(Math.random() * statuses.length)];
        },
        createdAt() {
            return randomDate();
        },
        updatedAt: null,
        createdBy: 0,
        updatedBy: null,
        unsubscribedAt: null
    });
});