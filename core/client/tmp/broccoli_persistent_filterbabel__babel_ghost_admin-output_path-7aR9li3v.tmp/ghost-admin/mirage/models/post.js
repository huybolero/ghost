define('ghost-admin/mirage/models/post', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _emberCliMirage.Model.extend({
        tags: (0, _emberCliMirage.hasMany)(),
        authors: (0, _emberCliMirage.hasMany)('user')
    });
});