define('ghost-admin/mirage/config/posts', ['exports', 'moment', 'ember-cli-mirage', 'ghost-admin/mirage/utils'], function (exports, _moment, _emberCliMirage, _utils) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = mockPosts;
    function mockPosts(server) {
        server.post('/posts', function ({ posts, users }) {
            let attrs = this.normalizedRequestAttrs();
            let authors = [];

            // NOTE: this is necessary so that ember-cli-mirage has a valid user
            // schema object rather than a plain object
            // TODO: should ember-cli-mirage be handling this automatically?
            attrs.authors.forEach(author => {
                authors.push(users.find(author.id));
            });

            attrs.authors = authors;

            if (Ember.isBlank(attrs.slug) && !Ember.isBlank(attrs.title)) {
                attrs.slug = Ember.String.dasherize(attrs.title);
            }

            return posts.create(attrs);
        });

        // TODO: handle author filter
        server.get('/posts/', function ({ posts }, { queryParams }) {
            let page = +queryParams.page || 1;
            let limit = +queryParams.limit || 15;
            let status = queryParams.status,
                staticPages = queryParams.staticPages;

            let query = {};

            if (status && status !== 'all') {
                query.status = status;
            }

            if (staticPages === 'false') {
                query.page = false;
            }

            if (staticPages === 'true') {
                query.page = true;
            }

            let collection = posts.where(query);

            return (0, _utils.paginateModelCollection)('posts', collection, page, limit);
        });

        server.get('/posts/:id/', function ({ posts }, { params }) {
            let id = params.id;

            let post = posts.find(id);

            return post || new _emberCliMirage.Response(404, {}, {
                errors: [{
                    errorType: 'NotFoundError',
                    message: 'Post not found.'
                }]
            });
        });

        server.put('/posts/:id/', function ({ posts, users }, { params }) {
            let attrs = this.normalizedRequestAttrs();
            let post = posts.find(params.id);
            let authors = [];

            // NOTE: this is necessary so that ember-cli-mirage has a valid user
            // schema object rather than a plain object
            // TODO: should ember-cli-mirage be handling this automatically?
            attrs.authors.forEach(author => {
                authors.push(users.find(author.id));
            });

            attrs.authors = authors;

            attrs.updatedAt = _moment.default.utc().toDate();

            return post.update(attrs);
        });

        server.del('/posts/:id/');
    }
});