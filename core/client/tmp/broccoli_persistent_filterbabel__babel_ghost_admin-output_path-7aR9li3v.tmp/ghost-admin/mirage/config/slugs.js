define('ghost-admin/mirage/config/slugs', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = mockSlugs;
    function mockSlugs(server) {
        server.get('/slugs/post/:slug/', function (schema, request) {
            return {
                slugs: [{ slug: Ember.String.dasherize(decodeURIComponent(request.params.slug)) }]
            };
        });

        server.get('/slugs/user/:slug/', function (schema, request) {
            return {
                slugs: [{ slug: Ember.String.dasherize(decodeURIComponent(request.params.slug)) }]
            };
        });
    }
});