define('ghost-admin/mirage/config/roles', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = mockRoles;
    function mockRoles(server) {
        server.get('/roles/', function ({ roles }, { queryParams }) {
            if (queryParams.permissions === 'assign') {
                return roles.find([1, 2, 3]);
            }

            return roles.all();
        });
    }
});