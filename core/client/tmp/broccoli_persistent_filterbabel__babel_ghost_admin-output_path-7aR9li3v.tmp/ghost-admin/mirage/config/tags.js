define('ghost-admin/mirage/config/tags', ['exports', 'ghost-admin/mirage/utils'], function (exports, _utils) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = mockTags;
    function mockTags(server) {
        server.post('/tags/', function ({ tags }) {
            let attrs = this.normalizedRequestAttrs();

            if (Ember.isBlank(attrs.slug) && !Ember.isBlank(attrs.name)) {
                attrs.slug = Ember.String.dasherize(attrs.name);
            }

            // NOTE: this does not use the tag factory to fill in blank fields
            return tags.create(attrs);
        });

        server.get('/tags/', (0, _utils.paginatedResponse)('tags'));

        server.get('/tags/slug/:slug/', function ({ tags }, { params: { slug } }) {
            // TODO: remove post_count unless requested?
            return tags.findBy({ slug });
        });

        server.put('/tags/:id/');

        server.del('/tags/:id/');
    }
});