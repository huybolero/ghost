define('ghost-admin/mirage/serializers/post', ['exports', 'ghost-admin/mirage/serializers/application'], function (exports, _application) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend({
        embed: true,

        include(request) {
            let includes = [];

            if (request.queryParams.include && request.queryParams.include.indexOf('tags') >= 0) {
                includes.push('tags');
            }

            if (request.queryParams.include && request.queryParams.include.indexOf('authors') >= 0) {
                includes.push('authors');
            }

            return includes;
        }
    });
});