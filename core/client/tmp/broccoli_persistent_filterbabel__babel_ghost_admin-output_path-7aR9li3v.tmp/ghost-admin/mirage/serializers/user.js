define('ghost-admin/mirage/serializers/user', ['exports', 'ghost-admin/mirage/serializers/application', 'ember-cli-mirage'], function (exports, _application, _emberCliMirage) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend({
        embed: true,

        include(request) {
            if (request.queryParams.include && request.queryParams.include.indexOf('roles') >= 0) {
                return ['roles'];
            }

            return [];
        },

        serialize(object, request) {
            if (this.isCollection(object)) {
                return _application.default.prototype.serialize.call(this, object, request);
            }

            var _RestSerializer$proto = _emberCliMirage.RestSerializer.prototype.serialize.call(this, object, request);

            let user = _RestSerializer$proto.user;


            if (object.postCount) {
                let posts = object.posts.models.length;

                user.count = { posts };
            }

            return { users: [user] };
        }
    });
});