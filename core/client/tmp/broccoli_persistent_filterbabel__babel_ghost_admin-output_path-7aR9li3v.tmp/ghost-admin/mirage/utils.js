define('ghost-admin/mirage/utils', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.paginatedResponse = paginatedResponse;
    exports.paginateModelCollection = paginateModelCollection;
    exports.maintenanceResponse = maintenanceResponse;
    exports.versionMismatchResponse = versionMismatchResponse;
    function paginatedResponse(modelName) {
        return function (schema, request) {
            let page = +request.queryParams.page || 1;
            let limit = +request.queryParams.limit || 15;
            let collection = schema[modelName].all();

            return paginateModelCollection(modelName, collection, page, limit);
        };
    } /* eslint-disable max-statements-per-line */
    function paginateModelCollection(modelName, collection, page, limit) {
        let pages, next, prev, models;

        if (limit === 'all') {
            pages = 1;
        } else {
            limit = +limit;

            let start = (page - 1) * limit;
            let end = start + limit;

            pages = Math.ceil(collection.models.length / limit);
            models = collection.models.slice(start, end);

            if (start > 0) {
                prev = page - 1;
            }

            if (end < collection.models.length) {
                next = page + 1;
            }
        }

        collection.meta = {
            pagination: {
                page,
                limit,
                pages,
                total: collection.models.length,
                next: next || null,
                prev: prev || null
            }
        };

        if (models) {
            collection.models = models;
        }

        return collection;
    }

    function maintenanceResponse() {
        return new _emberCliMirage.Response(503, {}, {
            errors: [{
                errorType: 'Maintenance'
            }]
        });
    }

    function versionMismatchResponse() {
        return new _emberCliMirage.Response(400, {}, {
            errors: [{
                errorType: 'VersionMismatchError'
            }]
        });
    }
});