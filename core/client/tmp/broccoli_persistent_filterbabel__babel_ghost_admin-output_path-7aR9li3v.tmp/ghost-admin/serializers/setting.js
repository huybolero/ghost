define('ghost-admin/serializers/setting', ['exports', 'ghost-admin/serializers/application', 'ember-inflector'], function (exports, _application, _emberInflector) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend({
        serializeIntoHash(hash, type, record, options) {
            // Settings API does not want ids
            options = options || {};
            options.includeId = false;

            let root = (0, _emberInflector.pluralize)(type.modelName);
            let data = this.serialize(record, options);
            let payload = [];

            delete data.id;

            Object.keys(data).forEach(k => {
                payload.push({ key: k, value: data[k] });
            });

            hash[root] = payload;
        },

        normalizeArrayResponse(store, primaryModelClass, _payload, id, requestType) {
            let payload = { settings: [this._extractObjectFromArrayPayload(_payload)] };
            return this._super(store, primaryModelClass, payload, id, requestType);
        },

        normalizeSingleResponse(store, primaryModelClass, _payload, id, requestType) {
            let payload = { setting: this._extractObjectFromArrayPayload(_payload) };
            return this._super(store, primaryModelClass, payload, id, requestType);
        },

        _extractObjectFromArrayPayload(_payload) {
            let payload = { id: '0' };

            _payload.settings.forEach(setting => {
                payload[setting.key] = setting.value;
            });

            return payload;
        }
    });
});