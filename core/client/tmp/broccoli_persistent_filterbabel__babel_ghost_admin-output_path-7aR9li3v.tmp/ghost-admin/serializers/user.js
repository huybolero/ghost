define('ghost-admin/serializers/user', ['exports', 'ghost-admin/serializers/application', 'ember-data/serializers/embedded-records-mixin', 'ember-inflector'], function (exports, _application, _embeddedRecordsMixin, _emberInflector) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend(_embeddedRecordsMixin.default, {
        attrs: {
            roles: { embedded: 'always' },
            lastLoginUTC: { key: 'last_seen' },
            createdAtUTC: { key: 'created_at' },
            updatedAtUTC: { key: 'updated_at' }
        },

        extractSingle(store, primaryType, payload) {
            let root = this.keyForAttribute(primaryType.modelName);
            let pluralizedRoot = (0, _emberInflector.pluralize)(primaryType.modelName);

            payload[root] = payload[pluralizedRoot][0];
            delete payload[pluralizedRoot];

            return this._super(...arguments);
        },

        normalizeSingleResponse(store, primaryModelClass, payload) {
            let root = this.keyForAttribute(primaryModelClass.modelName);
            let pluralizedRoot = (0, _emberInflector.pluralize)(primaryModelClass.modelName);

            if (payload[pluralizedRoot]) {
                payload[root] = payload[pluralizedRoot][0];
                delete payload[pluralizedRoot];
            }

            return this._super(...arguments);
        }
    });
});