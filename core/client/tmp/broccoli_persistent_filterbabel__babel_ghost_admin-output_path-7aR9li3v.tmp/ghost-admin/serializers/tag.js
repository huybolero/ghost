define('ghost-admin/serializers/tag', ['exports', 'ghost-admin/serializers/application', 'ember-inflector'], function (exports, _application, _emberInflector) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _application.default.extend({
        attrs: {
            createdAtUTC: { key: 'created_at' },
            updatedAtUTC: { key: 'updated_at' }
        },

        serialize() /*snapshot, options*/{
            let json = this._super(...arguments);

            // Properties that exist on the model but we don't want sent in the payload
            delete json.count;

            return json;
        },

        // if we use `queryRecord` ensure we grab the first record to avoid
        // DS.SERIALIZER.REST.QUERYRECORD-ARRAY-RESPONSE deprecations
        normalizeResponse(store, primaryModelClass, payload, id, requestType) {
            if (requestType === 'queryRecord') {
                let singular = primaryModelClass.modelName;
                let plural = (0, _emberInflector.pluralize)(singular);

                if (payload[plural]) {
                    payload[singular] = payload[plural][0];
                    delete payload[plural];
                }
            }
            return this._super(...arguments);
        }
    });
});