define('ghost-admin/serializers/application', ['exports', 'ember-data/serializers/rest', 'ember-inflector'], function (exports, _rest, _emberInflector) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _rest.default.extend({
        serialize() /*snapshot, options*/{
            let json = this._super(...arguments);

            // don't send attributes that are updated automatically on the server
            delete json.created_by;
            delete json.updated_by;

            return json;
        },

        serializeIntoHash(hash, type, record, options) {
            // Our API expects an id on the posted object
            options = options || {};
            options.includeId = true;

            // We have a plural root in the API
            let root = (0, _emberInflector.pluralize)(type.modelName);
            let data = this.serialize(record, options);

            hash[root] = [data];
        },

        keyForAttribute(attr) {
            return Ember.String.decamelize(attr);
        }
    });
});