define('ghost-admin/services/config', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    const _ProxyMixin = Ember._ProxyMixin;
    exports.default = Ember.Service.extend(_ProxyMixin, {
        ajax: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),

        content: null,

        init() {
            this._super(...arguments);
            this.content = {};
        },

        fetch() {
            let configUrl = this.get('ghostPaths.url').api('configuration');

            return this.get('ajax').request(configUrl).then(publicConfig => {
                // normalize blogUrl to non-trailing-slash
                var _publicConfig$configu = _slicedToArray(publicConfig.configuration, 1);

                let blogUrl = _publicConfig$configu[0].blogUrl;

                publicConfig.configuration[0].blogUrl = blogUrl.replace(/\/$/, '');

                this.set('content', publicConfig.configuration[0]);
            });
        },

        fetchPrivate() {
            let privateConfigUrl = this.get('ghostPaths.url').api('configuration', 'private');

            return this.get('ajax').request(privateConfigUrl).then(privateConfig => {
                Ember.assign(this.get('content'), privateConfig.configuration[0]);
            });
        },

        availableTimezones: Ember.computed(function () {
            let timezonesUrl = this.get('ghostPaths.url').api('configuration', 'timezones');

            return this.get('ajax').request(timezonesUrl).then(configTimezones => {
                var _configTimezones$conf = _slicedToArray(configTimezones.configuration, 1);

                let timezonesObj = _configTimezones$conf[0];


                timezonesObj = timezonesObj.timezones;

                return timezonesObj;
            });
        }),

        blogDomain: Ember.computed('blogUrl', function () {
            let blogUrl = this.get('blogUrl');
            let blogDomain = blogUrl.replace(/^https?:\/\//, '').replace(/\/?$/, '');

            return blogDomain;
        })
    });
});