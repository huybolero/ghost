define('ghost-admin/services/ghost-paths', ['exports', 'ghost-admin/utils/ghost-paths'], function (exports, _ghostPaths) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Service.extend((0, _ghostPaths.default)());
});