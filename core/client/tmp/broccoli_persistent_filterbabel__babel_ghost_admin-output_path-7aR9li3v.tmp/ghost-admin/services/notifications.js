define('ghost-admin/services/notifications', ['exports', 'ghost-admin/services/ajax'], function (exports, _ajax) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend({
        delayedNotifications: Ember.A(),
        content: Ember.A(),

        upgradeStatus: Ember.inject.service(),

        alerts: Ember.computed.filter('content', function (notification) {
            let status = Ember.get(notification, 'status');
            return status === 'alert';
        }),

        notifications: Ember.computed.filter('content', function (notification) {
            let status = Ember.get(notification, 'status');
            return status === 'notification';
        }),

        handleNotification(message, delayed) {
            // If this is an alert message from the server, treat it as html safe
            if (typeof message.toJSON === 'function' && message.get('status') === 'alert') {
                message.set('message', message.get('message').htmlSafe());
            }

            if (!Ember.get(message, 'status')) {
                Ember.set(message, 'status', 'notification');
            }

            // close existing duplicate alerts/notifications to avoid stacking
            if (Ember.get(message, 'key')) {
                this._removeItems(Ember.get(message, 'status'), Ember.get(message, 'key'));
            }

            if (!delayed) {
                this.get('content').pushObject(message);
            } else {
                this.get('delayedNotifications').pushObject(message);
            }
        },

        showAlert(message, options) {
            options = options || {};

            this.handleNotification({
                message,
                status: 'alert',
                type: options.type,
                key: options.key
            }, options.delayed);
        },

        showNotification(message, options) {
            options = options || {};

            if (!options.doNotCloseNotifications) {
                this.closeNotifications();
            } else {
                // TODO: this should be removed along with showErrors
                options.key = undefined;
            }

            this.handleNotification({
                message,
                status: 'notification',
                type: options.type,
                key: options.key
            }, options.delayed);
        },

        showAPIError(resp, options) {
            // handle "global" errors
            if ((0, _ajax.isVersionMismatchError)(resp)) {
                return this.get('upgradeStatus').requireUpgrade();
            } else if ((0, _ajax.isMaintenanceError)(resp)) {
                return this.get('upgradeStatus').maintenanceAlert();
            }

            // loop over ember-ajax errors object
            if (resp && resp.payload && Ember.isArray(resp.payload.errors)) {
                return resp.payload.errors.forEach(error => {
                    this._showAPIError(error, options);
                });
            }

            this._showAPIError(resp, options);
        },

        _showAPIError(resp, options) {
            options = options || {};
            options.type = options.type || 'error';

            // if possible use the title to get a unique key
            // - we only show one alert for each key so if we get multiple errors
            //   only the last one will be shown
            if (!options.key && !Ember.isBlank(Ember.get(resp, 'title'))) {
                options.key = Ember.String.dasherize(Ember.get(resp, 'title'));
            }
            options.key = ['api-error', options.key].compact().join('.');

            let msg = options.defaultErrorText || 'There was a problem on the server, please try again.';

            if (resp instanceof String) {
                msg = resp;
            } else if (!Ember.isBlank(Ember.get(resp, 'detail'))) {
                msg = resp.detail;
            } else if (!Ember.isBlank(Ember.get(resp, 'message'))) {
                msg = resp.message;
            }

            this.showAlert(msg, options);
        },

        displayDelayed() {
            this.delayedNotifications.forEach(message => {
                this.get('content').pushObject(message);
            });
            this.delayedNotifications = [];
        },

        closeNotification(notification) {
            let content = this.get('content');

            if (typeof notification.toJSON === 'function') {
                notification.deleteRecord();
                notification.save().finally(() => {
                    content.removeObject(notification);
                });
            } else {
                content.removeObject(notification);
            }
        },

        closeNotifications(key) {
            this._removeItems('notification', key);
        },

        closeAlerts(key) {
            this._removeItems('alert', key);
        },

        clearAll() {
            this.get('content').clear();
        },

        _removeItems(status, key) {
            if (key) {
                let keyBase = this._getKeyBase(key);
                // TODO: keys should only have . special char but we should
                // probably use a better regexp escaping function/polyfill
                let escapedKeyBase = keyBase.replace('.', '\\.');
                let keyRegex = new RegExp(`^${escapedKeyBase}`);

                this.set('content', this.get('content').reject(item => {
                    let itemKey = Ember.get(item, 'key');
                    let itemStatus = Ember.get(item, 'status');

                    return itemStatus === status && itemKey && itemKey.match(keyRegex);
                }));
            } else {
                this.set('content', this.get('content').rejectBy('status', status));
            }
        },

        // take a key and return the first two elements, eg:
        // "invite.revoke.failed" => "invite.revoke"
        _getKeyBase(key) {
            return key.split('.').slice(0, 2).join('.');
        }
    });
});