define('ghost-admin/services/ajax', ['exports', 'ember-ajax/services/ajax', 'ghost-admin/config/environment', 'ember-ajax/errors'], function (exports, _ajax, _environment, _errors) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.VersionMismatchError = VersionMismatchError;
    exports.isVersionMismatchError = isVersionMismatchError;
    exports.ServerUnreachableError = ServerUnreachableError;
    exports.isServerUnreachableError = isServerUnreachableError;
    exports.RequestEntityTooLargeError = RequestEntityTooLargeError;
    exports.isRequestEntityTooLargeError = isRequestEntityTooLargeError;
    exports.UnsupportedMediaTypeError = UnsupportedMediaTypeError;
    exports.isUnsupportedMediaTypeError = isUnsupportedMediaTypeError;
    exports.MaintenanceError = MaintenanceError;
    exports.isMaintenanceError = isMaintenanceError;
    exports.ThemeValidationError = ThemeValidationError;
    exports.isThemeValidationError = isThemeValidationError;


    const JSON_CONTENT_TYPE = 'application/json';
    const GHOST_REQUEST = /\/ghost\/api\//;
    const TOKEN_REQUEST = /authentication\/(?:token|ghost|revoke)/;

    function isJSONContentType(header) {
        if (!header || Ember.isNone(header)) {
            return false;
        }
        return header.indexOf(JSON_CONTENT_TYPE) === 0;
    }

    /* Version mismatch error */

    function VersionMismatchError(payload) {
        _errors.AjaxError.call(this, payload, 'API server is running a newer version of Ghost, please upgrade.');
    }

    VersionMismatchError.prototype = Object.create(_errors.AjaxError.prototype);

    function isVersionMismatchError(errorOrStatus, payload) {
        if ((0, _errors.isAjaxError)(errorOrStatus)) {
            return errorOrStatus instanceof VersionMismatchError;
        } else {
            return Ember.get(payload || {}, 'errors.firstObject.errorType') === 'VersionMismatchError';
        }
    }

    /* Request entity too large error */

    function ServerUnreachableError(payload) {
        _errors.AjaxError.call(this, payload, 'Server was unreachable');
    }

    ServerUnreachableError.prototype = Object.create(_errors.AjaxError.prototype);

    function isServerUnreachableError(error) {
        if ((0, _errors.isAjaxError)(error)) {
            return error instanceof ServerUnreachableError;
        } else {
            return error === 0 || error === '0';
        }
    }

    function RequestEntityTooLargeError(payload) {
        _errors.AjaxError.call(this, payload, 'Request is larger than the maximum file size the server allows');
    }

    RequestEntityTooLargeError.prototype = Object.create(_errors.AjaxError.prototype);

    function isRequestEntityTooLargeError(errorOrStatus) {
        if ((0, _errors.isAjaxError)(errorOrStatus)) {
            return errorOrStatus instanceof RequestEntityTooLargeError;
        } else {
            return errorOrStatus === 413;
        }
    }

    /* Unsupported media type error */

    function UnsupportedMediaTypeError(payload) {
        _errors.AjaxError.call(this, payload, 'Request contains an unknown or unsupported file type.');
    }

    UnsupportedMediaTypeError.prototype = Object.create(_errors.AjaxError.prototype);

    function isUnsupportedMediaTypeError(errorOrStatus) {
        if ((0, _errors.isAjaxError)(errorOrStatus)) {
            return errorOrStatus instanceof UnsupportedMediaTypeError;
        } else {
            return errorOrStatus === 415;
        }
    }

    /* Maintenance error */

    function MaintenanceError(payload) {
        _errors.AjaxError.call(this, payload, 'Ghost is currently undergoing maintenance, please wait a moment then retry.');
    }

    MaintenanceError.prototype = Object.create(_errors.AjaxError.prototype);

    function isMaintenanceError(errorOrStatus) {
        if ((0, _errors.isAjaxError)(errorOrStatus)) {
            return errorOrStatus instanceof MaintenanceError;
        } else {
            return errorOrStatus === 503;
        }
    }

    /* Theme validation error */

    function ThemeValidationError(payload) {
        _errors.AjaxError.call(this, payload, 'Theme is not compatible or contains errors.');
    }

    ThemeValidationError.prototype = Object.create(_errors.AjaxError.prototype);

    function isThemeValidationError(errorOrStatus, payload) {
        if ((0, _errors.isAjaxError)(errorOrStatus)) {
            return errorOrStatus instanceof ThemeValidationError;
        } else {
            return Ember.get(payload || {}, 'errors.firstObject.errorType') === 'ThemeValidationError';
        }
    }

    /* end: custom error types */

    let ajaxService = _ajax.default.extend({
        session: Ember.inject.service(),

        headers: Ember.computed('session.isAuthenticated', function () {
            let session = this.get('session');
            let headers = {};

            headers['X-Ghost-Version'] = _environment.default.APP.version;
            headers['App-Pragma'] = 'no-cache';

            if (session.get('isAuthenticated')) {
                var _session$get = session.get('data.authenticated');

                let access_token = _session$get.access_token;

                headers.Authorization = `Bearer ${access_token}`;
                /* eslint-enable camelcase */
            }

            return headers;
        }).volatile(),

        // ember-ajax recognises `application/vnd.api+json` as a JSON-API request
        // and formats appropriately, we want to handle `application/json` the same
        _makeRequest(hash) {
            let isAuthenticated = this.get('session.isAuthenticated');
            let isGhostRequest = GHOST_REQUEST.test(hash.url);
            let isTokenRequest = isGhostRequest && TOKEN_REQUEST.test(hash.url);
            let tokenExpiry = this.get('session.authenticated.expires_at');
            let isTokenExpired = tokenExpiry < new Date().getTime();

            if (isJSONContentType(hash.contentType) && hash.type !== 'GET') {
                if (typeof hash.data === 'object') {
                    hash.data = JSON.stringify(hash.data);
                }
            }

            // we can get into a situation where the app is left open without a
            // network connection and the token subsequently expires, this will
            // result in the next network request returning a 401 and killing the
            // session. This is an attempt to detect that and restore the session
            // using the stored refresh token before continuing with the request
            //
            // TODO:
            // - this might be quite blunt, if we have a lot of requests at once
            //   we probably want to queue the requests until the restore completes
            // BUG:
            // - the original caller gets a rejected promise with `undefined` instead
            //   of the AjaxError object when session restore fails. This isn't a
            //   huge deal because the session will be invalidated and app reloaded
            //   but it would be nice to be consistent
            if (isAuthenticated && isGhostRequest && !isTokenRequest && isTokenExpired) {
                return this.get('session').restore().then(() => this._makeRequest(hash));
            }

            return this._super(...arguments);
        },

        handleResponse(status, headers, payload, request) {
            if (this.isVersionMismatchError(status, headers, payload)) {
                return new VersionMismatchError(payload);
            } else if (this.isServerUnreachableError(status, headers, payload)) {
                return new ServerUnreachableError(payload);
            } else if (this.isRequestEntityTooLargeError(status, headers, payload)) {
                return new RequestEntityTooLargeError(payload);
            } else if (this.isUnsupportedMediaTypeError(status, headers, payload)) {
                return new UnsupportedMediaTypeError(payload);
            } else if (this.isMaintenanceError(status, headers, payload)) {
                return new MaintenanceError(payload);
            } else if (this.isThemeValidationError(status, headers, payload)) {
                return new ThemeValidationError(payload);
            }

            let isGhostRequest = GHOST_REQUEST.test(request.url);
            let isAuthenticated = this.get('session.isAuthenticated');
            let isUnauthorized = this.isUnauthorizedError(status, headers, payload);

            if (isAuthenticated && isGhostRequest && isUnauthorized) {
                this.get('session').invalidate();
            }

            return this._super(...arguments);
        },

        normalizeErrorResponse(status, headers, payload) {
            if (payload && typeof payload === 'object') {
                let errors = payload.error || payload.errors || payload.message || undefined;

                if (errors) {
                    if (!Ember.isArray(errors)) {
                        errors = [errors];
                    }

                    payload.errors = errors.map(function (error) {
                        if (typeof error === 'string') {
                            return { message: error };
                        } else {
                            return error;
                        }
                    });
                }
            }

            return this._super(status, headers, payload);
        },

        isVersionMismatchError(status, headers, payload) {
            return isVersionMismatchError(status, payload);
        },

        isServerUnreachableError(status) {
            return isServerUnreachableError(status);
        },

        isRequestEntityTooLargeError(status) {
            return isRequestEntityTooLargeError(status);
        },

        isUnsupportedMediaTypeError(status) {
            return isUnsupportedMediaTypeError(status);
        },

        isMaintenanceError(status, headers, payload) {
            return isMaintenanceError(status, payload);
        },

        isThemeValidationError(status, headers, payload) {
            return isThemeValidationError(status, payload);
        }
    });

    // we need to reopen so that internal methods use the correct contentType
    ajaxService.reopen({
        contentType: 'application/json; charset=UTF-8'
    });

    exports.default = ajaxService;
});