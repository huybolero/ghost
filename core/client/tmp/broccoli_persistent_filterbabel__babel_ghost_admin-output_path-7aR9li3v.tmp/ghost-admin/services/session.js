define('ghost-admin/services/session', ['exports', 'ember-simple-auth/services/session'], function (exports, _session) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _session.default.extend({
        feature: Ember.inject.service(),
        store: Ember.inject.service(),
        tour: Ember.inject.service(),

        user: Ember.computed(function () {
            return this.get('store').queryRecord('user', { id: 'me' });
        }),

        authenticate() {
            return this._super(...arguments).then(authResult => {
                // TODO: remove duplication with application.afterModel
                let preloadPromises = [this.get('feature').fetch(), this.get('tour').fetchViewed()];

                return Ember.RSVP.all(preloadPromises).then(() => authResult);
            });
        }
    });
});