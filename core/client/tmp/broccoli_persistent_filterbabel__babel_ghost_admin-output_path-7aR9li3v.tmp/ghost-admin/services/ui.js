define('ghost-admin/services/ui', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend({
        dropdown: Ember.inject.service(),
        mediaQueries: Ember.inject.service(),

        autoNav: false,
        isFullScreen: false,
        showMobileMenu: false,
        showSettingsMenu: false,

        hasSideNav: Ember.computed.not('isSideNavHidden'),
        isMobile: Ember.computed.reads('mediaQueries.isMobile'),
        isSideNavHidden: Ember.computed.or('autoNav', 'isFullScreen', 'isMobile'),

        autoNavOpen: Ember.computed('autoNav', {
            get() {
                return false;
            },
            set(key, value) {
                if (this.get('autoNav')) {
                    return value;
                }
                return false;
            }
        }),

        closeMenus() {
            this.get('dropdown').closeDropdowns();
            this.setProperties({
                showSettingsMenu: false,
                showMobileMenu: false
            });
        },

        openAutoNav() {
            this.set('autoNavOpen', true);
        },

        closeAutoNav() {
            if (this.get('autoNavOpen')) {
                this.get('dropdown').closeDropdowns();
            }
            this.set('autoNavOpen', false);
        },

        closeMobileMenu() {
            this.set('showMobileMenu', false);
        },

        openMobileMenu() {
            this.set('showMobileMenu', true);
        },

        openSettingsMenu() {
            this.set('showSettingsMenu', true);
        },

        toggleAutoNav() {
            this.toggleProperty('autoNav');
        },

        actions: {
            closeMenus() {
                this.closeMenus();
            },

            openAutoNav() {
                this.openAutoNav();
            },

            closeAutoNav() {
                this.closeAutoNav();
            },

            closeMobileMenu() {
                this.closeMobileMenu();
            },

            openMobileMenu() {
                this.openMobileMenu();
            },

            openSettingsMenu() {
                this.openSettingsMenu();
            },

            toggleAutoNav() {
                this.toggleAutoNav();
            }
        }
    });
});