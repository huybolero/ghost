define('ghost-admin/services/clock', ['exports', 'ghost-admin/config/environment', 'moment'], function (exports, _environment, _moment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    const ONE_SECOND = 1000;

    // Creates a clock service to run intervals.

    exports.default = Ember.Service.extend({
        second: null,
        minute: null,
        hour: null,

        init() {
            this._super(...arguments);
            this.tick();
        },

        tick() {
            let now = (0, _moment.default)().utc();

            this.setProperties({
                second: now.seconds(),
                minute: now.minutes(),
                hour: now.hours()
            });

            if (_environment.default.environment !== 'test') {
                Ember.run.later(() => {
                    this.tick();
                }, ONE_SECOND);
            }
        }

    });
});