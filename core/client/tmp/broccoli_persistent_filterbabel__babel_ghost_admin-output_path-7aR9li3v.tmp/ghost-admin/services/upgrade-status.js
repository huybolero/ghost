define('ghost-admin/services/upgrade-status', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend({
        notifications: Ember.inject.service(),

        isRequired: false,
        message: '',

        // called when notifications are fetched during app boot for notifications
        // where the `location` is not 'top' and `custom` is false
        handleUpgradeNotification(notification) {
            let message = Ember.get(notification, 'message');
            Ember.set(this, 'message', Ember.String.htmlSafe(message));
        },

        // called when a MaintenanceError is encountered
        maintenanceAlert() {
            Ember.get(this, 'notifications').showAlert('Sorry, Ghost is currently undergoing maintenance, please wait a moment then try again.', { type: 'error', key: 'api-error.under-maintenance' });
        },

        // called when a VersionMismatchError is encountered
        requireUpgrade() {
            Ember.set(this, 'isRequired', true);
            Ember.get(this, 'notifications').showAlert('Ghost has been upgraded, please copy any unsaved data and refresh the page to continue.', { type: 'error', key: 'api-error.upgrade-required' });
        }
    });
});