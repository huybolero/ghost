define('ghost-admin/services/resize-detector', ['exports', 'npm:element-resize-detector'], function (exports, _npmElementResizeDetector) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend({
        init() {
            this._super(...arguments);
            this.detector = (0, _npmElementResizeDetector.default)({
                strategy: 'scroll'
            });
        },

        setup(selector, callback) {
            let element = document.querySelector(selector);
            if (!element) {
                // eslint-disable-next-line
                console.error(`service:resize-detector - could not find element matching ${selector}`);
            }
            this.detector.listenTo(element, callback);
        },

        teardown(selector, callback) {
            let element = document.querySelector(selector);
            if (element) {
                this.detector.removeListener(element, callback);
            }
        }
    });
});