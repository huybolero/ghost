define('ghost-admin/services/infinity', ['exports', 'ember-infinity/services/infinity'], function (exports, _infinity) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _infinity.default;
    }
  });
});