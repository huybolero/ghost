define('ghost-admin/services/event-bus', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend(Ember.Evented, {
        publish() {
            return this.trigger(...arguments);
        },
        subscribe() {
            return this.on(...arguments);
        },
        unsubscribe() {
            return this.off(...arguments);
        }
    });
});