define('ghost-admin/services/dropdown', ['exports', 'ghost-admin/mixins/body-event-listener'], function (exports, _bodyEventListener) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend(Ember.Evented, _bodyEventListener.default, {
        bodyClick(event) {
            let dropdownSelector = '.ember-basic-dropdown-trigger, .ember-basic-dropdown-content';

            if (Ember.$(event.target).closest(dropdownSelector).length <= 0) {
                this.closeDropdowns();
            }
        },

        closeDropdowns() {
            this.trigger('close');
        },

        toggleDropdown(dropdownName, dropdownButton) {
            this.trigger('toggle', { target: dropdownName, button: dropdownButton });
        }
    });
});