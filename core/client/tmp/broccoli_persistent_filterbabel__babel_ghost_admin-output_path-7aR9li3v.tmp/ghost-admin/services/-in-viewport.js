define('ghost-admin/services/-in-viewport', ['exports', 'ember-in-viewport/services/-in-viewport'], function (exports, _inViewport) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _inViewport.default;
    }
  });
});