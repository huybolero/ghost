define('ghost-admin/services/lazy-loader', ['exports', 'ghost-admin/config/environment'], function (exports, _environment) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Service.extend({
        ajax: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),

        // This is needed so we can disable it in unit tests
        testing: undefined,

        scriptPromises: null,

        init() {
            this._super(...arguments);
            this.scriptPromises = {};

            if (this.testing === undefined) {
                this.testing = _environment.default.environment === 'test';
            }
        },

        loadScript(key, url) {
            if (this.get('testing')) {
                return Ember.RSVP.resolve();
            }

            if (this.get(`scriptPromises.${key}`)) {
                // Script is already loaded/in the process of being loaded,
                // so return that promise
                return this.get(`scriptPromises.${key}`);
            }

            let ajax = this.get('ajax');
            let adminRoot = this.get('ghostPaths.adminRoot');

            let scriptPromise = ajax.request(`${adminRoot}${url}`, {
                dataType: 'script',
                cache: true
            });

            this.set(`scriptPromises.${key}`, scriptPromise);

            return scriptPromise;
        },

        loadStyle(key, url, alternate = false) {
            if (this.get('testing') || Ember.$(`#${key}-styles`).length) {
                return Ember.RSVP.resolve();
            }

            return new Ember.RSVP.Promise((resolve, reject) => {
                let link = document.createElement('link');
                link.id = `${key}-styles`;
                link.rel = alternate ? 'alternate stylesheet' : 'stylesheet';
                link.href = `${this.get('ghostPaths.adminRoot')}${url}`;
                link.onload = () => {
                    if (alternate) {
                        // If stylesheet is alternate and we disable the stylesheet before injecting into the DOM,
                        // the onload handler never gets called. Thus, we should disable the link after it has finished loading
                        link.disabled = true;
                    }
                    resolve();
                };
                link.onerror = reject;

                if (alternate) {
                    link.title = key;
                }

                Ember.$('head').append(Ember.$(link));
            });
        }
    });
});