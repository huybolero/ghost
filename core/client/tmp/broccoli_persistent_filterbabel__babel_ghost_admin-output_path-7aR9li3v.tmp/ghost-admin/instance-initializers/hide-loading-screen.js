define('ghost-admin/instance-initializers/hide-loading-screen', ['exports', 'ghost-admin/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;


  const userConfig = _environment.default['ember-load'] || {};

  function initialize() {
    const instance = arguments[1] || arguments[0];
    const container = !!arguments[1] ? arguments[0] : instance.container;

    if (Ember.View) {
      let ApplicationView = container.lookupFactory ? container.lookupFactory('view:application') : instance.resolveRegistration('view:application');

      ApplicationView = ApplicationView.reopen({
        didInsertElement() {
          this._super(...arguments);

          var loadingIndicatorClass = userConfig.loadingIndicatorClass || 'ember-load-indicator';

          Ember.$(`.${loadingIndicatorClass}`).remove();
        }
      });
    }
  }

  exports.default = {
    name: 'hide-loading-screen-instance',
    initialize
  };
});