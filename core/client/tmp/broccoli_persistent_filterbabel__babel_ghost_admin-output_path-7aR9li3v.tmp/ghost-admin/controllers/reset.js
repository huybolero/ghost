define('ghost-admin/controllers/reset', ['exports', 'ghost-admin/mixins/validation-engine', 'ember-concurrency'], function (exports, _validationEngine, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend(_validationEngine.default, {
        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),

        newPassword: '',
        ne2Password: '',
        token: '',
        flowErrors: '',

        validationType: 'reset',

        email: Ember.computed('token', function () {
            // The token base64 encodes the email (and some other stuff),
            // each section is divided by a '|'. Email comes second.
            return atob(this.get('token')).split('|')[1];
        }),

        actions: {
            submit() {
                return this.get('resetPassword').perform();
            }
        },

        // Used to clear sensitive information
        clearData() {
            this.setProperties({
                newPassword: '',
                ne2Password: '',
                token: ''
            });
        },

        resetPassword: (0, _emberConcurrency.task)(function* () {
            let credentials = this.getProperties('newPassword', 'ne2Password', 'token');
            let authUrl = this.get('ghostPaths.url').api('authentication', 'passwordreset');

            this.set('flowErrors', '');
            this.get('hasValidated').addObjects(['newPassword', 'ne2Password']);

            try {
                yield this.validate();
                try {
                    let resp = yield this.get('ajax').put(authUrl, {
                        data: {
                            passwordreset: [credentials]
                        }
                    });
                    this.get('notifications').showAlert(resp.passwordreset[0].message, { type: 'warn', delayed: true, key: 'password.reset' });
                    this.get('session').authenticate('authenticator:oauth2', this.get('email'), credentials.newPassword);
                    return true;
                } catch (error) {
                    this.get('notifications').showAPIError(error, { key: 'password.reset' });
                }
            } catch (error) {
                if (this.get('errors.newPassword')) {
                    this.set('flowErrors', this.get('errors.newPassword')[0].message);
                }

                if (this.get('errors.ne2Password')) {
                    this.set('flowErrors', this.get('errors.ne2Password')[0].message);
                }

                if (error && this.get('errors.length') === 0) {
                    throw error;
                }
            }
        }).drop()
    });
});