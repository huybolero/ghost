define('ghost-admin/controllers/posts', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    const TYPES = [{
        name: 'All posts',
        value: null
    }, {
        name: 'Draft posts',
        value: 'draft'
    }, {
        name: 'Published posts',
        value: 'published'
    }, {
        name: 'Scheduled posts',
        value: 'scheduled'
    }, {
        name: 'Featured posts',
        value: 'featured'
    }, {
        name: 'Pages',
        value: 'page'
    }];

    const ORDERS = [{
        name: 'Newest',
        value: null
    }, {
        name: 'Oldest',
        value: 'published_at asc'
    }];

    exports.default = Ember.Controller.extend({

        session: Ember.inject.service(),
        store: Ember.inject.service(),

        queryParams: ['type', 'author', 'tag', 'order'],

        type: null,
        author: null,
        tag: null,
        order: null,

        _hasLoadedTags: false,
        _hasLoadedAuthors: false,

        availableTypes: null,
        availableOrders: null,

        init() {
            this._super(...arguments);
            this.availableTypes = TYPES;
            this.availableOrders = ORDERS;
        },

        postsInfinityModel: Ember.computed.alias('model'),

        showingAll: Ember.computed('type', 'author', 'tag', function () {
            var _getProperties = this.getProperties(['type', 'author', 'tag']);

            let type = _getProperties.type,
                author = _getProperties.author,
                tag = _getProperties.tag;


            return !type && !author && !tag;
        }),

        selectedType: Ember.computed('type', function () {
            let types = this.get('availableTypes');
            return types.findBy('value', this.get('type'));
        }),

        selectedOrder: Ember.computed('order', function () {
            let orders = this.get('availableOrders');
            return orders.findBy('value', this.get('order'));
        }),

        _availableTags: Ember.computed(function () {
            return this.get('store').peekAll('tag');
        }),

        availableTags: Ember.computed('_availableTags.[]', function () {
            let tags = this.get('_availableTags').filter(tag => tag.get('id') !== null);
            let options = tags.toArray();

            options.unshiftObject({ name: 'All tags', slug: null });

            return options;
        }),

        selectedTag: Ember.computed('tag', '_availableTags.[]', function () {
            let tag = this.get('tag');
            let tags = this.get('availableTags');

            return tags.findBy('slug', tag);
        }),

        _availableAuthors: Ember.computed(function () {
            return this.get('store').peekAll('user');
        }),

        availableAuthors: Ember.computed('_availableAuthors.[]', function () {
            let authors = this.get('_availableAuthors');
            let options = authors.toArray();

            options.unshiftObject({ name: 'All authors', slug: null });

            return options;
        }),

        selectedAuthor: Ember.computed('author', 'availableAuthors.[]', function () {
            let author = this.get('author');
            let authors = this.get('availableAuthors');

            return authors.findBy('slug', author);
        }),

        actions: {
            changeType(type) {
                this.set('type', Ember.get(type, 'value'));
            },

            changeAuthor(author) {
                this.set('author', Ember.get(author, 'slug'));
            },

            changeTag(tag) {
                this.set('tag', Ember.get(tag, 'slug'));
            },

            changeOrder(order) {
                this.set('order', Ember.get(order, 'value'));
            },

            openEditor(post) {
                this.transitionToRoute('editor.edit', post.get('id'));
            }
        }
    });
});