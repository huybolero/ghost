define('ghost-admin/controllers/team/index', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        session: Ember.inject.service(),

        showInviteUserModal: false,

        activeUsers: null,
        suspendedUsers: null,
        invites: null,

        inviteOrder: null,
        userOrder: null,

        init() {
            this._super(...arguments);
            this.inviteOrder = ['email'];
            this.userOrder = ['name', 'email'];
        },

        sortedInvites: Ember.computed.sort('filteredInvites', 'inviteOrder'),
        sortedActiveUsers: Ember.computed.sort('activeUsers', 'userOrder'),
        sortedSuspendedUsers: Ember.computed.sort('suspendedUsers', 'userOrder'),

        filteredInvites: Ember.computed('invites.@each.isNew', function () {
            return this.get('invites').filterBy('isNew', false);
        }),

        actions: {
            toggleInviteUserModal() {
                this.toggleProperty('showInviteUserModal');
            }
        }
    });
});