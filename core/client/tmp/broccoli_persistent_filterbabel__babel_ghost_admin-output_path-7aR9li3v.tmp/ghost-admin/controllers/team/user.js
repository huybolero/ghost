define('ghost-admin/controllers/team/user', ['exports', 'ghost-admin/utils/bound-one-way', 'ghost-admin/utils/isNumber', 'npm:validator', 'ghost-admin/utils/window-proxy', 'ember-concurrency'], function (exports, _boundOneWay, _isNumber, _npmValidator, _windowProxy, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    exports.default = Ember.Controller.extend({
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),
        dropdown: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),
        slugGenerator: Ember.inject.service(),

        leaveSettingsTransition: null,
        dirtyAttributes: false,
        showDeleteUserModal: false,
        showSuspendUserModal: false,
        showTransferOwnerModal: false,
        showUploadCoverModal: false,
        showUplaodImageModal: false,
        _scratchFacebook: null,
        _scratchTwitter: null,

        saveHandlers: (0, _emberConcurrency.taskGroup)().enqueue(),

        user: Ember.computed.alias('model'),
        currentUser: Ember.computed.alias('session.user'),

        email: Ember.computed.readOnly('user.email'),
        slugValue: (0, _boundOneWay.default)('user.slug'),

        canAssignRoles: Ember.computed.or('currentUser.isAdmin', 'currentUser.isOwner'),
        canChangeEmail: Ember.computed.not('isAdminUserOnOwnerProfile'),
        canChangePassword: Ember.computed.not('isAdminUserOnOwnerProfile'),
        canMakeOwner: Ember.computed.and('currentUser.isOwner', 'isNotOwnProfile', 'user.isAdmin'),
        isAdminUserOnOwnerProfile: Ember.computed.and('currentUser.isAdmin', 'user.isOwner'),
        isNotOwnersProfile: Ember.computed.not('user.isOwner'),
        rolesDropdownIsVisible: Ember.computed.and('isNotOwnProfile', 'canAssignRoles', 'isNotOwnersProfile'),
        userActionsAreVisible: Ember.computed.or('deleteUserActionIsVisible', 'canMakeOwner'),

        isNotOwnProfile: Ember.computed.not('isOwnProfile'),
        isOwnProfile: Ember.computed('user.id', 'currentUser.id', function () {
            return this.get('user.id') === this.get('currentUser.id');
        }),

        deleteUserActionIsVisible: Ember.computed('currentUser', 'canAssignRoles', 'user', function () {
            if (this.get('canAssignRoles') && this.get('isNotOwnProfile') && !this.get('user.isOwner') || this.get('currentUser.isEditor') && (this.get('isNotOwnProfile') || this.get('user.isAuthorOrContributor'))) {
                return true;
            }
        }),

        coverTitle: Ember.computed('user.name', function () {
            return `${this.get('user.name')}'s Cover Image`;
        }),

        roles: Ember.computed(function () {
            return this.store.query('role', { permissions: 'assign' });
        }),

        actions: {
            changeRole(newRole) {
                this.get('user').set('role', newRole);
                this.set('dirtyAttributes', true);
            },

            deleteUser() {
                return this._deleteUser().then(() => {
                    this._deleteUserSuccess();
                }, () => {
                    this._deleteUserFailure();
                });
            },

            toggleDeleteUserModal() {
                if (this.get('deleteUserActionIsVisible')) {
                    this.toggleProperty('showDeleteUserModal');
                }
            },

            suspendUser() {
                this.get('user').set('status', 'inactive');
                return this.get('save').perform();
            },

            toggleSuspendUserModal() {
                if (this.get('deleteUserActionIsVisible')) {
                    this.toggleProperty('showSuspendUserModal');
                }
            },

            unsuspendUser() {
                this.get('user').set('status', 'active');
                return this.get('save').perform();
            },

            toggleUnsuspendUserModal() {
                if (this.get('deleteUserActionIsVisible')) {
                    this.toggleProperty('showUnsuspendUserModal');
                }
            },

            validateFacebookUrl() {
                let newUrl = this.get('_scratchFacebook');
                let oldUrl = this.get('user.facebook');
                let errMessage = '';

                // reset errors and validation
                this.get('user.errors').remove('facebook');
                this.get('user.hasValidated').removeObject('facebook');

                if (newUrl === '') {
                    // Clear out the Facebook url
                    this.set('user.facebook', '');
                    return;
                }

                // _scratchFacebook will be null unless the user has input something
                if (!newUrl) {
                    newUrl = oldUrl;
                }

                try {
                    // strip any facebook URLs out
                    newUrl = newUrl.replace(/(https?:\/\/)?(www\.)?facebook\.com/i, '');

                    // don't allow any non-facebook urls
                    if (newUrl.match(/^(http|\/\/)/i)) {
                        throw 'invalid url';
                    }

                    // strip leading / if we have one then concat to full facebook URL
                    newUrl = newUrl.replace(/^\//, '');
                    newUrl = `https://www.facebook.com/${newUrl}`;

                    // don't allow URL if it's not valid
                    if (!_npmValidator.default.isURL(newUrl)) {
                        throw 'invalid url';
                    }

                    this.set('user.facebook', '');
                    Ember.run.schedule('afterRender', this, function () {
                        this.set('user.facebook', newUrl);
                    });
                } catch (e) {
                    if (e === 'invalid url') {
                        errMessage = 'The URL must be in a format like ' + 'https://www.facebook.com/yourPage';
                        this.get('user.errors').add('facebook', errMessage);
                        return;
                    }

                    throw e;
                } finally {
                    this.get('user.hasValidated').pushObject('facebook');
                }
            },

            validateTwitterUrl() {
                let newUrl = this.get('_scratchTwitter');
                let oldUrl = this.get('user.twitter');
                let errMessage = '';

                // reset errors and validation
                this.get('user.errors').remove('twitter');
                this.get('user.hasValidated').removeObject('twitter');

                if (newUrl === '') {
                    // Clear out the Twitter url
                    this.set('user.twitter', '');
                    return;
                }

                // _scratchTwitter will be null unless the user has input something
                if (!newUrl) {
                    newUrl = oldUrl;
                }

                if (newUrl.match(/(?:twitter\.com\/)(\S+)/) || newUrl.match(/([a-z\d.]+)/i)) {
                    let username = [];

                    if (newUrl.match(/(?:twitter\.com\/)(\S+)/)) {
                        var _newUrl$match = newUrl.match(/(?:twitter\.com\/)(\S+)/);

                        var _newUrl$match2 = _slicedToArray(_newUrl$match, 2);

                        username = _newUrl$match2[1];
                    } else {
                        var _newUrl$match3 = newUrl.match(/([^/]+)\/?$/mi);

                        var _newUrl$match4 = _slicedToArray(_newUrl$match3, 1);

                        username = _newUrl$match4[0];
                    }

                    // check if username starts with http or www and show error if so
                    if (username.match(/^(http|www)|(\/)/) || !username.match(/^[a-z\d._]{1,15}$/mi)) {
                        errMessage = !username.match(/^[a-z\d._]{1,15}$/mi) ? 'Your Username is not a valid Twitter Username' : 'The URL must be in a format like https://twitter.com/yourUsername';

                        this.get('user.errors').add('twitter', errMessage);
                        this.get('user.hasValidated').pushObject('twitter');
                        return;
                    }

                    newUrl = `https://twitter.com/${username}`;

                    this.get('user.hasValidated').pushObject('twitter');

                    this.set('user.twitter', '');
                    Ember.run.schedule('afterRender', this, function () {
                        this.set('user.twitter', newUrl);
                    });
                } else {
                    errMessage = 'The URL must be in a format like ' + 'https://twitter.com/yourUsername';
                    this.get('user.errors').add('twitter', errMessage);
                    this.get('user.hasValidated').pushObject('twitter');
                    return;
                }
            },

            transferOwnership() {
                let user = this.get('user');
                let url = this.get('ghostPaths.url').api('users', 'owner');

                this.get('dropdown').closeDropdowns();

                return this.get('ajax').put(url, {
                    data: {
                        owner: [{
                            id: user.get('id')
                        }]
                    }
                }).then(response => {
                    // manually update the roles for the users that just changed roles
                    // because store.pushPayload is not working with embedded relations
                    if (response && Ember.isArray(response.users)) {
                        response.users.forEach(userJSON => {
                            let user = this.store.peekRecord('user', userJSON.id);
                            let role = this.store.peekRecord('role', userJSON.roles[0].id);

                            user.set('role', role);
                        });
                    }

                    this.get('notifications').showAlert(`Ownership successfully transferred to ${user.get('name')}`, { type: 'success', key: 'owner.transfer.success' });
                }).catch(error => {
                    this.get('notifications').showAPIError(error, { key: 'owner.transfer' });
                });
            },

            toggleLeaveSettingsModal(transition) {
                let leaveTransition = this.get('leaveSettingsTransition');

                if (!transition && this.get('showLeaveSettingsModal')) {
                    this.set('leaveSettingsTransition', null);
                    this.set('showLeaveSettingsModal', false);
                    return;
                }

                if (!leaveTransition || transition.targetName === leaveTransition.targetName) {
                    this.set('leaveSettingsTransition', transition);

                    // if a save is running, wait for it to finish then transition
                    if (this.get('saveHandlers.isRunning')) {
                        return this.get('saveHandlers.last').then(() => {
                            transition.retry();
                        });
                    }

                    // we genuinely have unsaved data, show the modal
                    this.set('showLeaveSettingsModal', true);
                }
            },

            leaveSettings() {
                let transition = this.get('leaveSettingsTransition');
                let user = this.get('user');

                if (!transition) {
                    this.get('notifications').showAlert('Sorry, there was an error in the application. Please let the Ghost team know what happened.', { type: 'error' });
                    return;
                }

                // roll back changes on user props
                user.rollbackAttributes();
                // roll back the slugValue property
                if (this.get('dirtyAttributes')) {
                    this.set('slugValue', user.get('slug'));
                    this.set('dirtyAttributes', false);
                }

                return transition.retry();
            },

            toggleTransferOwnerModal() {
                if (this.get('canMakeOwner')) {
                    this.toggleProperty('showTransferOwnerModal');
                }
            },

            toggleUploadCoverModal() {
                this.toggleProperty('showUploadCoverModal');
            },

            toggleUploadImageModal() {
                this.toggleProperty('showUploadImageModal');
            },

            // TODO: remove those mutation actions once we have better
            // inline validations that auto-clear errors on input
            updatePassword(password) {
                this.set('user.password', password);
                this.get('user.hasValidated').removeObject('password');
                this.get('user.errors').remove('password');
            },

            updateNewPassword(password) {
                this.set('user.newPassword', password);
                this.get('user.hasValidated').removeObject('newPassword');
                this.get('user.errors').remove('newPassword');
            },

            updateNe2Password(password) {
                this.set('user.ne2Password', password);
                this.get('user.hasValidated').removeObject('ne2Password');
                this.get('user.errors').remove('ne2Password');
            }
        },

        _deleteUser() {
            if (this.get('deleteUserActionIsVisible')) {
                let user = this.get('user');
                return user.destroyRecord();
            }
        },

        _deleteUserSuccess() {
            this.get('notifications').closeAlerts('user.delete');
            this.store.unloadAll('post');
            this.transitionToRoute('team');
        },

        _deleteUserFailure() {
            this.get('notifications').showAlert('The user could not be deleted. Please try again.', { type: 'error', key: 'user.delete.failed' });
        },

        updateSlug: (0, _emberConcurrency.task)(function* (newSlug) {
            let slug = this.get('user.slug');

            newSlug = newSlug || slug;
            newSlug = newSlug.trim();

            // Ignore unchanged slugs or candidate slugs that are empty
            if (!newSlug || slug === newSlug) {
                this.set('slugValue', slug);

                return true;
            }

            let serverSlug = yield this.get('slugGenerator').generateSlug('user', newSlug);

            // If after getting the sanitized and unique slug back from the API
            // we end up with a slug that matches the existing slug, abort the change
            if (serverSlug === slug) {
                return true;
            }

            // Because the server transforms the candidate slug by stripping
            // certain characters and appending a number onto the end of slugs
            // to enforce uniqueness, there are cases where we can get back a
            // candidate slug that is a duplicate of the original except for
            // the trailing incrementor (e.g., this-is-a-slug and this-is-a-slug-2)

            // get the last token out of the slug candidate and see if it's a number
            let slugTokens = serverSlug.split('-');
            let check = Number(slugTokens.pop());

            // if the candidate slug is the same as the existing slug except
            // for the incrementor then the existing slug should be used
            if ((0, _isNumber.default)(check) && check > 0) {
                if (slug === slugTokens.join('-') && serverSlug !== newSlug) {
                    this.set('slugValue', slug);

                    return true;
                }
            }

            this.set('slugValue', serverSlug);
            this.set('dirtyAttributes', true);

            return true;
        }).group('saveHandlers'),

        save: (0, _emberConcurrency.task)(function* () {
            let user = this.get('user');
            let slugValue = this.get('slugValue');
            let slugChanged;

            if (user.get('slug') !== slugValue) {
                slugChanged = true;
                user.set('slug', slugValue);
            }

            try {
                user = yield user.save({ format: false });

                // If the user's slug has changed, change the URL and replace
                // the history so refresh and back button still work
                if (slugChanged) {
                    let currentPath = window.location.hash;

                    let newPath = currentPath.split('/');
                    newPath[newPath.length - 1] = user.get('slug');
                    newPath = newPath.join('/');

                    _windowProxy.default.replaceState({ path: newPath }, '', newPath);
                }

                this.set('dirtyAttributes', false);
                this.get('notifications').closeAlerts('user.update');

                return user;
            } catch (error) {
                // validation engine returns undefined so we have to check
                // before treating the failure as an API error
                if (error) {
                    this.get('notifications').showAPIError(error, { key: 'user.update' });
                }
            }
        }).group('saveHandlers')
    });
});