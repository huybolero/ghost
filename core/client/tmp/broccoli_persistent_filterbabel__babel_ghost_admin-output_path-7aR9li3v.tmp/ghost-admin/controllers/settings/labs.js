define('ghost-admin/controllers/settings/labs', ['exports', 'ghost-admin/config/environment', 'ghost-admin/services/ajax', 'ember-concurrency'], function (exports, _environment, _ajax, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    const Promise = Ember.RSVP.Promise;


    const IMPORT_MIME_TYPES = ['application/json', 'application/zip', 'application/x-zip-compressed'];

    const JSON_EXTENSION = ['json'];
    const JSON_MIME_TYPE = ['application/json'];

    exports.default = Ember.Controller.extend({
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),
        feature: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),
        settings: Ember.inject.service(),

        importErrors: null,
        importSuccessful: false,
        showDeleteAllModal: false,
        submitting: false,
        uploadButtonText: 'Import',

        importMimeType: null,
        jsonExtension: null,
        jsonMimeType: null,

        init() {
            this._super(...arguments);
            this.importMimeType = IMPORT_MIME_TYPES;
            this.jsonExtension = JSON_EXTENSION;
            this.jsonMimeType = JSON_MIME_TYPE;
        },

        actions: {
            onUpload(file) {
                let formData = new FormData();
                let notifications = this.get('notifications');
                let currentUserId = this.get('session.user.id');
                let dbUrl = this.get('ghostPaths.url').api('db');

                this.set('uploadButtonText', 'Importing');
                this.set('importErrors', null);
                this.set('importSuccessful', false);

                return this._validate(file).then(() => {
                    formData.append('importfile', file);

                    return this.get('ajax').post(dbUrl, {
                        data: formData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }).then(response => {
                    let store = this.get('store');

                    this.set('importSuccessful', true);

                    if (response.problems) {
                        this.set('importErrors', response.problems);
                    }

                    // Clear the store, so that all the new data gets fetched correctly.
                    store.unloadAll();

                    // NOTE: workaround for behaviour change in Ember 2.13
                    // store.unloadAll has some async tendencies so we need to schedule
                    // the reload of the current user once the unload has finished
                    // https://github.com/emberjs/data/issues/4963
                    Ember.run.schedule('destroy', this, () => {
                        // Reload currentUser and set session
                        this.set('session.user', store.findRecord('user', currentUserId));

                        // TODO: keep as notification, add link to view content
                        notifications.showNotification('Import successful.', { key: 'import.upload.success' });

                        // reload settings
                        return this.get('settings').reload().then(settings => {
                            this.get('feature').fetch();
                            this.get('config').set('blogTitle', settings.get('title'));
                        });
                    });
                }).catch(response => {
                    if ((0, _ajax.isUnsupportedMediaTypeError)(response) || (0, _ajax.isRequestEntityTooLargeError)(response)) {
                        this.set('importErrors', [response]);
                    } else if (response && response.payload.errors && Ember.isArray(response.payload.errors)) {
                        this.set('importErrors', response.payload.errors);
                    } else {
                        this.set('importErrors', [{ message: 'Import failed due to an unknown error. Check the Web Inspector console and network tabs for errors.' }]);
                    }

                    throw response;
                }).finally(() => {
                    this.set('uploadButtonText', 'Import');
                });
            },

            downloadFile(url) {
                let dbUrl = this.get('ghostPaths.url').api(url);
                let accessToken = this.get('session.data.authenticated.access_token');
                let downloadURL = `${dbUrl}?access_token=${accessToken}`;
                let iframe = Ember.$('#iframeDownload');

                if (iframe.length === 0) {
                    iframe = Ember.$('<iframe>', { id: 'iframeDownload' }).hide().appendTo('body');
                }

                iframe.attr('src', downloadURL);
            },

            toggleDeleteAllModal() {
                this.toggleProperty('showDeleteAllModal');
            },

            /**
             * Opens a file selection dialog - Triggered by "Upload x" buttons,
             * searches for the hidden file input within the .gh-setting element
             * containing the clicked button then simulates a click
             * @param  {MouseEvent} event - MouseEvent fired by the button click
             */
            triggerFileDialog(event) {
                // simulate click to open file dialog
                // using jQuery because IE11 doesn't support MouseEvent
                Ember.$(event.target).closest('.gh-setting-action').find('input[type="file"]').click();
            }
        },

        // TODO: convert to ember-concurrency task
        _validate(file) {
            // Windows doesn't have mime-types for json files by default, so we
            // need to have some additional checking
            if (file.type === '') {
                // First check file extension so we can early return
                var _$exec = /(?:\.([^.]+))?$/.exec(file.name),
                    _$exec2 = _slicedToArray(_$exec, 2);

                let extension = _$exec2[1];


                if (!extension || extension.toLowerCase() !== 'json') {
                    return Ember.RSVP.reject(new _ajax.UnsupportedMediaTypeError());
                }

                return new Promise((resolve, reject) => {
                    // Extension is correct, so check the contents of the file
                    let reader = new FileReader();

                    reader.onload = function () {
                        let result = reader.result;


                        try {
                            JSON.parse(result);

                            return resolve();
                        } catch (e) {
                            return reject(new _ajax.UnsupportedMediaTypeError());
                        }
                    };

                    reader.readAsText(file);
                });
            }

            let accept = this.get('importMimeType');

            if (!Ember.isBlank(accept) && file && accept.indexOf(file.type) === -1) {
                return Ember.RSVP.reject(new _ajax.UnsupportedMediaTypeError());
            }

            return Ember.RSVP.resolve();
        },

        sendTestEmail: (0, _emberConcurrency.task)(function* () {
            let notifications = this.get('notifications');
            let emailUrl = this.get('ghostPaths.url').api('mail', 'test');

            try {
                yield this.get('ajax').post(emailUrl);
                notifications.showAlert('Check your email for the test message.', { type: 'info', key: 'test-email.send.success' });
                return true;
            } catch (error) {
                notifications.showAPIError(error, { key: 'test-email:send' });
            }
        }).drop(),

        redirectUploadResult: (0, _emberConcurrency.task)(function* (success) {
            this.set('redirectSuccess', success);
            this.set('redirectFailure', !success);

            yield (0, _emberConcurrency.timeout)(_environment.default.environment === 'test' ? 100 : 5000);

            this.set('redirectSuccess', null);
            this.set('redirectFailure', null);
            return true;
        }).drop(),

        reset() {
            this.set('importErrors', null);
            this.set('importSuccessful', false);
        }
    });
});