define('ghost-admin/controllers/settings/tags', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({

        tagController: Ember.inject.controller('settings.tags.tag'),

        tags: Ember.computed.alias('model'),
        selectedTag: Ember.computed.alias('tagController.tag'),

        tagListFocused: Ember.computed.equal('keyboardFocus', 'tagList'),
        tagContentFocused: Ember.computed.equal('keyboardFocus', 'tagContent'),

        filteredTags: Ember.computed('tags.@each.isNew', function () {
            return this.get('tags').filterBy('isNew', false);
        }),

        // TODO: replace with ordering by page count once supported by the API
        sortedTags: Ember.computed.sort('filteredTags', function (a, b) {
            let idA = +a.get('id');
            let idB = +b.get('id');

            if (idA > idB) {
                return 1;
            } else if (idA < idB) {
                return -1;
            }

            return 0;
        }),

        actions: {
            leftMobile() {
                let firstTag = this.get('tags.firstObject');
                // redirect to first tag if possible so that you're not left with
                // tag settings blank slate when switching from portrait to landscape
                if (firstTag && !this.get('tagController.tag')) {
                    this.transitionToRoute('settings.tags.tag', firstTag);
                }
            }
        },

        scrollTagIntoView(tag) {
            Ember.run.scheduleOnce('afterRender', this, function () {
                let id = `#gh-tag-${tag.get('id')}`;
                let element = document.querySelector(id);

                if (element) {
                    let scroll = document.querySelector('.tag-list');
                    let scrollTop = scroll.scrollTop;

                    let scrollHeight = scroll.offsetHeight;
                    let element = document.querySelector(id);
                    let elementTop = element.offsetTop;
                    let elementHeight = element.offsetHeight;

                    if (elementTop < scrollTop) {
                        element.scrollIntoView(true);
                    }

                    if (elementTop + elementHeight > scrollTop + scrollHeight) {
                        element.scrollIntoView(false);
                    }
                }
            });
        }
    });
});