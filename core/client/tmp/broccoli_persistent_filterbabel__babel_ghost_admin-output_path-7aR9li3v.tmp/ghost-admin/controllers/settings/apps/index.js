define('ghost-admin/controllers/settings/apps/index', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        settings: Ember.inject.service()
    });
});