define('ghost-admin/controllers/about', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        upgradeStatus: Ember.inject.service(),

        about: Ember.computed.readOnly('model'),

        copyrightYear: Ember.computed(function () {
            let date = new Date();
            return date.getFullYear();
        })
    });
});