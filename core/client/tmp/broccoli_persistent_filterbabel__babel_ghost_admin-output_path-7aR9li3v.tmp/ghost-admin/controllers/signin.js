define('ghost-admin/controllers/signin', ['exports', 'ghost-admin/mixins/validation-engine', 'ghost-admin/services/ajax', 'ember-concurrency'], function (exports, _validationEngine, _ajax, _emberConcurrency) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    exports.default = Ember.Controller.extend(_validationEngine.default, {
        application: Ember.inject.controller(),
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),
        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),
        settings: Ember.inject.service(),

        submitting: false,
        loggingIn: false,
        authProperties: null,

        flowErrors: '',
        // ValidationEngine settings
        validationType: 'signin',

        init() {
            this._super(...arguments);
            this.authProperties = ['identification', 'password'];
        },

        signin: Ember.computed.alias('model'),

        actions: {
            authenticate() {
                this.get('validateAndAuthenticate').perform();
            }
        },

        authenticate: (0, _emberConcurrency.task)(function* (authStrategy, authentication) {
            try {
                let authResult = yield this.get('session').authenticate(authStrategy, ...authentication);
                let promises = [];

                promises.pushObject(this.get('settings').fetch());
                promises.pushObject(this.get('config').fetchPrivate());

                // fetch settings and private config for synchronous access
                yield Ember.RSVP.all(promises);

                return authResult;
            } catch (error) {
                if ((0, _ajax.isVersionMismatchError)(error)) {
                    return this.get('notifications').showAPIError(error);
                }

                if (error && error.payload && error.payload.errors) {
                    error.payload.errors.forEach(err => {
                        err.message = err.message.htmlSafe();
                    });

                    this.set('flowErrors', error.payload.errors[0].message.string);

                    if (error.payload.errors[0].message.string.match(/user with that email/)) {
                        this.get('signin.errors').add('identification', '');
                    }

                    if (error.payload.errors[0].message.string.match(/password is incorrect/)) {
                        this.get('signin.errors').add('password', '');
                    }
                } else {
                    // Connection errors don't return proper status message, only req.body
                    this.get('notifications').showAlert('There was a problem on the server.', { type: 'error', key: 'session.authenticate.failed' });
                }
            }
        }).drop(),

        validateAndAuthenticate: (0, _emberConcurrency.task)(function* () {
            let signin = this.get('signin');
            let authStrategy = 'authenticator:oauth2';

            this.set('flowErrors', '');
            // Manually trigger events for input fields, ensuring legacy compatibility with
            // browsers and password managers that don't send proper events on autofill
            Ember.$('#login').find('input').trigger('change');

            // This is a bit dirty, but there's no other way to ensure the properties are set as well as 'signin'
            this.get('hasValidated').addObjects(this.authProperties);

            try {
                yield this.validate({ property: 'signin' });
                return yield this.get('authenticate').perform(authStrategy, [signin.get('identification'), signin.get('password')]);
            } catch (error) {
                this.set('flowErrors', 'Please fill out the form to sign in.');
            }
        }).drop(),

        forgotten: (0, _emberConcurrency.task)(function* () {
            let email = this.get('signin.identification');
            let forgottenUrl = this.get('ghostPaths.url').api('authentication', 'passwordreset');
            let notifications = this.get('notifications');

            this.set('flowErrors', '');
            // This is a bit dirty, but there's no other way to ensure the properties are set as well as 'forgotPassword'
            this.get('hasValidated').addObject('identification');

            try {
                yield this.validate({ property: 'forgotPassword' });
                yield this.get('ajax').post(forgottenUrl, { data: { passwordreset: [{ email }] } });
                notifications.showAlert('Please check your email for instructions.', { type: 'info', key: 'forgot-password.send.success' });
                return true;
            } catch (error) {
                // ValidationEngine throws "undefined" for failed validation
                if (!error) {
                    return this.set('flowErrors', 'We need your email address to reset your password!');
                }

                if ((0, _ajax.isVersionMismatchError)(error)) {
                    return notifications.showAPIError(error);
                }

                if (error && error.payload && error.payload.errors && Ember.isArray(error.payload.errors)) {
                    var _error$payload$errors = _slicedToArray(error.payload.errors, 1);

                    let message = _error$payload$errors[0].message;


                    this.set('flowErrors', message);

                    if (message.match(/no user with that email/)) {
                        this.get('signin.errors').add('identification', '');
                    }
                } else {
                    notifications.showAPIError(error, { defaultErrorText: 'There was a problem with the reset, please try again.', key: 'forgot-password.send' });
                }
            }
        })
    });
});