define('ghost-admin/controllers/error', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({

        stack: false,
        error: Ember.computed.readOnly('model'),

        code: Ember.computed('error.status', function () {
            return this.get('error.status') > 200 ? this.get('error.status') : 500;
        }),

        message: Ember.computed('error.statusText', function () {
            if (this.get('code') === 404) {
                return 'Page not found';
            }

            return this.get('error.statusText') !== 'error' ? this.get('error.statusText') : 'Internal Server Error';
        })
    });
});