define('ghost-admin/controllers/posts-loading', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({

        postsController: Ember.inject.controller('posts'),
        session: Ember.inject.service(),

        availableTypes: Ember.computed.readOnly('postsController.availableTypes'),
        selectedType: Ember.computed.readOnly('postsController.selectedType'),
        availableTags: Ember.computed.readOnly('postsController.availableTags'),
        selectedTag: Ember.computed.readOnly('postsController.selectedTag'),
        availableAuthors: Ember.computed.readOnly('postsController.availableAuthors'),
        selectedAuthor: Ember.computed.readOnly('postsController.selectedAuthor'),
        availableOrders: Ember.computed.readOnly('postsController.availableOrders'),
        selectedOrder: Ember.computed.readOnly('postsController.selectedOrder')

    });
});