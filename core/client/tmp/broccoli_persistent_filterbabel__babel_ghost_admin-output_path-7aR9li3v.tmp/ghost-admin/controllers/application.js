define('ghost-admin/controllers/application', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        dropdown: Ember.inject.service(),
        session: Ember.inject.service(),
        settings: Ember.inject.service(),
        ui: Ember.inject.service(),

        showNavMenu: Ember.computed('currentPath', 'session.{isAuthenticated,user.isFulfilled}', function () {
            // we need to defer showing the navigation menu until the session.user
            // promise has fulfilled so that gh-user-can-admin has the correct data
            if (!this.get('session.isAuthenticated') || !this.get('session.user.isFulfilled')) {
                return false;
            }

            return (this.get('currentPath') !== 'error404' || this.get('session.isAuthenticated')) && !this.get('currentPath').match(/(signin|signup|setup|reset)/);
        })
    });
});