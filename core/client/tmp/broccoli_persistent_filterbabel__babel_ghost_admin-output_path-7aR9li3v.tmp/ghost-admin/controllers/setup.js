define('ghost-admin/controllers/setup', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        appController: Ember.inject.controller('application'),
        ghostPaths: Ember.inject.service(),

        showBackLink: Ember.computed.match('appController.currentRouteName', /^setup\.(two|three)$/),

        backRoute: Ember.computed('appController.currentRouteName', function () {
            let currentRoute = this.get('appController.currentRouteName');

            return currentRoute === 'setup.two' ? 'setup.one' : 'setup.two';
        })
    });
});