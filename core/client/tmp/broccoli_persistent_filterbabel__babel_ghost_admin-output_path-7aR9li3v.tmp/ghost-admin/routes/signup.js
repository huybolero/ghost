define('ghost-admin/routes/signup', ['exports', 'ember-data', 'ghost-admin/mixins/unauthenticated-route-mixin', 'ghost-admin/mixins/style-body'], function (exports, _emberData, _unauthenticatedRouteMixin, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    const Promise = Ember.RSVP.Promise;
    const Errors = _emberData.default.Errors;
    exports.default = Ember.Route.extend(_styleBody.default, _unauthenticatedRouteMixin.default, {
        ghostPaths: Ember.inject.service(),
        notifications: Ember.inject.service(),
        session: Ember.inject.service(),
        ajax: Ember.inject.service(),
        config: Ember.inject.service(),

        classNames: ['ghost-signup'],

        beforeModel() {
            if (this.get('session.isAuthenticated')) {
                this.get('notifications').showAlert('You need to sign out to register as a new user.', { type: 'warn', delayed: true, key: 'signup.create.already-authenticated' });
            }

            this._super(...arguments);
        },

        model(params) {
            let signupDetails = Ember.Object.create();
            let re = /^(?:[A-Za-z0-9_-]{4})*(?:[A-Za-z0-9_-]{2}|[A-Za-z0-9_-]{3})?$/;
            let email, tokenText;

            return new Promise(resolve => {
                if (!re.test(params.token)) {
                    this.get('notifications').showAlert('Invalid token.', { type: 'error', delayed: true, key: 'signup.create.invalid-token' });

                    return resolve(this.transitionTo('signin'));
                }

                tokenText = atob(params.token);
                email = tokenText.split('|')[1];

                signupDetails.set('email', email);
                signupDetails.set('token', params.token);
                signupDetails.set('errors', Errors.create());

                let authUrl = this.get('ghostPaths.url').api('authentication', 'invitation');

                return this.get('ajax').request(authUrl, {
                    dataType: 'json',
                    data: {
                        email
                    }
                }).then(response => {
                    if (response && response.invitation && response.invitation[0].valid === false) {
                        this.get('notifications').showAlert('The invitation does not exist or is no longer valid.', { type: 'warn', delayed: true, key: 'signup.create.invalid-invitation' });

                        return resolve(this.transitionTo('signin'));
                    }

                    signupDetails.set('invitedBy', response.invitation[0].invitedBy);

                    // set blogTitle, so password validation has access to it
                    signupDetails.set('blogTitle', this.get('config.blogTitle'));

                    resolve(signupDetails);
                }).catch(() => {
                    resolve(signupDetails);
                });
            });
        },

        deactivate() {
            this._super(...arguments);

            // clear the properties that hold the sensitive data from the controller
            this.controllerFor('signup').get('signupDetails').setProperties({ email: '', password: '', token: '' });
        }
    });
});