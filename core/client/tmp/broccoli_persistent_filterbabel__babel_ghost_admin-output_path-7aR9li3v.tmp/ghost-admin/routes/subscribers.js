define('ghost-admin/routes/subscribers', ['exports', 'ghost-admin/routes/authenticated'], function (exports, _authenticated) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend({
        feature: Ember.inject.service(),

        titleToken: 'Subscribers',

        // redirect if subscribers is disabled or user isn't owner/admin
        beforeModel() {
            this._super(...arguments);
            let promises = {
                user: this.get('session.user'),
                subscribers: this.get('feature.subscribers')
            };

            return Ember.RSVP.hash(promises).then(hash => {
                let user = hash.user,
                    subscribers = hash.subscribers;


                if (!subscribers || !(user.get('isOwner') || user.get('isAdmin'))) {
                    return this.transitionTo('posts');
                }
            });
        },

        setupController(controller) {
            this._super(...arguments);
            controller.initializeTable();
            controller.send('loadFirstPage');
        },

        resetController(controller, isExiting) {
            this._super(...arguments);
            if (isExiting) {
                controller.set('order', 'created_at');
                controller.set('direction', 'desc');
            }
        },

        actions: {
            addSubscriber(subscriber) {
                this.get('controller').send('addSubscriber', subscriber);
            },

            reset() {
                this.get('controller').send('reset');
            }
        }
    });
});