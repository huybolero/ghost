define('ghost-admin/routes/signin', ['exports', 'ember-data', 'ghost-admin/mixins/unauthenticated-route-mixin', 'ghost-admin/mixins/style-body'], function (exports, _emberData, _unauthenticatedRouteMixin, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    const Errors = _emberData.default.Errors;


    const defaultModel = function defaultModel() {
        return Ember.Object.create({
            identification: '',
            password: '',
            errors: Errors.create()
        });
    };

    exports.default = Ember.Route.extend(_unauthenticatedRouteMixin.default, _styleBody.default, {
        titleToken: 'Sign In',

        classNames: ['ghost-login'],

        model() {
            return defaultModel();
        },

        // the deactivate hook is called after a route has been exited.
        deactivate() {
            let controller = this.controllerFor('signin');

            this._super(...arguments);

            // clear the properties that hold the credentials when we're no longer on the signin screen
            controller.set('signin', defaultModel());
        }
    });
});