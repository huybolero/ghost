define('ghost-admin/routes/setup/index', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        beforeModel() {
            this._super(...arguments);
            this.transitionTo('setup.one');
        }
    });
});