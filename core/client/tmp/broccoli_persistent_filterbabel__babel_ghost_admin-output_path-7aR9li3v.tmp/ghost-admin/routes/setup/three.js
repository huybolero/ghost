define('ghost-admin/routes/setup/three', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        beforeModel() {
            this._super(...arguments);
            if (!this.controllerFor('setup.two').get('blogCreated')) {
                this.transitionTo('setup.two');
            }
        }
    });
});