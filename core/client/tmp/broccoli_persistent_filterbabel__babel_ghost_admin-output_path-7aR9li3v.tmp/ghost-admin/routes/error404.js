define('ghost-admin/routes/error404', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        controllerName: 'error',
        templateName: 'error',
        titleToken: 'Error',

        model() {
            return {
                status: 404
            };
        }
    });
});