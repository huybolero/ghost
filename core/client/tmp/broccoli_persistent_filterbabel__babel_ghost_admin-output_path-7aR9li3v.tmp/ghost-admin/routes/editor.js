define('ghost-admin/routes/editor', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/shortcuts-route', 'ghost-admin/utils/ctrl-or-cmd'], function (exports, _authenticated, _shortcutsRoute, _ctrlOrCmd) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    let generalShortcuts = {};
    generalShortcuts[`${_ctrlOrCmd.default}+shift+p`] = 'publish';

    exports.default = _authenticated.default.extend(_shortcutsRoute.default, {
        classNames: ['editor'],
        shortcuts: generalShortcuts,
        titleToken: 'Editor',

        actions: {
            save() {
                this._blurAndScheduleAction(function () {
                    this.get('controller').send('save');
                });
            },

            publish() {
                this._blurAndScheduleAction(function () {
                    this.get('controller').send('setSaveType', 'publish');
                    this.get('controller').send('save');
                });
            },

            authorizationFailed() {
                this.get('controller').send('toggleReAuthenticateModal');
            },

            redirectToContentScreen() {
                this.transitionTo('posts');
            },

            willTransition(transition) {
                // exit early if an upgrade is required because our extended route
                // class will abort the transition and show an error
                if (this.get('upgradeStatus.isRequired')) {
                    return this._super(...arguments);
                }

                this.get('controller').willTransition(transition);
            }
        },

        _blurAndScheduleAction(func) {
            let selectedElement = Ember.$(document.activeElement);

            // TODO: we should trigger a blur for textareas as well as text inputs
            if (selectedElement.is('input[type="text"]')) {
                selectedElement.trigger('focusout');
            }

            // wait for actions triggered by the focusout to finish before saving
            Ember.run.scheduleOnce('actions', this, func);
        }
    });
});