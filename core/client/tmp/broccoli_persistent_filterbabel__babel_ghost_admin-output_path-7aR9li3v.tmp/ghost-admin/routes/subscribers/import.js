define('ghost-admin/routes/subscribers/import', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        actions: {
            cancel() {
                this.transitionTo('subscribers');
            }
        }
    });
});