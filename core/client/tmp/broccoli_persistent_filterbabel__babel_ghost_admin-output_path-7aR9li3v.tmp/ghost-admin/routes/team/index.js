define('ghost-admin/routes/team/index', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/current-user-settings', 'ember-infinity/mixins/route', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _currentUserSettings, _route, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, _currentUserSettings.default, _route.default, {
        titleToken: 'Team',

        classNames: ['view-team'],

        modelPath: 'controller.activeUsers',
        perPage: 15,

        model() {
            return this.get('session.user').then(user => {
                let modelPath = this.get('modelPath');
                let perPage = this.get('perPage');

                let modelPromises = {
                    activeUsers: this.infinityModel('user', {
                        modelPath,
                        perPage,
                        filter: 'status:-inactive',
                        startingPage: 1,
                        perPageParam: 'limit',
                        totalPagesParam: 'meta.pagination.pages'
                    })
                };

                // authors do not have permission to hit the invites or suspended users endpoint
                if (!user.get('isAuthorOrContributor')) {
                    modelPromises.invites = this.store.query('invite', { limit: 'all' }).then(() => this.store.peekAll('invite'));

                    // fetch suspended users separately so that infinite scroll still works
                    modelPromises.suspendedUsers = this.store.query('user', { limit: 'all', filter: 'status:inactive' });
                }

                // we need to load the roles into ember cache
                // invites return role_id only and we do not offer a /role/:id endpoint
                modelPromises.roles = this.get('store').query('role', {});

                return Ember.RSVP.hash(modelPromises);
            });
        },

        setupController(controller, models) {
            controller.setProperties(models);
        },

        actions: {
            reload() {
                this.refresh();
            }
        }
    });
});