define('ghost-admin/routes/editor/new', ['exports', 'ghost-admin/routes/authenticated'], function (exports, _authenticated) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend({
        model() {
            return this.get('session.user').then(user => this.store.createRecord('post', { authors: [user] }));
        },

        // there's no specific controller for this route, instead all editor
        // handling is done on the editor route/controler
        setupController(controller, newPost) {
            let editor = this.controllerFor('editor');
            editor.setPost(newPost);
        }
    });
});