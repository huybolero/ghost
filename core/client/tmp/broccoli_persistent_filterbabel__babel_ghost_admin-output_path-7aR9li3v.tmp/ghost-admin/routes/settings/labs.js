define('ghost-admin/routes/settings/labs', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/current-user-settings', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _currentUserSettings, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, _currentUserSettings.default, {
        settings: Ember.inject.service(),

        titleToken: 'Settings - Labs',
        classNames: ['settings'],

        beforeModel() {
            this._super(...arguments);
            return this.get('session.user').then(this.transitionAuthor()).then(this.transitionEditor());
        },

        model() {
            return this.get('settings').reload();
        },

        resetController(controller, isExiting) {
            if (isExiting) {
                controller.reset();
            }
        }
    });
});