define('ghost-admin/routes/settings/apps/slack', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, {
        settings: Ember.inject.service(),

        titleToken: 'Slack',

        classNames: ['settings-view-apps-slack'],

        afterModel() {
            return this.get('settings').reload();
        },

        actions: {
            save() {
                this.get('controller').send('save');
            },

            willTransition(transition) {
                let controller = this.get('controller');
                let settings = this.get('settings');
                let modelIsDirty = settings.get('hasDirtyAttributes');

                if (modelIsDirty) {
                    transition.abort();
                    controller.send('toggleLeaveSettingsModal', transition);
                    return;
                }
            }
        }
    });
});