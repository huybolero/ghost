define('ghost-admin/routes/settings/apps/unsplash', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/models/unsplash-integration', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _unsplashIntegration, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, {
        config: Ember.inject.service(),
        settings: Ember.inject.service(),

        titleToken: 'Unsplash',
        classNames: ['settings-view-apps-unsplash'],

        beforeModel() {
            let settings = this.get('settings');

            if (settings.get('unsplash')) {
                return;
            }

            // server doesn't have any unsplash settings by default but it can provide
            // overrides via config:
            // - isActive: use as default but allow settings override
            // - applicationId: total override, no field is shown if present
            let unsplash = _unsplashIntegration.default.create({
                isActive: true
            });

            settings.set('unsplash', unsplash);

            return unsplash;
        },

        actions: {
            save() {
                this.get('controller').send('save');
            },

            willTransition(transition) {
                let controller = this.get('controller');
                let modelIsDirty = controller.get('dirtyAttributes');

                if (modelIsDirty) {
                    transition.abort();
                    controller.send('toggleLeaveSettingsModal', transition);
                    return;
                }
            }
        }
    });
});