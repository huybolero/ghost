define('ghost-admin/routes/settings/general', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/current-user-settings', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _currentUserSettings, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, _currentUserSettings.default, {
        config: Ember.inject.service(),
        settings: Ember.inject.service(),

        titleToken: 'Settings - General',
        classNames: ['settings-view-general'],

        beforeModel() {
            this._super(...arguments);
            return this.get('session.user').then(this.transitionAuthor()).then(this.transitionEditor());
        },

        model() {
            return Ember.RSVP.hash({
                settings: this.get('settings').reload(),
                availableTimezones: this.get('config.availableTimezones')
            });
        },

        setupController(controller, models) {
            // reset the leave setting transition
            controller.set('leaveSettingsTransition', null);
            controller.set('availableTimezones', models.availableTimezones);
        },

        actions: {
            save() {
                return this.get('controller').send('save');
            },

            reloadSettings() {
                return this.get('settings').reload();
            },

            willTransition(transition) {
                let controller = this.get('controller');
                let settings = this.get('settings');
                let settingsIsDirty = settings.get('hasDirtyAttributes');

                if (settingsIsDirty) {
                    transition.abort();
                    controller.send('toggleLeaveSettingsModal', transition);
                    return;
                }
            }

        }
    });
});