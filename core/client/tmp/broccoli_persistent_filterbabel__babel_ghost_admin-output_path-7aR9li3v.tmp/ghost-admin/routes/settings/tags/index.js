define('ghost-admin/routes/settings/tags/index', ['exports', 'ghost-admin/routes/authenticated'], function (exports, _authenticated) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend({
        mediaQueries: Ember.inject.service(),

        beforeModel() {
            let firstTag = this.modelFor('settings.tags').get('firstObject');

            this._super(...arguments);

            if (firstTag && !this.get('mediaQueries.maxWidth600')) {
                this.transitionTo('settings.tags.tag', firstTag);
            }
        }
    });
});