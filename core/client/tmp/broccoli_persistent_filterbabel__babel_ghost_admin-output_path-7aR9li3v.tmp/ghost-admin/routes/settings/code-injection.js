define('ghost-admin/routes/settings/code-injection', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/current-user-settings', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _currentUserSettings, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _authenticated.default.extend(_styleBody.default, _currentUserSettings.default, {
        settings: Ember.inject.service(),

        titleToken: 'Settings - Code injection',
        classNames: ['settings-view-code'],

        beforeModel() {
            this._super(...arguments);
            return this.get('session.user').then(this.transitionAuthor()).then(this.transitionEditor());
        },

        model() {
            return this.get('settings').reload();
        },

        actions: {
            save() {
                this.get('controller').send('save');
            },

            willTransition(transition) {
                let controller = this.get('controller');
                let settings = this.get('settings');
                let modelIsDirty = settings.get('hasDirtyAttributes');

                if (modelIsDirty) {
                    transition.abort();
                    controller.send('toggleLeaveSettingsModal', transition);
                    return;
                }
            }
        }
    });
});