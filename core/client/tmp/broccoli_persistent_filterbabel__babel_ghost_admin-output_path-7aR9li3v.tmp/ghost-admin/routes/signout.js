define('ghost-admin/routes/signout', ['exports', 'ghost-admin/routes/authenticated', 'ghost-admin/mixins/style-body'], function (exports, _authenticated, _styleBody) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    const canInvoke = Ember.canInvoke;
    exports.default = _authenticated.default.extend(_styleBody.default, {
        notifications: Ember.inject.service(),

        titleToken: 'Sign Out',

        classNames: ['ghost-signout'],

        afterModel(model, transition) {
            this.get('notifications').clearAll();
            if (canInvoke(transition, 'send')) {
                transition.send('logout');
            } else {
                this.send('logout');
            }
        }
    });
});