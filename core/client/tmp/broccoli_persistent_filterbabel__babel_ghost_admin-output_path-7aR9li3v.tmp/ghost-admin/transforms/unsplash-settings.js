define('ghost-admin/transforms/unsplash-settings', ['exports', 'ember-data/transform', 'ghost-admin/models/unsplash-integration'], function (exports, _transform, _unsplashIntegration) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    /* eslint-disable camelcase */
    const DEFAULT_SETTINGS = {
        isActive: true
    };

    exports.default = _transform.default.extend({
        deserialize(serialized) {
            if (serialized) {
                let settingsObject;
                try {
                    settingsObject = JSON.parse(serialized) || DEFAULT_SETTINGS;
                } catch (e) {
                    settingsObject = DEFAULT_SETTINGS;
                }

                return _unsplashIntegration.default.create(settingsObject);
            }

            return DEFAULT_SETTINGS;
        },

        serialize(deserialized) {
            return deserialized ? JSON.stringify(deserialized) : JSON.stringify(DEFAULT_SETTINGS);
        }
    });
});