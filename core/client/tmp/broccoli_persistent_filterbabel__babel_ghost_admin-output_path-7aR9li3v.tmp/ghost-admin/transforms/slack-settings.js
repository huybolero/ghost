define('ghost-admin/transforms/slack-settings', ['exports', 'ghost-admin/models/slack-integration', 'ember-data/transform'], function (exports, _slackIntegration, _transform) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _transform.default.extend({
        deserialize(serialized) {
            let slackObj, settingsArray;
            try {
                settingsArray = JSON.parse(serialized) || [];
            } catch (e) {
                settingsArray = [];
            }

            slackObj = settingsArray.map(itemDetails => _slackIntegration.default.create(itemDetails));
            return Ember.A(slackObj);
        },

        serialize(deserialized) {
            let settingsArray;
            if (Ember.isArray(deserialized)) {
                settingsArray = deserialized.map(item => {
                    let url = (item.get('url') || '').trim();

                    return { url };
                }).compact();
            } else {
                settingsArray = [];
            }
            return JSON.stringify(settingsArray);
        }
    });
});