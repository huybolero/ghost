define('ghost-admin/transforms/navigation-settings', ['exports', 'ghost-admin/models/navigation-item', 'ember-data/transform'], function (exports, _navigationItem, _transform) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _transform.default.extend({
        deserialize(serialized) {
            let navItems, settingsArray;

            try {
                settingsArray = JSON.parse(serialized) || [];
            } catch (e) {
                settingsArray = [];
            }

            navItems = settingsArray.map(itemDetails => _navigationItem.default.create(itemDetails));

            return Ember.A(navItems);
        },

        serialize(deserialized) {
            let settingsArray;

            if (Ember.isArray(deserialized)) {
                settingsArray = deserialized.map(item => {
                    let label = item.get('label').trim();
                    let url = item.get('url').trim();

                    return { label, url };
                }).compact();
            } else {
                settingsArray = [];
            }

            return JSON.stringify(settingsArray);
        }
    });
});