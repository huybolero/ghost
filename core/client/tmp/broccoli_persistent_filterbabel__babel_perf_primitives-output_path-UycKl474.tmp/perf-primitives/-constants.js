define("perf-primitives/-constants", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  const SMALL_ARRAY_LENGTH = exports.SMALL_ARRAY_LENGTH = 200;
  const LARGE_ARRAY_LENGTH = exports.LARGE_ARRAY_LENGTH = 64000;

  const UNDEFINED_KEY = exports.UNDEFINED_KEY = Object.create(null);
});