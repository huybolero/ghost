define('liquid-fire/helpers/lf-or', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.lfOr = lfOr;
  function lfOr(params /*, hash*/) {
    return params.reduce((a, b) => a || b, false);
  }

  exports.default = Ember.Helper.helper(lfOr);
});