define("liquid-fire/action", ["exports", "liquid-fire/promise"], function (exports, _promise) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  class Action {
    constructor(nameOrHandler, args = [], opts = {}) {
      if (typeof nameOrHandler === 'function') {
        this.handler = nameOrHandler;
      } else {
        this.name = nameOrHandler;
      }
      this.reversed = opts.reversed;
      this.args = args;
    }

    validateHandler(transitionMap) {
      if (!this.handler) {
        this.handler = transitionMap.lookup(this.name);
      }
    }

    run(context) {
      return new _promise.default((resolve, reject) => {
        _promise.default.resolve(this.handler.apply(context, this.args)).then(resolve, reject);
      });
    }
  }
  exports.default = Action;
});