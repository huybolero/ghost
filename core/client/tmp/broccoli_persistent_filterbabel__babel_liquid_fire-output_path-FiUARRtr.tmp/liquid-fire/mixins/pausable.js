define('liquid-fire/mixins/pausable', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Mixin.create({
    _transitionMap: Ember.inject.service('liquid-fire-transitions'),

    _initializeLiquidFirePauseable: Ember.on('init', function () {
      this._lfDefer = [];
    }),
    pauseLiquidFire() {
      const context = this.nearestWithProperty('_isLiquidChild');
      if (context) {
        let def = new Ember.RSVP.defer();
        let tmap = this.get('_transitionMap');
        tmap.incrementRunningTransitions();
        def.promise.finally(() => tmap.decrementRunningTransitions());
        this._lfDefer.push(def);
        context._waitForMe(def.promise);
      }
    },
    resumeLiquidFire: Ember.on('willDestroyElement', function () {
      let def = this._lfDefer.pop();
      if (def) {
        def.resolve();
      }
    })
  });
});