define('ember-cli-ghost-spirit/helpers/ui-btn', ['exports', 'spirit-product'], function (exports, _spiritProduct) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.uiBtn = uiBtn;
    function uiBtn([style], hash) {
        return _spiritProduct.default.button(Object.assign({}, { style }, hash)).button;
    }

    exports.default = Ember.Helper.helper(uiBtn);
});