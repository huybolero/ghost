define('ember-cli-ghost-spirit/helpers/ui-text', ['exports', 'spirit-product'], function (exports, _spiritProduct) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.uiText = uiText;
    function uiText([style]) {
        return _spiritProduct.default.uiText(style);
    }

    exports.default = Ember.Helper.helper(uiText);
});