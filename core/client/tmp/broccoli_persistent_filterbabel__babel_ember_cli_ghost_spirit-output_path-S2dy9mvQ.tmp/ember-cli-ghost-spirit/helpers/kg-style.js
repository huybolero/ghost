define('ember-cli-ghost-spirit/helpers/kg-style', ['exports', 'spirit-product'], function (exports, _spiritProduct) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.kgStyle = kgStyle;
    function kgStyle([style], options) {
        return _spiritProduct.default.koenig(style, options);
    }

    exports.default = Ember.Helper.helper(kgStyle);
});