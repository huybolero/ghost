define('ember-cli-ghost-spirit/helpers/ui-btn-span', ['exports', 'spirit-product'], function (exports, _spiritProduct) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.uiBtnSpan = uiBtnSpan;
    function uiBtnSpan([style], hash) {
        return _spiritProduct.default.button(Object.assign({}, { style }, hash)).span;
    }

    exports.default = Ember.Helper.helper(uiBtnSpan);
});