var guidFor = Ember.guidFor;


export default function identity(item) {
  var key = void 0;
  var type = typeof item;

  if (type === 'string' || type === 'number') {
    key = item;
  } else {
    key = guidFor(item);
  }

  return key;
}