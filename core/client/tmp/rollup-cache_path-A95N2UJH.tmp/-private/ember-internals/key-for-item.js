
import identity from './identity';

var get = Ember.get;


export default function keyForItem(item, keyPath, index) {
  var key = void 0;

  switch (keyPath) {
    case '@index':
      // allow 0 index
      if (!index && index !== 0) {
        throw new Error('No index was supplied to keyForItem');
      }
      key = index;
      break;
    case '@identity':
      key = identity(item);
      break;
    default:
      // TODO add explicit test
      if (keyPath) {
        key = get(item, keyPath);
      } else {
        key = identity(item);
      }
  }

  if (typeof key === 'number') {
    key = String(key);
  }

  return key;
}