export default function getHeight(dims, withMargins) {
  var height = void 0;

  switch (dims.boxSizing) {
    case 'border-box':
      height = dims.height + dims.borderTopWidth + dims.borderBottomWidth + dims.paddingTop + dims.paddingBottom;
      break;
    case 'content-box':
      height = dims.height;
      break;
    default:
      height = dims.height;
      break;
  }

  // TODO add explicit test
  if (withMargins) {
    height += dims.marginTop + dims.marginBottom;
  }
  return height;
}