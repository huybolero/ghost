

export function estimateElementHeight(element, fallbackHeight) {
  (true && !(fallbackHeight) && Ember.assert(`You called estimateElement height without a fallbackHeight`, fallbackHeight));
  (true && !(element) && Ember.assert(`You called estimateElementHeight without an element`, element));


  if (fallbackHeight.indexOf('%') !== -1) {
    return Math.max(getPercentageHeight(element, fallbackHeight), 1);
  }

  if (fallbackHeight.indexOf('em') !== -1) {
    return Math.max(getEmHeight(element, fallbackHeight), 1);
  }

  // px or no units
  return Math.max(parseInt(fallbackHeight, 10), 1);
}

/**
 * Returns estimated max height of an element.
 */
export function estimateElementMaxHeight(element) {
  var maxHeightString = window.getComputedStyle(element).maxHeight;
  if (maxHeightString.indexOf('%') !== -1) {
    return getPercentageHeight(element, maxHeightString);
  }

  if (maxHeightString.indexOf('em') !== -1) {
    return getEmHeight(element, maxHeightString);
  }

  // px or no units
  return parseInt(maxHeightString, 10);
}

function getPercentageHeight(element, fallbackHeight) {
  var parentHeight = element.getBoundingClientRect().height;
  var per = parseFloat(fallbackHeight);

  return per * parentHeight / 100.0;
}

function getEmHeight(element, fallbackHeight) {
  var fontSizeElement = fallbackHeight.indexOf('rem') !== -1 ? document.body : element;
  var fontSize = window.getComputedStyle(fontSizeElement).getPropertyValue('font-size');

  return parseFloat(fallbackHeight) * parseFloat(fontSize);
}