define('ember-light-table/components/lt-infinity', ['exports', 'ember-light-table/templates/components/lt-infinity', 'ember-in-viewport'], function (exports, _ltInfinity, _emberInViewport) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend(_emberInViewport.default, {
    classNames: ['lt-infinity'],
    classNameBindings: ['viewportEntered:in-viewport'],
    layout: _ltInfinity.default,

    rows: null,
    scrollBuffer: null,

    didInsertElement() {
      this._super(...arguments);

      let scrollBuffer = this.get('scrollBuffer');
      let width = this.$().width();

      this.setProperties({
        viewportSpy: true,
        viewportTolerance: {
          left: width,
          right: width,
          bottom: scrollBuffer,
          top: 0
        }
      });
    },

    willDestroyElement() {
      this._super(...arguments);
      this._cancelTimers();
    },

    didEnterViewport() {
      this._debounceScrolledToBottom();
    },

    didExitViewport() {
      this._cancelTimers();
    },

    scheduleScrolledToBottom: Ember.observer('rows.[]', 'viewportEntered', function () {
      if (this.get('viewportEntered')) {
        /*
         Continue scheduling onScrolledToBottom until no longer in viewport
         */
        this._scheduleScrolledToBottom();
      }
    }),

    _scheduleScrolledToBottom() {
      this._schedulerTimer = Ember.run.scheduleOnce('afterRender', this, this._debounceScrolledToBottom);
    },

    _debounceScrolledToBottom(delay = 100) {
      /*
       This debounce is needed when there is not enough delay between onScrolledToBottom calls.
       Without this debounce, all rows will be rendered causing immense performance problems
       */
      this._debounceTimer = Ember.run.debounce(this, this.sendAction, 'onScrolledToBottom', delay);
    },

    _cancelTimers() {
      Ember.run.cancel(this._schedulerTimer);
      Ember.run.cancel(this._debounceTimer);
    }
  });
});