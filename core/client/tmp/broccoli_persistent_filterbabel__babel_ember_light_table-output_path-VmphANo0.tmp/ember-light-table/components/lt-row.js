define('ember-light-table/components/lt-row', ['exports', 'ember-light-table/templates/components/lt-row'], function (exports, _ltRow) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const Row = Ember.Component.extend({
    layout: _ltRow.default,
    tagName: 'tr',
    classNames: ['lt-row'],
    classNameBindings: ['isSelected', 'isExpanded', 'canExpand:is-expandable', 'canSelect:is-selectable', 'row.classNames'],
    attributeBindings: ['colspan', 'data-row-id'],

    columns: null,
    row: null,
    tableActions: null,
    extra: null,
    canExpand: false,
    canSelect: false,
    colspan: 1,

    isSelected: Ember.computed.readOnly('row.selected'),
    isExpanded: Ember.computed.readOnly('row.expanded')
  });

  Row.reopenClass({
    positionalParams: ['row', 'columns']
  });

  exports.default = Row;
});