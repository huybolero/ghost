define('ember-in-viewport/utils/check-scroll-direction', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = checkScrollDirection;
  const floor = Math.floor;
  function checkScrollDirection(lastPosition = null, newPosition = {}, sensitivity = 1) {
    if (!lastPosition) {
      return 'none';
    }

    (true && !(sensitivity) && Ember.assert('sensitivity cannot be 0', sensitivity));
    const top = newPosition.top,
          left = newPosition.left;
    const lastTop = lastPosition.top,
          lastLeft = lastPosition.left;


    const delta = {
      top: floor((top - lastTop) / sensitivity) * sensitivity,
      left: floor((left - lastLeft) / sensitivity) * sensitivity
    };

    if (delta.top > 0) {
      return 'down';
    }

    if (delta.top < 0) {
      return 'up';
    }

    if (delta.left > 0) {
      return 'right';
    }

    if (delta.left < 0) {
      return 'left';
    }
  }
});