define('ember-in-viewport/index', ['exports', 'ember-in-viewport/mixins/in-viewport'], function (exports, _inViewport) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _inViewport.default;
});