define('koenig-editor/utils/markup-utils', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getLinkMarkupFromRange = getLinkMarkupFromRange;
    function getLinkMarkupFromRange(range) {
        let headMarker = range.headMarker,
            tailMarker = range.tailMarker;

        if (headMarker === tailMarker || headMarker.next === tailMarker) {
            return tailMarker.markups.findBy('tagName', 'a');
        }
    }
});