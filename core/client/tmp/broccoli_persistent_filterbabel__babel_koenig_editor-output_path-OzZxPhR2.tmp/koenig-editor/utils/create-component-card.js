define('koenig-editor/utils/create-component-card', ['exports', 'koenig-editor/components/koenig-editor'], function (exports, _koenigEditor) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = createComponentCard;


    const RENDER_TYPE = 'dom';
    const DEFAULT_KOENIG_OPTIONS = {
        hasEditMode: true,
        selectAfterInsert: true
    };

    function renderFallback(doc) {
        let element = doc.createElement('div');
        let text = doc.createTextNode('[placeholder for Ember component card]');
        element.appendChild(text);
        return element;
    }

    // sets up boilerplate for an Ember component card
    function createComponentCard(name, koenigOptions, doc = window.document) {
        return {
            name,
            type: RENDER_TYPE,

            // Called when the card is added to a mobiledoc document.
            // The `cardArg.options` object contains the methods that were set up
            // on the `cardOptions` property of the editor in `{{koenig-editor}}`,
            // by calling those hooks we set up everything needed for rendering
            // ember components as cards
            render(cardArg) {
                let env = cardArg.env,
                    options = cardArg.options;

                let kgOptions = Ember.assign({}, DEFAULT_KOENIG_OPTIONS, koenigOptions);

                if (!options[_koenigEditor.ADD_CARD_HOOK]) {
                    return renderFallback(doc);
                }

                var _options$ADD_CARD_HOO = options[_koenigEditor.ADD_CARD_HOOK](cardArg, kgOptions);

                let card = _options$ADD_CARD_HOO.card,
                    element = _options$ADD_CARD_HOO.element;
                let onTeardown = env.onTeardown;


                onTeardown(() => options[_koenigEditor.REMOVE_CARD_HOOK](card));

                return element;
            }
        };
    }
});