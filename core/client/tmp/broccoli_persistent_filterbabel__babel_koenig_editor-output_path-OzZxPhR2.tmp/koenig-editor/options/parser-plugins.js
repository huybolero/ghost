define('koenig-editor/options/parser-plugins', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.brToSoftBreakAtom = brToSoftBreakAtom;
    exports.removeLeadingNewline = removeLeadingNewline;
    exports.imgToCard = imgToCard;
    exports.hrToCard = hrToCard;
    exports.preCodeToCard = preCodeToCard;

    var _slicedToArray = function () {
        function sliceIterator(arr, i) {
            var _arr = [];
            var _n = true;
            var _d = false;
            var _e = undefined;

            try {
                for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
                    _arr.push(_s.value);

                    if (i && _arr.length === i) break;
                }
            } catch (err) {
                _d = true;
                _e = err;
            } finally {
                try {
                    if (!_n && _i["return"]) _i["return"]();
                } finally {
                    if (_d) throw _e;
                }
            }

            return _arr;
        }

        return function (arr, i) {
            if (Array.isArray(arr)) {
                return arr;
            } else if (Symbol.iterator in Object(arr)) {
                return sliceIterator(arr, i);
            } else {
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            }
        };
    }();

    // mobiledoc by default ignores <BR> tags but we have a custom SoftReturn atom
    function brToSoftBreakAtom(node, builder, { addMarkerable, nodeFinished }) {
        if (node.nodeType !== 1 || node.tagName !== 'BR') {
            return;
        }

        let softReturn = builder.createAtom('soft-return');
        addMarkerable(softReturn);

        nodeFinished();
    }

    // leading newlines in text nodes will add a space to the beginning of the text
    // which doesn't render correctly if we're replacing <br> with SoftReturn atoms
    // after parsing text as markdown to html
    function removeLeadingNewline(node) {
        if (node.nodeType !== 3 || node.nodeName !== '#text') {
            return;
        }

        node.nodeValue = node.nodeValue.replace(/^\n/, '');
    }

    function imgToCard(node, builder, { addSection, nodeFinished }) {
        if (node.nodeType !== 1 || node.tagName !== 'IMG') {
            return;
        }

        let payload = { src: node.src };

        // TODO: this is a workaround for grabbing a figure>img+figcaption until
        // https://github.com/bustle/mobiledoc-kit/issues/494 is resolved
        // NOTE: it will only work in a strict <figure><img><figcaption></figure> case
        let nextSibling = node.nextSibling;
        if (nextSibling && nextSibling.tagName === 'FIGCAPTION') {
            payload.caption = nextSibling.textContent;
            node.parentNode.removeChild(nextSibling);
        }

        let cardSection = builder.createCardSection('image', payload);
        addSection(cardSection);
        nodeFinished();
    }

    function hrToCard(node, builder, { addSection, nodeFinished }) {
        if (node.nodeType !== 1 || node.tagName !== 'HR') {
            return;
        }

        let cardSection = builder.createCardSection('hr');
        addSection(cardSection);
        nodeFinished();
    }

    function preCodeToCard(node, builder, { addSection, nodeFinished }) {
        if (node.nodeType !== 1 || node.tagName !== 'PRE') {
            return;
        }

        var _node$children = _slicedToArray(node.children, 1);

        let codeElement = _node$children[0];


        if (codeElement && codeElement.tagName === 'CODE') {
            let payload = { code: codeElement.textContent };
            let cardSection = builder.createCardSection('code', payload);
            addSection(cardSection);
            nodeFinished();
        }
    }

    exports.default = [brToSoftBreakAtom, removeLeadingNewline, imgToCard, hrToCard, preCodeToCard];
});