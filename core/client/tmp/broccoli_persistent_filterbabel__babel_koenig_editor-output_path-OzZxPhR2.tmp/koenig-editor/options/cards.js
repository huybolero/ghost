define('koenig-editor/options/cards', ['exports', 'koenig-editor/utils/create-component-card'], function (exports, _createComponentCard) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = [(0, _createComponentCard.default)('hr', { hasEditMode: false, selectAfterInsert: false }), (0, _createComponentCard.default)('image', { hasEditMode: false }), (0, _createComponentCard.default)('markdown'), (0, _createComponentCard.default)('card-markdown'), // backwards-compat with markdown editor
    (0, _createComponentCard.default)('html'), (0, _createComponentCard.default)('code')];
});