define('koenig-editor/options/atoms', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = [
    // soft-return is triggered by SHIFT+ENTER and allows for line breaks
    // without creating paragraphs
    {
        name: 'soft-return',
        type: 'dom',
        render() {
            return document.createElement('br');
        }
    }];
});