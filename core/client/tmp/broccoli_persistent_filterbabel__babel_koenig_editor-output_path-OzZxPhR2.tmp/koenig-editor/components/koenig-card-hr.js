define('koenig-editor/components/koenig-card-hr', ['exports', 'koenig-editor/templates/components/koenig-card-hr'], function (exports, _koenigCardHr) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        layout: _koenigCardHr.default,
        tagName: ''
    });
});