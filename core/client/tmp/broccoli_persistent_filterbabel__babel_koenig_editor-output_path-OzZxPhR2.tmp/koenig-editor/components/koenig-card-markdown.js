define('koenig-editor/components/koenig-card-markdown', ['exports', 'ghost-admin/utils/format-markdown', 'koenig-editor/templates/components/koenig-card-markdown'], function (exports, _formatMarkdown, _koenigCardMarkdown) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });


    const MIN_HEIGHT = 130;

    exports.default = Ember.Component.extend({
        layout: _koenigCardMarkdown.default,

        // attrs
        payload: null,
        isSelected: false,
        isEditing: false,
        headerOffset: 0,

        // internal attrs
        bottomOffset: 0,

        // closure actions
        editCard() {},
        saveCard() {},
        selectCard() {},
        deleteCard() {},

        renderedMarkdown: Ember.computed('payload.markdown', function () {
            return Ember.String.htmlSafe((0, _formatMarkdown.default)(this.payload.markdown));
        }),

        toolbar: Ember.computed('isEditing', function () {
            if (!this.isEditing) {
                return {
                    items: [{
                        buttonClass: 'fw4 flex items-center white',
                        icon: 'koenig/kg-edit-v2',
                        iconClass: 'stroke-white',
                        title: 'Edit',
                        text: '',
                        action: Ember.run.bind(this, this.editCard)
                    }]
                };
            }
        }),

        init() {
            this._super(...arguments);

            if (!this.payload) {
                this.set('payload', {});
            }

            // subtract toolbar height from MIN_HEIGHT so the trigger happens at
            // the expected position without forcing the min height to be too small
            this.set('bottomOffset', -MIN_HEIGHT - 49);
        },

        willDestroyElement() {
            this._super(...arguments);
            this._teardownResizeHandler();
        },

        actions: {
            enterEditMode() {},

            leaveEditMode() {
                if (Ember.isBlank(this.payload.markdown)) {
                    // afterRender is required to avoid double modification of `isSelected`
                    // TODO: see if there's a way to avoid afterRender
                    Ember.run.scheduleOnce('afterRender', this, function () {
                        this.deleteCard();
                    });
                }
            },

            updateMarkdown(markdown) {
                let payload = this.payload;
                let save = this.saveCard;

                Ember.set(payload, 'markdown', markdown);

                // update the mobiledoc and stay in edit mode
                save(payload, false);
            },

            // fires if top comes into view 0 px from viewport top
            // fires if top comes into view MIN_HEIGHTpx above viewport bottom
            topEntered() {
                this._isTopVisible = true;
                Ember.run.scheduleOnce('actions', this, this._applyToolbarStyles);
            },

            // fires if top leaves viewport 0 px from viewport top
            // fires if top leaves viewport MIN_HEIGHTpx above viewport bottom
            topExited() {
                let top = this._topElement.getBoundingClientRect().top;
                this._isTopVisible = false;
                this._isTopAbove = top < 0;
                Ember.run.scheduleOnce('actions', this, this._applyToolbarStyles);
            },

            bottomEntered() {
                this._isBottomVisible = true;
                Ember.run.scheduleOnce('actions', this, this._applyToolbarStyles);
            },

            bottomExited() {
                let top = this._bottomElement.getBoundingClientRect().top;
                this._isBottomVisible = false;
                this._isBottomBelow = top > window.innerHeight;
                Ember.run.scheduleOnce('actions', this, this._applyToolbarStyles);
            },

            registerTop(element) {
                this._topElement = element;
            },

            registerBottom(element) {
                this._bottomElement = element;
            }
        },

        _applyToolbarStyles() {
            let toolbar = this.element.querySelector('.editor-toolbar');

            if (!toolbar) {
                return;
            }

            var _containerDimensions = this._containerDimensions();

            let left = _containerDimensions.left,
                width = _containerDimensions.width;


            let style = '';
            let stuckTop = `top: ${MIN_HEIGHT}px; bottom: auto`;
            let fixedBottom = `position: fixed; left: ${left + 1}px; width: ${width - 2}px`;
            let stuckBottom = '';

            if (this._isTopVisible && this._isBottomVisible) {
                style = stuckBottom;
            }

            if (this._isTopVisible && !this._isBottomVisible) {
                style = fixedBottom;
            }

            if (!this._isTopVisible && !this._isTopAbove) {
                style = stuckTop;
            }

            if (!this._isTopVisible && this._isBottomVisible) {
                style = stuckBottom;
            }

            if (!this._isTopVisible && !this._isBottomVisible && this._isTopAbove && this._isBottomBelow) {
                style = fixedBottom;
            }

            // set up resize watchers if in fixed position because we have to
            // recalculate left position and width
            if (!this._resizeHandler && style === fixedBottom) {
                this._setupResizeHandler();
            } else if (this._resizeHandler && style !== fixedBottom) {
                this._teardownResizeHandler();
            }

            // account for the mobile nav bar when in fixed position
            if (style === fixedBottom) {
                let mobileNav = document.querySelector('.gh-mobile-nav-bar');

                if (mobileNav.offsetHeight) {
                    style = `${style}; bottom: ${mobileNav.offsetHeight}px`;
                }
            }

            toolbar.setAttribute('style', style);
        },

        _containerDimensions() {
            return this.element.querySelector('.kg-card-selected').getBoundingClientRect();
        },

        _setupResizeHandler() {
            if (this._resizeHandler) {
                return;
            }

            this._resizeHandler = Ember.run.bind(this, this._applyToolbarStyles);
            window.addEventListener('resize', this._resizeHandler);
        },

        _teardownResizeHandler() {
            window.removeEventListener('resize', this._resizeHandler);
            this._resizeHandler = null;
        }

    });
});