define('koenig-editor/components/koenig-card-code', ['exports', 'koenig-editor/templates/components/koenig-card-code'], function (exports, _koenigCardCode) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    const Handlebars = Ember.Handlebars;
    exports.default = Ember.Component.extend({
        layout: _koenigCardCode.default,

        // attrs
        payload: null,
        isSelected: false,
        isEditing: false,
        headerOffset: 0,

        // closure actions
        editCard() {},
        saveCard() {},
        selectCard() {},
        deleteCard() {},

        toolbar: Ember.computed('isEditing', function () {
            if (!this.isEditing) {
                return {
                    items: [{
                        buttonClass: 'fw4 flex items-center white',
                        icon: 'koenig/kg-edit-v2',
                        iconClass: 'stroke-white',
                        title: 'Edit',
                        text: '',
                        action: Ember.run.bind(this, this.editCard)
                    }]
                };
            }
        }),

        escapedCode: Ember.computed('payload.code', function () {
            let escapedCode = Handlebars.Utils.escapeExpression(this.payload.code);
            return Ember.String.htmlSafe(escapedCode);
        }),

        init() {
            this._super(...arguments);
            let payload = this.payload || {};

            // CodeMirror errors on a `null` or `undefined` value
            if (!payload.code) {
                Ember.set(payload, 'code', '');
            }

            this.set('payload', payload);
        },

        actions: {
            updateCode(code) {
                this._updatePayloadAttr('code', code);
            },

            leaveEditMode() {
                if (Ember.isBlank(this.payload.code)) {
                    // afterRender is required to avoid double modification of `isSelected`
                    // TODO: see if there's a way to avoid afterRender
                    Ember.run.scheduleOnce('afterRender', this, function () {
                        this.deleteCard();
                    });
                }
            }
        },

        _updatePayloadAttr(attr, value) {
            let payload = this.payload;
            let save = this.saveCard;

            Ember.set(payload, attr, value);

            // update the mobiledoc and stay in edit mode
            save(payload, false);
        }
    });
});