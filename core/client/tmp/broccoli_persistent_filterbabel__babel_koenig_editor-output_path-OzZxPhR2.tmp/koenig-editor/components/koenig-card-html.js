define('koenig-editor/components/koenig-card-html', ['exports', 'koenig-editor/templates/components/koenig-card-html'], function (exports, _koenigCardHtml) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        layout: _koenigCardHtml.default,

        // attrs
        payload: null,
        isSelected: false,
        isEditing: false,
        headerOffset: 0,

        // closure actions
        editCard() {},
        saveCard() {},
        selectCard() {},
        deleteCard() {},

        toolbar: Ember.computed('isEditing', function () {
            if (!this.isEditing) {
                return {
                    items: [{
                        buttonClass: 'fw4 flex items-center white',
                        icon: 'koenig/kg-edit-v2',
                        iconClass: 'stroke-white',
                        title: 'Edit',
                        text: '',
                        action: Ember.run.bind(this, this.editCard)
                    }]
                };
            }
        }),

        init() {
            this._super(...arguments);
            let payload = this.payload || {};

            // CodeMirror errors on a `null` or `undefined` value
            if (!payload.html) {
                Ember.set(payload, 'html', '');
            }

            this.set('payload', payload);
        },

        actions: {
            updateHtml(html) {
                this._updatePayloadAttr('html', html);
            },

            leaveEditMode() {
                if (Ember.isBlank(this.payload.html)) {
                    // afterRender is required to avoid double modification of `isSelected`
                    // TODO: see if there's a way to avoid afterRender
                    Ember.run.scheduleOnce('afterRender', this, function () {
                        this.deleteCard();
                    });
                }
            }
        },

        _updatePayloadAttr(attr, value) {
            let payload = this.payload;
            let save = this.saveCard;

            Ember.set(payload, attr, value);

            // update the mobiledoc and stay in edit mode
            save(payload, false);
        }
    });
});