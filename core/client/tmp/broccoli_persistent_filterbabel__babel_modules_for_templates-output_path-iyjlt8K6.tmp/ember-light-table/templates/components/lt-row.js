define("ember-light-table/templates/components/lt-row", ["exports"], function (exports) {
  "use strict";

  exports.__esModule = true;
  exports.default = Ember.HTMLBars.template({ "id": "63YPD3k/", "block": "{\"symbols\":[\"column\"],\"statements\":[[4,\"each\",[[22,[\"columns\"]]],null,{\"statements\":[[0,\"  \"],[1,[26,\"component\",[[26,\"concat\",[\"light-table/cells/\",[21,1,[\"cellType\"]]],null],[21,1,[]],[22,[\"row\"]]],[[\"table\",\"rawValue\",\"tableActions\",\"extra\"],[[22,[\"table\"]],[26,\"get\",[[22,[\"row\"]],[21,1,[\"valuePath\"]]],null],[22,[\"tableActions\"]],[22,[\"extra\"]]]]],false],[0,\"\\n\"]],\"parameters\":[1]},null]],\"hasEval\":false}", "meta": { "moduleName": "ember-light-table/templates/components/lt-row.hbs" } });
});