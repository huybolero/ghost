define("ember-light-table/templates/components/lt-scrollable", ["exports"], function (exports) {
  "use strict";

  exports.__esModule = true;
  exports.default = Ember.HTMLBars.template({ "id": "g63UgPiJ", "block": "{\"symbols\":[\"scrollbar\",\"&default\"],\"statements\":[[4,\"if\",[[22,[\"virtualScrollbar\"]]],null,{\"statements\":[[4,\"ember-scrollable\",null,[[\"classNames\",\"autoHide\",\"horizontal\",\"vertical\",\"scrollToY\",\"onScrollY\"],[\"lt-scrollable\",[22,[\"autoHide\"]],[22,[\"horizontal\"]],[22,[\"vertical\"]],[22,[\"scrollTo\"]],[22,[\"onScrollY\"]]]],{\"statements\":[[0,\"    \"],[13,2],[0,\"\\n\"]],\"parameters\":[1]},null]],\"parameters\":[]},{\"statements\":[[0,\"  \"],[13,2],[0,\"\\n\"]],\"parameters\":[]}]],\"hasEval\":false}", "meta": { "moduleName": "ember-light-table/templates/components/lt-scrollable.hbs" } });
});