define("ember-svg-jar/inlined/arrow-down", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path fill=\"#010101\" d=\"M47.658 10.104a.999.999 0 0 0-1.411.095L24 35.624 1.753 10.2a1 1 0 0 0-1.506 1.316l23 26.285a.999.999 0 0 0 1.506-.001l23-26.285a1 1 0 0 0-.095-1.411z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 48 48" } };
});