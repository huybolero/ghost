define("ember-svg-jar/inlined/koenig/kg-thin-delete", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path stroke-width=\"1.2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M1 4h13.5M13 4v9.5c0 1-.5 1.5-1.5 1.5H4c-1 0-1.5-.5-1.5-1.5V4m2.25 0V2.5c0-.414.146-.768.44-1.06A1.44 1.44 0 0 1 6.25 1h3c1 0 1.5.5 1.5 1.5V4m-4.5 2.75v5.5m3-5.5v5.5\" stroke=\"#FFF\" fill=\"none\" fill-rule=\"evenodd\"/>", "attrs": { "viewBox": "0 0 16 16", "xmlns": "http://www.w3.org/2000/svg" } };
});