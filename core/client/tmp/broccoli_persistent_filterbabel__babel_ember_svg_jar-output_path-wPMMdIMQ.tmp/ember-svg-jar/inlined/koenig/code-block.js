define("ember-svg-jar/inlined/koenig/code-block", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g fill=\"none\" fill-rule=\"evenodd\" stroke=\"#738A94\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M2 2h19l9 9v19H2z\"/><path d=\"M11 21l-3-3 3-3m10 6l3-3-3-3m-7 6l4-6m3.041-12.863v8.913h8.904\"/></g>", "attrs": { "viewBox": "0 0 32 32", "xmlns": "http://www.w3.org/2000/svg" } };
});