define("ember-svg-jar/inlined/koenig/html", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g fill=\"none\" fill-rule=\"evenodd\" stroke=\"#738A94\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M2 2h19l9 9v19H2z\"/><path d=\"M21.041 2.137v8.913h8.904M6 19.667V15m3.188 4.667V15M6 17.333h3.188m11.303 2.334V15l-2.123 3.333L16.245 15v4.667m10.456 0h-3.188V15m-10.766 4.667V15m-1.596 0h3.188\"/></g>", "attrs": { "viewBox": "0 0 32 32", "xmlns": "http://www.w3.org/2000/svg" } };
});