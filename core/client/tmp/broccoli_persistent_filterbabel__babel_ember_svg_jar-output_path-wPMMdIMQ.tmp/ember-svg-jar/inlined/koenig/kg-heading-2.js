define("ember-svg-jar/inlined/koenig/kg-heading-2", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M5 5.483V13m6-7.517V13M5 9h6\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>", "attrs": { "viewBox": "0 0 16 16", "xmlns": "http://www.w3.org/2000/svg" } };
});