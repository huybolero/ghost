define("ember-svg-jar/inlined/koenig/card-indicator-markdown", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path stroke=\"#9BAEB8\" stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M2 14V6l4 4 4-4v8m6-7.93v7.916M13.5 11.5l2.48 2.525L18.5 11.5\" fill=\"none\" fill-rule=\"evenodd\"/>", "attrs": { "viewBox": "0 0 20 20", "xmlns": "http://www.w3.org/2000/svg" } };
});