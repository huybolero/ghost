define("ember-svg-jar/inlined/koenig/kg-replace", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#FFF\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path stroke-width=\"2\" d=\"M2 10h5v4H2zm7-8h5v4H9z\"/><path d=\"M10 14h1a1 1 0 0 0 1-1V9\"/><path d=\"M14.034 11.054L12.002 9l-2.009 2.054M6 2H5a1 1 0 0 0-1 1v4\"/><path d=\"M1.966 4.946L3.998 7l2.009-2.054\"/></g>", "attrs": { "viewBox": "0 0 16 16", "xmlns": "http://www.w3.org/2000/svg" } };
});