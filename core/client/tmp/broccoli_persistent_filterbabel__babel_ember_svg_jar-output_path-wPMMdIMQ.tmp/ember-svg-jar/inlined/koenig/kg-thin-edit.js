define("ember-svg-jar/inlined/koenig/kg-thin-edit", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#FFF\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M5.255 13.644l-3.772.885.885-3.772 9.1-9.1a1.166 1.166 0 0 1 1.65 0l1.238 1.237a1.168 1.168 0 0 1 0 1.65l-9.101 9.1zm7.857-7.854l-2.881-2.896 2.881 2.896z\"/><path d=\"M5.402 13.497L2.514 10.61l2.888 2.887z\"/></g>", "attrs": { "viewBox": "0 0 16 16", "xmlns": "http://www.w3.org/2000/svg" } };
});