define("ember-svg-jar/inlined/koenig/kg-img-full", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#FFF\" fill=\"none\" fill-rule=\"evenodd\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M1 2v12M15 2v12\" opacity=\".6\"/><path d=\"M4 8h8M6.046 4.983L3.018 8l3.028 3.058m3.972-6.075L13.046 8l-3.028 3.058\"/></g>", "attrs": { "viewBox": "0 0 16 16", "xmlns": "http://www.w3.org/2000/svg" } };
});