define("ember-svg-jar/inlined/koenig/divider", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M2 12V4a2 2 0 0 1 2-2h24a2 2 0 0 1 2 2v8M2 20v8a2 2 0 0 0 2 2h24a2 2 0 0 0 2-2v-8M1 16h30\" stroke=\"#738A94\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" fill-rule=\"evenodd\"/>", "attrs": { "viewBox": "0 0 32 32", "xmlns": "http://www.w3.org/2000/svg" } };
});