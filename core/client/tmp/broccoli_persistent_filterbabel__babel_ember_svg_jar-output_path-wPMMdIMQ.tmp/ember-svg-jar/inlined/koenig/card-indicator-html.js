define("ember-svg-jar/inlined/koenig/card-indicator-html", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M1 12V7m3 5V7M1 9.472h2.364M14 12V7l-1.5 2.472L11 7v5m8 0h-2.5V7m-9 5V7M6 7h3\" stroke=\"#9BAEB8\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" fill-rule=\"evenodd\"/>", "attrs": { "viewBox": "0 0 20 20", "xmlns": "http://www.w3.org/2000/svg" } };
});