define("ember-svg-jar/inlined/download-circle", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M11.5 0C5.159 0 0 5.159 0 11.5S5.159 23 11.5 23 23 17.841 23 11.5 17.841 0 11.5 0zm0 22C5.71 22 1 17.29 1 11.5S5.71 1 11.5 1 22 5.71 22 11.5 17.29 22 11.5 22zm5.146-9.854L12 16.793V5a.5.5 0 0 0-1 0v11.793l-4.647-4.646a.5.5 0 0 0-.707.707l5.499 5.5.163.108.192.038.191-.039.163-.108 5.499-5.5a.5.5 0 0 0-.707-.707z\"/>", "attrs": { "version": "1", "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});