define("ember-svg-jar/inlined/list-number", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M6.5 4.5h17m-17 8h17m-17 8h17M2 6.5V2.573L.5 4.037M2.569 14.5H.5c1.48-2 2-1.836 2-2.963a.986.986 0 0 0-1-.982.948.948 0 0 0-.965.974M.5 18.5h1.931l-1 1.537c.826 0 1 .481 1 .981s-.174.982-1 .982H.5\" stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});