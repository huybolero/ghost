define("ember-svg-jar/inlined/store", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\" d=\"M21.502 10.333V22.5h-19V10.313\"/><path stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\" d=\"M4.502 13.5h8v6h-8zm10 0h5v9h-5zm7-11h-19l-2 4h23zm2 5a3 3 0 0 1-3 3c-.826 0-1.457-.46-2-1-.708 1.125-2.073 2-3.5 2a3.988 3.988 0 0 1-3-1.36 3.983 3.983 0 0 1-3 1.36c-1.427 0-2.792-.875-3.5-2-.542.54-1.174 1-2 1a3 3 0 0 1-3-3v-1h23v1zm-18 2v-3l1-4m5.5 7.646V2.5m6.5 7v-3l-1-4\"/><path d=\"M17.502 18a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});