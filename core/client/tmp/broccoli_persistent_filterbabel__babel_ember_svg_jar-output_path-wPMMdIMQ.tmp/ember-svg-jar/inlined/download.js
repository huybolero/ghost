define("ember-svg-jar/inlined/download", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M20 5.5l-8 8-8-8m-3.5 13h23\" fill=\"none\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});