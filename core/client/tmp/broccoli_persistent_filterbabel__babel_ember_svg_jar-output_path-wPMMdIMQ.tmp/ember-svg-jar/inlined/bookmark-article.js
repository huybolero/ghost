define("ember-svg-jar/inlined/bookmark-article", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\"><path d=\"M11.5 1.5h12v22H.5v-22h3\"/><path d=\"M11.5 13.5l-4-4-4 4V.5h8zm3-6h6m-6 3h6m-6 3h6m-17 3h17m-17 3h13\"/></g>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});