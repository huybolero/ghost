define("ember-svg-jar/inlined/arrow-left-small", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M13.54.426L.397 11.926l13.143 11.5\" transform=\"translate(2 2)\" stroke-width=\"3\" stroke=\"#0B0B0A\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>", "attrs": { "viewBox": "0 0 17 27", "xmlns": "http://www.w3.org/2000/svg" } };
});