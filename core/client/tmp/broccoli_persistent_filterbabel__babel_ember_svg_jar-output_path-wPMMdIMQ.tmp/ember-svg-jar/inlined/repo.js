define("ember-svg-jar/inlined/repo", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M20.5 4H20V.5a.5.5 0 0 0-.5-.5h-14A2.503 2.503 0 0 0 3 2.5v19C3 22.878 4.121 24 5.5 24h15a.5.5 0 0 0 .5-.5v-19a.5.5 0 0 0-.5-.5zm-15-3H19v3h-1v-.5a.5.5 0 0 0-.5-.5h-6a.5.5 0 0 0-.5.5V4H5.5C4.673 4 4 3.327 4 2.5S4.673 1 5.5 1zM12 4h5v10.293l-2.146-2.147a.502.502 0 0 0-.708 0L12 14.293V4zm8 19H5.5c-.827 0-1.5-.673-1.5-1.5V4.487c.419.318.935.513 1.5.513H11v10.5a.5.5 0 0 0 .854.354l2.646-2.647 2.646 2.647A.504.504 0 0 0 17.5 16l.191-.038A.5.5 0 0 0 18 15.5V5h2v18z\"/>", "attrs": { "version": "1", "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});