define("ember-svg-jar/inlined/account-group", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#000\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\"><path d=\"M4 6.609V8l-2.539.726C.933 8.877.5 9.45.5 10v.5H6m1-3.922V8l2.538.726c.529.151.962.724.962 1.274v.5H5\"/><ellipse cx=\"5.5\" cy=\"3.771\" rx=\"3\" ry=\"3.271\"/><path d=\"M8.469 3.198c-.5.5-1.93.476-2.469-.528-1 1-2.625 1-3.434.429M17 6.609V8l-2.539.726c-.528.151-.961.724-.961 1.274v.5H19m1-3.906V8l2.538.726c.529.151.962.724.962 1.274v.5H18\"/><ellipse cx=\"18.5\" cy=\"3.771\" rx=\"3\" ry=\"3.271\"/><path d=\"M21.453 3.195c-.5.5-1.914.479-2.453-.525-1 1-2.625 1-3.434.429M10.5 19.609V21l-2.539.726C7.433 21.877 7 22.45 7 23v.5h5.5m1-3.891V21l2.538.726c.529.151.962.724.962 1.274v.5h-5.5\"/><ellipse cx=\"12\" cy=\"16.771\" rx=\"3\" ry=\"3.271\"/><path d=\"M14.953 16.17c-.5.5-1.914.503-2.453-.5-1 1-2.625 1-3.434.429\"/><path stroke-linecap=\"round\" d=\"M4 12.5L6.5 15M20 12.5L17.5 15\"/></g>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});