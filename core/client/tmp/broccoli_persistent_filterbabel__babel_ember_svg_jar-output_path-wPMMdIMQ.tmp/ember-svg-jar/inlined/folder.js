define("ember-svg-jar/inlined/folder", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M31.313 8H28V6a.694.694 0 0 0-.688-.688H10.687v-2c0-.375-.313-.625-.688-.625H.686c-.375 0-.688.25-.688.625v22c0 2.25 1.813 4 4 4h23.313c2.625 0 4.688-2.063 4.688-4.625v-16a.694.694 0 0 0-.688-.688zm-24.625.688v16.625c0 1.5-1.188 2.688-2.688 2.688s-2.688-1.188-2.688-2.688V4h8v2c0 .375.313.688.688.688h16.688v1.313H7.313c-.375 0-.625.313-.625.688zm24 16c0 1.813-1.5 3.313-3.375 3.313H7c.625-.688 1-1.625 1-2.688v-16h22.688z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 32 32" } };
});