define("ember-svg-jar/inlined/arrow-down-small", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M.469.18l11.5 13.143L23.469.18\" transform=\"translate(1 2)\" stroke-width=\"3\" stroke=\"#0B0B0A\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>", "attrs": { "viewBox": "0 0 26 17", "xmlns": "http://www.w3.org/2000/svg" } };
});