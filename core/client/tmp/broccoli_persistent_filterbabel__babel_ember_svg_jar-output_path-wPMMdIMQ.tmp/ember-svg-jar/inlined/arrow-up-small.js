define("ember-svg-jar/inlined/arrow-up-small", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M23.469 13.323L11.969.18.469 13.323\" transform=\"translate(1 2)\" stroke-width=\"3\" stroke=\"#0B0B0A\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>", "attrs": { "viewBox": "0 0 26 17", "xmlns": "http://www.w3.org/2000/svg" } };
});