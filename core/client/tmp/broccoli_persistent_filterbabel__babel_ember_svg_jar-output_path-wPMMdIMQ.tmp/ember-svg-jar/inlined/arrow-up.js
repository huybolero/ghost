define("ember-svg-jar/inlined/arrow-up", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path fill=\"#010101\" d=\"M47.753 36.485l-23-26.285c-.381-.436-1.125-.436-1.506 0l-23 26.285a1 1 0 0 0 1.506 1.316L24 12.376l22.247 25.425a.996.996 0 0 0 1.411.095 1 1 0 0 0 .095-1.411z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 48 48" } };
});