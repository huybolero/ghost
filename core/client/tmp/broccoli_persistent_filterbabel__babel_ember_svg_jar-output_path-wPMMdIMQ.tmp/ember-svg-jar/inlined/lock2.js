define("ember-svg-jar/inlined/lock2", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M13.889 9.168V5.722c0-3.16-2.601-5.72-5.81-5.72-.04 0-.068-.003-.108-.002l-.106.002c-3.209 0-5.809 2.561-5.809 5.72v3.446H0V21h16V9.168h-2.111zm-4.073 9.663H6.187l.89-3.825a1.779 1.779 0 0 1-.89-1.535c0-.987.813-1.787 1.815-1.787s1.814.8 1.814 1.787a1.78 1.78 0 0 1-.895 1.538l.895 3.822zm1.166-9.663h-6.02V5.722c0-1.575 1.302-2.857 2.903-2.857l.142-.003.073.002c1.6 0 2.902 1.282 2.902 2.858v3.446z\" fill=\"#000\" fill-rule=\"evenodd\"/>", "attrs": { "viewBox": "0 0 16 21", "xmlns": "http://www.w3.org/2000/svg" } };
});