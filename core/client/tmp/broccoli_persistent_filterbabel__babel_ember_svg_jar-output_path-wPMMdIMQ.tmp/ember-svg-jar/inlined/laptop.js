define("ember-svg-jar/inlined/laptop", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M21 15.5v-10a.5.5 0 0 0-.5-.5h-17a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h17a.5.5 0 0 0 .5-.5zm-1-.5H4V6h16v9zm3.5 2H23V5c0-1.102-.897-2-2-2H3c-1.103 0-2 .898-2 2v12H.5a.5.5 0 0 0-.5.5v2c0 .827.673 1.5 1.5 1.5h21c.827 0 1.5-.673 1.5-1.5v-2a.5.5 0 0 0-.5-.5zM2 5c0-.552.449-1 1-1h18c.551 0 1 .448 1 1v12h-7.5a.5.5 0 0 0-.5.5v.5h-4v-.5a.5.5 0 0 0-.5-.5H2V5zm21 14.5a.5.5 0 0 1-.5.5h-21a.5.5 0 0 1-.5-.5V18h8v.5a.5.5 0 0 0 .5.5h5a.5.5 0 0 0 .5-.5V18h8v1.5z\"/>", "attrs": { "version": "1", "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});