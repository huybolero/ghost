define("ember-svg-jar/inlined/text", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M15.979 22.5l-7-20.5L2 22.5m2.383-7h9.207M.5 22.5h3.021m10.958 0H17.5m5.001 0L17.903 9.035l-2.36 6.932M16.92 17.5h3.768m.828 5H23.5\" stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});