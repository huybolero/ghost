define("ember-svg-jar/inlined/zap", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path d=\"M20.444 9.271A.496.496 0 0 0 20 9h-7.377L14.488.608a.5.5 0 0 0-.895-.399l-10 14A.498.498 0 0 0 4 15h7.377l-1.865 8.392a.5.5 0 0 0 .285.565L10 24a.502.502 0 0 0 .407-.209l10-14a.5.5 0 0 0 .037-.52zM11.021 21.21l1.467-6.602A.498.498 0 0 0 12 14H4.972l8.007-11.21-1.467 6.602A.498.498 0 0 0 12 10h7.028l-8.007 11.21z\"/>", "attrs": { "version": "1", "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});