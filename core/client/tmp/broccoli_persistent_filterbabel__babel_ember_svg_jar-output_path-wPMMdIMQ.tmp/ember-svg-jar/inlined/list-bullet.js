define("ember-svg-jar/inlined/list-bullet", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" fill=\"none\"><circle cx=\"2.5\" cy=\"4.5\" r=\"2\"/><path d=\"M8.569 4.428H23.5\"/><circle cx=\"2.5\" cy=\"12.5\" r=\"2\"/><path d=\"M8.569 12.428H23.5\"/><circle cx=\"2.5\" cy=\"20.5\" r=\"2\"/><path d=\"M8.569 20.427H23.5\"/></g>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});