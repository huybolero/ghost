define("ember-svg-jar/inlined/github-star", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path fill=\"#010101\" d=\"M42.022 19.07l-11.799-1.704-5.27-10.614-5.269 10.614L7.885 19.07l8.538 8.273-2.013 11.673 10.543-5.511 10.544 5.511-2.013-11.673z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 48 48" } };
});