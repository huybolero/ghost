define("ember-svg-jar/inlined/loop-infinite", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<g fill=\"none\" stroke=\"#000\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\"><path d=\"M10.5 10S9 6.5 6 6.5.5 8.963.5 12s2.462 5.5 5.5 5.5c5 0 7-11 12-11 3 0 5.5 2.463 5.5 5.5S21 17.5 18 17.5 13.5 15 13.5 15\"/><path d=\"M7.025 9.344L10.5 10l.654-3.475\"/></g>", "attrs": { "version": "1", "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 24 24" } };
});