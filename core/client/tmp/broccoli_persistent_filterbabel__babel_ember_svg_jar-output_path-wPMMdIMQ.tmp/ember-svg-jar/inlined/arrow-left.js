define("ember-svg-jar/inlined/arrow-left", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = { "content": "<path fill=\"#010101\" d=\"M37.802 46.247L12.376 24 37.801 1.753A1 1 0 1 0 36.484.247l-26.286 23a1.004 1.004 0 0 0 .001 1.506l26.286 23a.997.997 0 0 0 1.411-.095 1 1 0 0 0-.094-1.411z\"/>", "attrs": { "xmlns": "http://www.w3.org/2000/svg", "viewBox": "0 0 48 48" } };
});