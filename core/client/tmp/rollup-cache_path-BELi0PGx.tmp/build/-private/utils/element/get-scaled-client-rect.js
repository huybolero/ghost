export default function getScaledClientRect(element, scale) {
  var rect = element.getBoundingClientRect();

  if (scale === 1) {
    return rect;
  }

  var scaled = {};

  for (var key in rect) {
    scaled[key] = rect[key] * scale;
  }

  return scaled;
}