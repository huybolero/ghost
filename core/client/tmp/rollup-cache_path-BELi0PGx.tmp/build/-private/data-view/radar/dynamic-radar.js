

import Radar from './radar';
import SkipList from '../skip-list';
import roundTo from '../utils/round-to';
import getScaledClientRect from '../../utils/element/get-scaled-client-rect';

export default class DynamicRadar extends Radar {
  constructor(parentToken, options) {
    super(parentToken, options);

    this._firstItemIndex = 0;
    this._lastItemIndex = 0;

    this._totalBefore = 0;
    this._totalAfter = 0;

    this._minHeight = Infinity;

    this._nextIncrementalRender = null;

    this.skipList = null;

    if (true) {
      Object.preventExtensions(this);
    }
  }

  destroy() {
    super.destroy();

    this.skipList = null;
  }

  scheduleUpdate(didUpdateItems) {
    // Cancel incremental render check, since we'll be remeasuring anyways
    if (this._nextIncrementalRender !== null) {
      this._nextIncrementalRender.cancel();
      this._nextIncrementalRender = null;
    }

    super.scheduleUpdate(didUpdateItems);
  }

  afterUpdate() {
    // Schedule a check to see if we should rerender
    if (this._nextIncrementalRender === null && this._nextUpdate === null) {
      this._nextIncrementalRender = this.schedule('sync', () => {
        this._nextIncrementalRender = null;

        if (this._shouldScheduleRerender()) {
          this.update();
        }
      });
    }

    super.afterUpdate();
  }

  _updateConstants() {
    super._updateConstants();

    if (this._calculatedEstimateHeight < this._minHeight) {
      this._minHeight = this._calculatedEstimateHeight;
    }

    // Create the SkipList only after the estimateHeight has been calculated the first time
    if (this.skipList === null) {
      this.skipList = new SkipList(this.totalItems, this._calculatedEstimateHeight);
    } else {
      this.skipList.defaultValue = this._calculatedEstimateHeight;
    }
  }

  _updateIndexes() {
    var bufferSize = this.bufferSize,
        skipList = this.skipList,
        visibleTop = this.visibleTop,
        visibleBottom = this.visibleBottom,
        totalItems = this.totalItems,
        _didReset = this._didReset;


    if (totalItems === 0) {
      this._firstItemIndex = 0;
      this._lastItemIndex = -1;
      this._totalBefore = 0;
      this._totalAfter = 0;

      return;
    }

    // Don't measure if the radar has just been instantiated or reset, as we are rendering with a
    // completely new set of items and won't get an accurate measurement until after they render the
    // first time.
    if (_didReset === false) {
      this._measure();
    }

    var values = skipList.values;

    var _skipList$find = this.skipList.find(visibleTop),
        totalBefore = _skipList$find.totalBefore,
        firstVisibleIndex = _skipList$find.index;

    var _skipList$find2 = this.skipList.find(visibleBottom),
        totalAfter = _skipList$find2.totalAfter,
        lastVisibleIndex = _skipList$find2.index;

    var maxIndex = totalItems - 1;

    var firstItemIndex = firstVisibleIndex;
    var lastItemIndex = lastVisibleIndex;

    // Add buffers
    for (var i = bufferSize; i > 0 && firstItemIndex > 0; i--) {
      firstItemIndex--;
      totalBefore -= values[firstItemIndex];
    }

    for (var _i = bufferSize; _i > 0 && lastItemIndex < maxIndex; _i--) {
      lastItemIndex++;
      totalAfter -= values[lastItemIndex];
    }

    this._firstItemIndex = firstItemIndex;
    this._lastItemIndex = lastItemIndex;
    this._totalBefore = totalBefore;
    this._totalAfter = totalAfter;
  }

  _calculateScrollDiff() {
    var firstItemIndex = this.firstItemIndex,
        _prevFirstVisibleIndex = this._prevFirstVisibleIndex,
        _prevFirstItemIndex = this._prevFirstItemIndex;


    var beforeVisibleDiff = 0;

    if (firstItemIndex < _prevFirstItemIndex) {
      // Measurement only items that could affect scrollTop. This will necesarilly be the
      // minimum of the either the total number of items that are rendered up to the first
      // visible item, OR the number of items that changed before the first visible item
      // (the delta). We want to measure the delta of exactly this number of items, because
      // items that are after the first visible item should not affect the scroll position,
      // and neither should items already rendered before the first visible item.
      var measureLimit = Math.min(Math.abs(firstItemIndex - _prevFirstItemIndex), _prevFirstVisibleIndex - firstItemIndex);

      beforeVisibleDiff = Math.round(this._measure(measureLimit));
    }

    return beforeVisibleDiff + super._calculateScrollDiff();
  }

  _shouldScheduleRerender() {
    var firstItemIndex = this.firstItemIndex,
        lastItemIndex = this.lastItemIndex;


    this._updateConstants();
    this._measure();

    // These indexes could change after the measurement, and in the incremental render
    // case we want to check them _after_ the change.
    var firstVisibleIndex = this.firstVisibleIndex,
        lastVisibleIndex = this.lastVisibleIndex;


    return firstVisibleIndex < firstItemIndex || lastVisibleIndex > lastItemIndex;
  }

  _measure(measureLimit = null) {
    var orderedComponents = this.orderedComponents,
        skipList = this.skipList,
        _occludedContentBefore = this._occludedContentBefore,
        _transformScale = this._transformScale;


    var numToMeasure = measureLimit !== null ? Math.min(measureLimit, orderedComponents.length) : orderedComponents.length;

    var totalDelta = 0;

    for (var i = 0; i < numToMeasure; i++) {
      var currentItem = orderedComponents[i];
      var previousItem = orderedComponents[i - 1];
      var itemIndex = currentItem.index;

      var _getScaledClientRect = getScaledClientRect(currentItem, _transformScale),
          currentItemTop = _getScaledClientRect.top,
          currentItemHeight = _getScaledClientRect.height;

      var margin = void 0;

      if (previousItem !== undefined) {
        margin = currentItemTop - getScaledClientRect(previousItem, _transformScale).bottom;
      } else {
        margin = currentItemTop - getScaledClientRect(_occludedContentBefore, _transformScale).bottom;
      }

      var newHeight = roundTo(currentItemHeight + margin);
      var itemDelta = skipList.set(itemIndex, newHeight);

      if (newHeight < this._minHeight) {
        this._minHeight = newHeight;
      }

      if (itemDelta !== 0) {
        totalDelta += itemDelta;
      }
    }

    return totalDelta;
  }

  _didEarthquake(scrollDiff) {
    return scrollDiff > this._minHeight / 2;
  }

  get total() {
    return this.skipList.total;
  }

  get totalBefore() {
    return this._totalBefore;
  }

  get totalAfter() {
    return this._totalAfter;
  }

  get firstItemIndex() {
    return this._firstItemIndex;
  }

  get lastItemIndex() {
    return this._lastItemIndex;
  }

  get firstVisibleIndex() {
    var visibleTop = this.visibleTop;

    var _skipList$find3 = this.skipList.find(visibleTop),
        index = _skipList$find3.index;

    return index;
  }

  get lastVisibleIndex() {
    var visibleBottom = this.visibleBottom,
        totalItems = this.totalItems;

    var _skipList$find4 = this.skipList.find(visibleBottom),
        index = _skipList$find4.index;

    return Math.min(index, totalItems - 1);
  }

  prepend(numPrepended) {
    super.prepend(numPrepended);

    this.skipList.prepend(numPrepended);
  }

  append(numAppended) {
    super.append(numAppended);

    this.skipList.append(numAppended);
  }

  reset() {
    super.reset();

    this.skipList.reset(this.totalItems);
  }

  /*
   * Public API to query the skiplist for the offset of an item
   */
  getOffsetForIndex(index) {
    this._measure();

    return this.skipList.getOffset(index);
  }
}