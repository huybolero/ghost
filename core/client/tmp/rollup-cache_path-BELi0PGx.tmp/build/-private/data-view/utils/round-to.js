export default function roundTo(number, decimal = 2) {
  var exp = Math.pow(10, decimal);
  return Math.round(number * exp) / exp;
}