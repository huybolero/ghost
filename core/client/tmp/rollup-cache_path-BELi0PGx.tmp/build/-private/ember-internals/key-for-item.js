

import identity from './identity';

export default function keyForItem(item, keyPath, index) {
  var key = void 0;

  (true && !(typeof keyPath === 'string') && Ember.assert(`keyPath must be a string, received: ${keyPath}`, typeof keyPath === 'string'));


  switch (keyPath) {
    case '@index':
      (true && !(typeof index === 'number') && Ember.assert(`A numerical index must be supplied for keyForItem when keyPath is @index, received: ${index}`, typeof index === 'number'));

      key = index;
      break;
    case '@identity':
      key = identity(item);
      break;
    default:
      key = Ember.get(item, keyPath);
  }

  if (typeof key === 'number') {
    key = String(key);
  }

  return key;
}