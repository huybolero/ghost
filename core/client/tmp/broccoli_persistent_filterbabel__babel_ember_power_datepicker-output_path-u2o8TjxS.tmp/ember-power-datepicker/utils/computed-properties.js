define('ember-power-datepicker/utils/computed-properties', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.fallbackAction = fallbackAction;
  function fallbackAction(fallback) {
    return Ember.computed({
      get() {
        return fallback.bind(this);
      },
      set(_, v) {
        return v === undefined ? fallback.bind(this) : v;
      }
    });
  }
});